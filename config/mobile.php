<?php
return [
  'mobileSellerId' => '1028',
  'vehicleClass' => 'VanUpTo7500',
  'OtherVanUpTo7500' => 'Other vans/trucks up to 7.5 t',
  'username' =>  env('MOBILE_USERNAME', ''),
  'password' =>  env('MOBILE_PASSWORD', ''),
  'sellerID' =>  env('MOBILE_SELLERID', ''),
]
;