<?php

return [
  'construction_types' => [
    'trunck', 'open_trailer', 'van', 'van_high_long', 'chassis_cab', 'sail', 'trunck_lift', 'trunck_doors'
  ],
  'drivetrains' => [
    'front', 'back'
  ],
];
