<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;


class DefaultTextsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $texts = [
          [
            'type'       => 'fixed_text_1',
            'locale'     => 'en',
            'message'     => 'Please call for an appointment',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
          ],
          [
            'type'       => 'fixed_text_1',
            'locale'     => 'nl',
            'message'     => 'Gelieve te bellen voor een afspraak',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
          ],
          [
            'type'       => 'fixed_text_1',
            'locale'     => 'fr',
            'message'     => 'Veuillez nous téléphoner pour un rendez-vous',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
          ],
          [
            'type'       => 'fixed_text_2',
            'locale'     => 'en',
            'message'    => 'Dicar Trucks is very easy to find and easily accessible: Directly at exit 23 (Geel-West) on the E313 Antwerp-Hasselt.<br/>At Dicar Trucks, you can count on the best value for money.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
          ],
          [
            'type'       => 'fixed_text_2',
            'locale'     => 'nl',
            'message'    => 'Dicar Vrachtwagens is zeer gemakkelijk te vinden en goed bereikbaar: direct langs afrit 23 (Geel-West) op de E313 Antwerpen-Hasselt. <br/>Bij Dicar Vrachtwagens mag u rekenen op de beste prijs-kwaliteitverhouding.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
          ],
          [
            'type'       => 'fixed_text_2',
            'locale'     => 'fr',
            'message'    => 'Dicar Trucks est très facile à trouver et facilement accessible: juste à côté de la sortie 23 (Geel-Ouest) sur la E313 Anvers-Hasselt.<br/> Chez  Dicar Trucks vous pouvez compter sur le meilleur rapport qualité-prix.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
          ],
        ];

        DB::table('defaultTexts')->insert($texts);
    }
}
