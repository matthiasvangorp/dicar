<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTrucksTable3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
  public function up()
  {
    //
    Schema::table('trucks', function (Blueprint $table) {
      $table->integer('truckengine_id')->unsigned()->after('truckbrand_id');
      if (!Schema::hasColumn('trucks', 'engine')) {
        $table->dropColumn('engine');
      }
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //
    Schema::table('trucks', function (Blueprint $table) {
      $table->dropColumn('truckengine_id');
      if (!Schema::hasColumn('trucks', 'engine')) {
        $table->string('engine')->nullable();
      }
    });

  }
}
