<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrucksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
  public function up()
  {
    Schema::create('trucks', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name');
      $table->string('engine')->nullable();
      $table->string('color')->nullable();
      $table->date('year')->nullable();
      $table->Integer('mileage')->nullable();
      $table->string('construction')->nullable();
      $table->string('dimensions')->nullable();
      $table->string('tailgate')->nullable();
      $table->smallInteger('mtm')->nullable();
      $table->smallInteger('empty_weight')->nullable();
      $table->smallInteger('horsepower')->nullable();
      $table->smallInteger('transmission')->nullable();
      $table->string('drivetrain')->nullable();
      $table->boolean('used')->nullable();
      $table->smallInteger('seats')->nullable();
      $table->boolean('open_roof')->nullable();
      $table->boolean('side_door')->nullable();
      $table->boolean('air_seat')->nullable();
      $table->boolean('double_rear_tires')->nullable();
      $table->boolean('electric_windows')->nullable();
      $table->string('remarks', 1000)->nullable();
      $table->string('images', 500)->nullable();
      $table->double('price')->nullable();
      $table->boolean('vat')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //
    Schema::drop('trucks');
  }
}
