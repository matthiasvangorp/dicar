<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTrucksTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('trucks', function (Blueprint $table) {


        $table->boolean('published', 1000)->after('truckbrand_id')->default(false);
        //chassis
        $table->boolean('chassis_air_suspension')->after('truckbrand_id')->default(false);
        $table->boolean('chassis_cruise_control')->after('truckbrand_id')->default(false);
        $table->string('chassis_remarks', 1000)->after('truckbrand_id')->nullable();

        //Construction
        $table->smallInteger('construction_type')->after('truckbrand_id')->nullable();
        $table->boolean('construction_lift')->after('truckbrand_id')->default(false);
        $table->smallInteger('construction_lift_power')->after('truckbrand_id')->nullable();
        $table->string('construction_dimensions')->after('truckbrand_id')->nullable();
        $table->boolean('construction_sidedoor')->after('truckbrand_id')->default(false);
        $table->string('construction_remarks', 1000)->after('truckbrand_id')->nullable();

        //weights
        $table->smallInteger('weights_empty')->after('truckbrand_id')->nullable();
        $table->smallInteger('weights_mtm')->after('truckbrand_id')->nullable();
        $table->smallInteger('weights_useful_load_capacity')->after('truckbrand_id')->nullable();
        $table->smallInteger('weights_useful_tow_capacity')->after('truckbrand_id')->nullable();
        $table->smallInteger('weights_max_trailer_load')->after('truckbrand_id')->nullable();
        $table->string('weights_driving_license')->after('truckbrand_id')->nullable();
        $table->boolean('weights_eurovignet')->after('truckbrand_id')->default(false);
        $table->string('weights_remarks', 1000)->after('truckbrand_id')->nullable();

        //equipment
        $table->smallInteger('equipment_airbags')->after('truckbrand_id')->nullable();
        $table->boolean('equipment_rockinger')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_roofspoiler')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_automatic_airco')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_airco')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_airsprung_seat')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_pto')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_abs')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_rear_camera_color')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_rear_camera')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_alarm')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_motor_break')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_retarder')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_aluminum_rims')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_cd_player')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_crane')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_sleeping_cabin')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_central_locking')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_double_rear_tires')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_roof_rails')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_electric_windows')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_electric_mirrors')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_gps')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_headlamp_sprayers')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_multifunctional_wheel')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_non_smoker')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_sunroof')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_pdf')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_rainsensor')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_stability_control')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_night_heating')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_power_steering')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_phone')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_towing_hook')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_detachable_towing_hook')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_tv')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_lowered_suspension')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_heated_mirrors')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_heated_seats')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_xenon_headlights')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_sunvisor')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_reserve_differential')->after('truckbrand_id')->default(false);
        $table->boolean('equipment_electropack')->after('truckbrand_id')->default(false);
        $table->string('equipment_terms_remarks', 1000)->after('truckbrand_id')->nullable();

        //delivery terms
        $table->boolean('delivery_terms_technical_inspection')->after('truckbrand_id')->default(false);
        $table->boolean('delivery_terms_legal_kit')->after('truckbrand_id')->default(false);
        $table->boolean('delivery_terms_cleaning')->after('truckbrand_id')->default(false);
        $table->boolean('delivery_terms_calibration_tachograph')->after('truckbrand_id')->default(false);
        $table->boolean('delivery_terms_pvg')->after('truckbrand_id')->default(false);
        $table->boolean('delivery_terms_technical_checkup')->after('truckbrand_id')->default(false);
        $table->boolean('delivery_terms_guarantee')->after('truckbrand_id')->default(false);
        $table->boolean('delivery_terms_new_tires')->after('truckbrand_id')->default(false);
        $table->string('delivery_terms_remarks', 1000)->after('truckbrand_id')->nullable();

        $table->dropColumn('construction');
        $table->dropColumn('dimensions');
        $table->dropColumn('tailgate');
        $table->dropColumn('mtm');
        $table->dropColumn('empty_weight');
        $table->dropColumn('transmission');
        $table->dropColumn('drivetrain');
        $table->dropColumn('open_roof');
        $table->dropColumn('side_door');
        $table->dropColumn('air_seat');
        $table->dropColumn('double_rear_tires');
        $table->dropColumn('electric_windows');






      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

      Schema::table('trucks', function (Blueprint $table) {
      $table->dropColumn('published');
      //chassis
      $table->dropColumn('chassis_air_suspension');
      $table->dropColumn('chassis_cruise_control');
      $table->dropColumn('chassis_remarks');

      //Construction
      $table->dropColumn('construction_type');
      $table->dropColumn('construction_lift');
      $table->dropColumn('construction_lift_power');
      $table->dropColumn('construction_dimensions');
      $table->dropColumn('construction_sidedoor');
      $table->dropColumn('construction_remarks');

      //weights

      $table->dropColumn('weights_empty');
      $table->dropColumn('weights_mtm');
      $table->dropColumn('weights_useful_load_capacity');
      $table->dropColumn('weights_useful_tow_capacity');
      $table->dropColumn('weights_max_trailer_load');
      $table->dropColumn('weights_driving_license');
      $table->dropColumn('weights_eurovignet');
      $table->dropColumn('weights_remarks');


      //equipment

      $table->dropColumn('equipment_airbags');
      $table->dropColumn('equipment_rockinger');
      $table->dropColumn('equipment_roofspoiler');
      $table->dropColumn('equipment_automatic_airco');
      $table->dropColumn('equipment_airco');
      $table->dropColumn('equipment_airsprung_seat');
      $table->dropColumn('equipment_pto');
      $table->dropColumn('equipment_abs');
      $table->dropColumn('equipment_rear_camera_color');
      $table->dropColumn('equipment_rear_camera');
      $table->dropColumn('equipment_alarm');
      $table->dropColumn('equipment_motor_break');
      $table->dropColumn('equipment_retarder');
      $table->dropColumn('equipment_aluminum_rims');
      $table->dropColumn('equipment_cd_player');
      $table->dropColumn('equipment_crane');
      $table->dropColumn('equipment_sleeping_cabin');
      $table->dropColumn('equipment_central_locking');
      $table->dropColumn('equipment_double_rear_tires');
      $table->dropColumn('equipment_roof_rails');
      $table->dropColumn('equipment_electric_windows');

      $table->dropColumn('equipment_electric_mirrors');
      $table->dropColumn('equipment_gps');
      $table->dropColumn('equipment_headlamp_sprayers');
      $table->dropColumn('equipment_multifunctional_wheel');
      $table->dropColumn('equipment_non_smoker');

      $table->dropColumn('equipment_sunroof');
      $table->dropColumn('equipment_pdf');
      $table->dropColumn('equipment_rainsensor');
      $table->dropColumn('equipment_stability_control');
      $table->dropColumn('equipment_night_heating');

      $table->dropColumn('equipment_power_steering');
      $table->dropColumn('equipment_phone');
      $table->dropColumn('equipment_towing_hook');
      $table->dropColumn('equipment_detachable_towing_hook');
      $table->dropColumn('equipment_tv');

      $table->dropColumn('equipment_lowered_suspension');
      $table->dropColumn('equipment_heated_mirrors');
      $table->dropColumn('equipment_heated_seats');
      $table->dropColumn('equipment_xenon_headlights');
      $table->dropColumn('equipment_sunvisor');

      $table->dropColumn('equipment_reserve_differential');
      $table->dropColumn('equipment_electropack');
      $table->dropColumn('equipment_terms_remarks');


      //delivery terms

      $table->dropColumn('delivery_terms_technical_inspection');
      $table->dropColumn('delivery_terms_legal_kit');
      $table->dropColumn('delivery_terms_cleaning');
      $table->dropColumn('delivery_terms_calibration_tachograph');
      $table->dropColumn('delivery_terms_pvg');
      $table->dropColumn('delivery_terms_technical_checkup');
      $table->dropColumn('delivery_terms_guarantee');
      $table->dropColumn('delivery_terms_new_tires');
      $table->dropColumn('delivery_terms_remarks');

      $table->string('construction')->nullable();
      $table->string('dimensions')->nullable();
      $table->string('tailgate')->nullable();
      $table->smallInteger('mtm')->nullable();
      $table->smallInteger('empty_weight')->nullable();
      $table->smallInteger('transmission')->nullable();
      $table->string('drivetrain')->nullable();
      $table->boolean('open_roof')->nullable();
      $table->boolean('side_door')->nullable();
      $table->boolean('air_seat')->nullable();
      $table->boolean('double_rear_tires')->nullable();
      $table->boolean('electric_windows')->nullable();

      });
    }
}
