<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTrucksTable6 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
      Schema::table('trucks', function (Blueprint $table) {
        $table->string('construction_type', 50)->change();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('trucks', function (Blueprint $table) {
            $table->dropColumn('construction_type');
        });

        Schema::table('trucks', function (Blueprint $table) {
            $table->smallInteger('construction_type')->nullable();
        });
    }
}
