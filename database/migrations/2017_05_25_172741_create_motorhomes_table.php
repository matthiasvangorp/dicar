<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMotorhomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('motorhomes', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->string('engine')->nullable();
        $table->string('color')->nullable();
        $table->smallInteger('sleepingPlaces')->nullable();
        $table->smallInteger('seats')->nullable();
        $table->string('remarks')->nullable();
        $table->float('price')->nullable();
        $table->float('discount')->nullable();
        $table->boolean('vat')->nullable();
        $table->smallInteger('airbags')->nullable();
        $table->smallInteger('cookingpits')->nullable();
        $table->smallInteger('watertank')->nullable();
        $table->smallInteger('fridge')->nullable();
        $table->string('images', 500)->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
      Schema::drop('motorhomes');
    }
}
