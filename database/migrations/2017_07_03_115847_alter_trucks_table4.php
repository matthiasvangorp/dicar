<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTrucksTable4 extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    //
    Schema::table('trucks', function (Blueprint $table) {
      $table->dropColumn('images');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
    Schema::table('trucks', function (Blueprint $table) {
      $table->string('images', 500)->nullable();
    });
  }
}