<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('defaultTexts', function (Blueprint $table){
            $table->increments('id');
            $table->string(('type'));
            $table->longText(('message'));
            $table->string(('locale'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
        if (Schema::hasTable('defaultTexts')) {
            Schema::drop('defaultTexts');
        }
    }
}
