<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTrucksTable8 extends Migration
{
    public function up()
    {
        //
        Schema::table('trucks', function (Blueprint $table) {
            $table->integer('construction_lift_power_id')->unsigned()->after('truckbrand_id');
            if (!Schema::hasColumn('trucks', 'construction_lift_power')) {
                $table->dropColumn('construction_lift_power');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('trucks', function (Blueprint $table) {
            $table->dropColumn('construction_lift_power_id');
            if (!Schema::hasColumn('trucks', 'construction_lift_power')) {
                $table->string('construction_lift_power')->nullable();
            }
        });

    }
}
