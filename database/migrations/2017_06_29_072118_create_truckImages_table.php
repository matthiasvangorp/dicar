<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTruckImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('truckImages', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('truck_id')->unsigned()->nullable();
        $table->foreign('truck_id')->references('id')->on('trucks');
        $table->string('filename');
        $table->string('description', 1000)->nullable();
        $table->smallInteger('order')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('truckImages');
    }
}
