<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTruckBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
  public function up()
  {
    //
    Schema::create('truckBrands', function (Blueprint $table){
      $table->increments('id');
      $table->string(('name'));
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
    if (Schema::hasTable('truckBrands')) {
      Schema::drop('truckBrands');
    }
  }
}
