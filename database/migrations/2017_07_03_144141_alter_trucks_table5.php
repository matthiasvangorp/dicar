<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTrucksTable5 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('trucks', function (Blueprint $table) {
        $table->boolean('sold')->default(false);
        $table->string('property_consignment', 50)->nullable();
        $table->string('vat_margin', 50)->nullable();
        $table->string('damage', 50)->nullable();
        $table->string('chassis_number', 50)->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
      //
      Schema::table('trucks', function (Blueprint $table) {
        $table->dropColumn('sold');
        $table->dropColumn('property_consignment');
        $table->dropColumn('vat_margin');
        $table->dropColumn('damage');
        $table->dropColumn('chassis_number');
      });
    }
}
