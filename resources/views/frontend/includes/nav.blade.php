<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#frontend-navbar-collapse">
                <span class="sr-only">{{ trans('labels.general.toggle_navigation') }}</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            {{ link_to_route('frontend.index', trans('strings.frontend.site_name'), [], ['class' => 'navbar-brand dicar-orange']) }}
        </div><!--navbar-header-->

        <div class="collapse navbar-collapse" id="frontend-navbar-collapse">

            <ul class="nav navbar-nav">
                <li>{!! link_to_route('frontend.contact', trans('navs.frontend.contact'), [], ['class' => active_class(Active::checkRoute('frontend.contact')) ]) !!}</li>
                <li>{!! link_to_route('frontend.sell_your_truck', trans('strings.sell.sell_your_truck'), [], ['class' => active_class(Active::checkRoute('frontend.sell_your_truck')) ]) !!}</li>
                <!--li>{{ link_to_route('frontend.macros', trans('navs.frontend.macros'), [], ['class' => active_class(Active::checkRoute('frontend.macros')) ]) }}</li-->
            </ul>

            <ul class="nav navbar-nav navbar-right">
                @if (config('locale.status') && count(config('locale.languages')) > 1)
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <span class="lang-sm lang-lbl-'{!!App::getLocale() !!}" lang="{!!App::getLocale() !!}"></span>
                            <span class="caret"></span>
                        </a>

                        @include('includes.partials.lang')
                    </li>
                @endif

                @if ($logged_in_user)
                        <li>{{ link_to_route('admin.trucks.index', trans('navs.frontend.trucks'), [], ['class' => active_class(Active::checkRoute('admin.trucks.index')) ]) }}</li>

                        <li>{{ link_to_route('frontend.user.dashboard', trans('navs.frontend.dashboard'), [], ['class' => active_class(Active::checkRoute('frontend.user.dashboard')) ]) }}</li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ $logged_in_user->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                @permission('view-backend')
                                <li>{{ link_to_route('admin.dashboard', trans('navs.frontend.user.administration')) }}</li>
                                @endauth
                                <li>{{ link_to_route('frontend.user.account', trans('navs.frontend.user.account'), [], ['class' => active_class(Active::checkRoute('frontend.user.account')) ]) }}</li>
                                <li>{{ link_to_route('frontend.auth.logout', trans('navs.general.logout')) }}</li>
                            </ul>
                        </li>
                @endif

            </ul>
        </div><!--navbar-collapse-->
    </div><!--container-->
</nav>
<div class="container">
    <div class="row" style="text-align: center">
        <img src="{!! URL::to('/images/trucks/vrachtwagenbanner_1200pix.png') !!}" class="img-responsive">
    </div>
</div>