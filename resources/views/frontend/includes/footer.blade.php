<!-- Origional snipet by msurguy<http://bootsnipp.com/msurguy>, I editted his snipet so that it works with bootstrap 3.3 -->
<div class="container">
    <hr>
    <div class="row">
        <div class="col-xs-8">
            <ul class="list-unstyled list-inline pull-left">
                <li><a href="http://www.dicar.be">{!! trans('strings.footer.dicar_motorhomes') !!}</a></li>
                <li>{!! link_to_route('frontend.contact', trans('strings.contact.contact_us')) !!}</li>
                <li>Grote steenweg 2 - 2440 Geel - {!! trans('strings.contact.belgium') !!}</li>
                <li><a href="http://www.seo-website-promotie.be" target="_blank">{!! trans('strings.footer.seo_website_promotie') !!}</a></li>
            </ul>
        </div>
        <div class="col-xs-4">
            <p class="text-muted pull-right">© {!! trans('strings.frontend.site_name') !!}</p>
        </div>
    </div>
</div>