@extends('frontend.layouts.app')

@section('title', trans('titles.used').' '.trans('titles.trucks').' '.trans('titles.for_sale'))
@section('meta_description', trans('meta.homepage'))

@section('content')
    <div class="row">
        <div class="col-xs-12" style="color: black">
            <h1>DICAR VRACHTWAGENS: SPECIALISATIE TWEEDEHANDS VRACHTWAGENS MAN TGL EURO 5 / EURO 6</h1>
            <p>
                Dicar is, behalve motorhomes, ook gespecialiseerd in de verkoop van betrouwbare jonge tweedehands
                vrachtwagens van MAN TGL. Dat kan gaan van MAN 7.150 over MAN TGL 8.180, MAN TGL 10.220 tot MAN TGL
                12.220. </p>
            <p>
                De meeste vrachtwagens zijn occasie, maar wel met een motor die minimum voldoet aan de EURO5- emissienorm.
            </p>
            <p>
                Dicar biedt de tweedehands MAN TGL euro 5 / euro6 aan in verschillende configuraties:
                <ul>
                    <li>
                        Tweedehands MAN TGL met standaard koffer van 6m10 en laadklep
                    </li>
                    <li>
                        Tweedehands MAN TGL met standaard koffer van 6m10 en deuren achteraan
                    </li>
                    <li>
                        Tweedehands MAN TGL met korte koffer van 4m45 tot 5m30 en laadklep
                    </li>
                    <li>
                        Tweedehands MAN TGL met korte koffer van 4m45 tot 5m30 en deuren achteraan
                    </li>
                    <li>
                        Tweedehands MAN TGL met lange koffer van 7m20 en laadlift achteraan
                    </li>
                    <li>
                        Tweedehands MAN TGL met extra lange koffer van 8m20 en laadlift achteraan
                    </li>
                    <li>
                        Tweedehands MAN TGL met bache of rolgordijn
                    </li>
                    <li>
                        Tweedehands MAN TGL met open laadbak
                    </li>
                    <li>
                        Tweedehands MAN TGL chassis-cabine
                    </li>
                </ul>
            </p>
            <p>
                Deze tweedehands MAN TGL kunnen beschikken over volgende extra’s:
                <ul>
                    <li>
                        Airco
                    </li>
                    <li>
                        Airco automatisch
                    </li>
                    <li>
                        Luchtvering achteraan
                    </li>
                    <li>
                        Dakspoiler
                    </li>
                    <li>
                        Muilkoppeling / trekhaak
                    </li>
                    <li>
                        Elektropakket (elektrische ruiten  en – spiegels, verwarmde spiegels)
                    </li>
                    <li>
                        Cruise control
                    </li>
                    <li>
                        Open dak
                    </li>
                    <li>
                        Zonneklep
                    </li>
                    <li>
                        Motorrem
                    </li>
                    <li>
                        Sperdifferentieel
                    </li>
                </ul>
            </p>
            <p>
                Aanvullend kan u, mbt uw reclame, kiezen voor een voordelige belettering of volledige bestickering van de occasie camion.  Deze belettering kan hier bij Dicar gebeuren.
            </p>
            <p>
                Bij Dicar Vrachtwagens koopt u in vertrouwen.
            </p>
            <p>
                Dicar Vrachtwagens geeft 12 maanden waarborg op de tweedehands vrachtwagens en bestelwagens, tenzij anders vermeld.
            </p>
            <p>
                Dicar Vrachtwagens draagt zorg voor het bekomen van het Belgische gelijkvormigheidsattest (PVG).
            </p>
            <p>
                Dicar Vrachtwagens draagt zorg voor een volledig nazicht en onderhoud bij een erkende MAN-garage.
            </p>
            <p>
                Dicar Vrachtwagens draagt zowel zorg voor de administratieve keuring, als voor de techische keuring.
            </p>
            <p>Dicar Vrachtwagens is zeer gemakkelijk te vinden en goed bereikbaar: direct langs afrit 23 (Geel-West)
                op de E313 Antwerpen-Hasselt.<br/> Bij Dicar Vrachtwagens mag u rekenen op de beste
                prijs-kwaliteitverhouding. <br/> Dicar Vrachtwagens - Grote Steenweg 2 - 2440 Geel - B.
            </p>
            <p>
                Geachte bezoeker,  <br/>Om u sneller en beter te kunnen bedienen, graag vooraf een telefoontje op 0497 537064
                voor een afspraak. <br/>Maarten Goossens / Dicar Vrachtwagens
            </p>
        </div>

        @foreach ($trucks as $truck)

        <div class="col-xs-12">

            <br/><br/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-truck dicar-orange" aria-hidden="true"></i>
                    <strong>
                        <a class="dicar-orange" href="{!!  URL::to('trucks/'.$truck->id) !!}">{!! $truck->brand->name !!}
                            {!! $truck->name !!} {!! $truck->engine->name !!} {!! $truck->construction !!} </a>
                    </strong>
                    @role('Executive')
                    <div class="pull-right">
                        <a href="{!! URL::to('export/' . $truck->id) !!}" title="Export 2dehands">
                            <i class="fa fa-file" aria-hidden="true"></i>
                        </a>
                        <a href="{!! URL::to('pdf/' . $truck->id) !!}" title="Vensterblad maken">
                            <i class="fa fa-map" aria-hidden="true"></i>
                        </a>
                        <a href="{!! URL::to('admin/trucks/' . $truck->id . '/edit') !!}">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                    </div>
                    @endauth
                </div>

                <a href="{!!  URL::to('trucks/'.$truck->id) !!}">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                        @if(!empty($truck->mileage))
                            <div class="row">
                                <div class="col-xs-6">{!! trans('strings.frontend.mileage') !!} </div>
                                <div class="col-xs-6"> {!! $truck->mileage !!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->euronorm))
                            <div class="row">
                                <div class="col-xs-6">{!! trans('trucks.fields.euronorm') !!} </div>
                                <div class="col-xs-6"> {!! $truck->euronorm !!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->delivery_terms_guarantee && $truck->delivery_terms_guarantee))
                            <div class="row">
                                <div class="col-xs-6">{!! trans('trucks.fields.delivery_terms_guarantee') !!} </div>
                                <div class="col-xs-6"> <i class="fa fa-check" aria-hidden="true" style="color:green"></i></div>
                            </div>
                        @endif
                        @if(!empty($truck->engine))
                            <div class="row">
                                <div class="col-xs-6">{!! trans('strings.frontend.engine') !!} </div>
                                <div class="col-xs-6"> {!! $truck->engine->name !!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->horsepower))
                            <div class="row">
                                <div class="col-xs-6">{!! trans('strings.frontend.horsepower') !!} </div>
                                <div class="col-xs-6">{!! $truck->horsepower !!} / {!! round($truck->horsepower * 1.34102209, 0)!!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->color))
                            <div class="row">
                                <div class="col-xs-6">{!! trans('strings.frontend.color') !!} </div>
                                <div class="col-xs-6">{!! trans('strings.colors.'.$truck->color) !!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->seats))
                            <div class="row">
                                <div class="col-xs-6">{!! trans('strings.frontend.seats') !!} </div>
                                <div class="col-xs-6"> {!! $truck->seats !!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->year))
                            <div class="row">
                                <div class="col-xs-6">{!! trans('trucks.fields.year') !!} </div>
                                <div class="col-xs-6"> {!! date_create_from_format('Y-m-d', $truck->year)->format('m/Y')!!}</div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-6">
                                <strong>{!! trans('trucks.fields.reference') !!} </strong>
                            </div>
                            <div class="col-xs-6">
                                <strong>{!! $truck->reference !!}</strong>
                            </div>
                        </div>

                    </div>
                        <div class="col-sm-6 col-md-3">
                        <span class="price" style="font-size: 20px;">
                            @if($truck->vat_margin == "vat")
                                &euro;  {!! $truck->price !!} +  &euro; {!! round(($truck->price * 0.21), 0) !!} {!! trans('trucks.fields.vat') !!} <br/>=  &euro; {!! round(($truck->price * 1.21), 0) !!}
                            @else
                                &euro;  {!! $truck->price !!}
                            @endif
                        </span>
                    </div>
                        <div class="col-sm-6 col-md-3">
                        @if ($truck->images()->first())
                            <img height="180" width="240" class="thumbnail" src="{!!  URL::to('/photo/240x180/images/trucks/'.$truck->id.'/'.$truck->images()->where('order', 1)->first()->filename) !!}"/>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <p>{!! $truck->remarks !!}</p>
                    </div>
                </div>
                </div>
                </a>
            </div><!-- panel -->

        </div><!-- col-md-10 -->

        @endforeach



        @if (isset($soldTrucks))
            @foreach ($soldTrucks as $truck)

                <div class="col-xs-12">

                    <br/><br/>
                    <div class="panel panel-default">

                        <div class="panel-heading">
                            <i class="fa fa-truck dicar-orange" aria-hidden="true"></i>
                            <strong>
                                <a class="dicar-orange" href="{!!  URL::to('trucks/'.$truck->id) !!}">{!! $truck->brand->name !!} {!! $truck->name !!} {!! $truck->engine->name !!} {!! $truck->construction !!} </a>
                            </strong>
                            @role('Executive')
                            <div class="pull-right">
                                <a href="{!! URL::to('admin/trucks/' . $truck->id . '/edit') !!}">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                            </div>
                            @endauth
                        </div>

                        <a href="{!!  URL::to('trucks/'.$truck->id) !!}">
                            <div class="panel-body" style="overflow:hidden">

                                <div class="col-sm-6 col-md-6">
                                        @if(!empty($truck->seats))
                                            <div class="row">

                                                <div class="col-xs-6">{!! trans('strings.frontend.seats') !!} </div>
                                                <div class="col-xs-6">{!! $truck->seats !!}</div>
                                            </div>
                                        @endif
                                        @if(!empty($truck->engine))
                                            <div class="row">
                                                <div class="col-xs-6">{!! trans('strings.frontend.engine') !!} </div>
                                                <div class="col-xs-6">{!! $truck->engine->name !!}</div>
                                            </div>
                                        @endif
                                        @if(!empty($truck->horsepower))
                                            <div class="row">
                                                <div class="col-xs-6">{!! trans('strings.frontend.horsepower') !!} </div>
                                                <div class="col-xs-6"> {!! $truck->horsepower !!} / {!! round($truck->horsepower * 1.34102209, 0)!!}</div>
                                            </div>
                                        @endif
                                        @if(!empty($truck->color))
                                            <div class="row">
                                                <div class="col-xs-6">{!! trans('strings.frontend.color') !!} </div>
                                                <div class="col-xs-6">{!! trans('strings.colors.'.$truck->color) !!}</div>
                                            </div>
                                        @endif
                                        @if(!empty($truck->mileage))
                                            <div class="row">
                                                <div class="col-xs-6">{!! trans('strings.frontend.mileage') !!} </div>
                                                <div class="col-xs-6">{!! $truck->mileage !!}</div>
                                            </div>
                                        @endif
                                        @if(!empty($truck->euronorm))
                                            <div class="row">
                                                <div class="col-xs-6">{!! trans('trucks.fields.euronorm') !!} </div>
                                                <div class="col-xs-6"> {!! $truck->euronorm !!}</div>
                                            </div>
                                        @endif
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <strong>{!! trans('trucks.fields.reference') !!} </strong>
                                            </div>
                                            <div class="col-xs-6">
                                                <strong>{!! $truck->reference !!}</strong>
                                            </div>
                                        </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <span class="price" style="font-size: 20px;">
                                        @if($truck->vat_margin == "vat")
                                            &euro;  {!! number_format($truck->price, 0, ",", ".") !!} +  &euro; {!! number_format(round(($truck->price * 0.21), 0), 0, ",", ".") !!} {!! trans('trucks.fields.vat') !!} <br/>=  &euro; {!! number_format(round(($truck->price * 1.21), 0), 0, ",", ".") !!}
                                        @else
                                            &euro;  {!! number_format($truck->price, 0, ".", ".") !!}
                                        @endif
                                    </span>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="sold">{!! trans('trucks.fields.sold') !!}</div>
                                    <img height="180" width="240" class="thumbnail img-responsive" src="{!!  URL::to('/photo/240x180/images/trucks/'.$truck->id.'/'.$truck->images()->where('order', 1)->first()->filename) !!}"/>
                                </div>
                            </div>
                        </a>
                    </div><!-- panel -->

                </div><!-- col-md-10 -->

            @endforeach
        @endif
    </div><!--row-->
@endsection

@section('after-scripts')
    {!! Html::script('js/frontend/trucks/sort.js') !!}
@endsection