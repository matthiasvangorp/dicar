@extends('frontend.layouts.pdf')

@section('content')
    <div class="row">
        <div class="col-xs-12">

            <div class="panel panel-default">

                <div class="panel-body">
                    {!! trans('trucks.fields.reference') !!} : {!! $truck->reference !!}
                    @if(!empty($truck->remarks))
                        <div class="row">
                            <br/><br/>
                            <div class="col-xs-12">{!! $truck->remarks !!} <br/> </div>
                            <br/><br/>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-xs-12">
                            {!! $truck->brand->name !!} {!! $truck->name !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            @if($truck->vat_margin == "vat")
                            &euro;  {!! $truck->price !!} +  &euro; {!! round(($truck->price * 0.21), 0) !!} {!! trans('trucks.fields.vat') !!} =  &euro; {!! round(($truck->price * 1.21), 0) !!}
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            @if(!empty($truck->year))
                                <div class="row">
                                    <div class="col-xs-12">{!! trans('trucks.fields.year') !!} : {!! date_create_from_format('d/m/Y', $truck->year)->format('m/Y')!!}</div>
                                </div>
                            @endif
                            @if(!empty($truck->mileage))
                                <div class="row">
                                    <div class="col-xs-12">{{ trans('strings.frontend.mileage') }} : {!! $truck->mileage !!}</div>
                                </div>
                            @endif
                            @if(!empty($truck->engine->name))
                                <div class="row">
                                    <div class="col-xs-12"> {{ trans('strings.frontend.engine') }} : {!! $truck->engine->name !!}</div>
                                </div>
                            @endif
                            @if(!empty($truck->horsepower))
                                <div class="row">
                                    <div class="col-xs-12">{{ trans('strings.frontend.kw') }} : {!! $truck->horsepower !!}</div>
                                </div>
                            @endif
                            @if(!empty($truck->horsepower))
                                <div class="row">
                                    <div class="col-xs-12">{{ trans('strings.frontend.hp') }} : {!! round($truck->horsepower * 1.34102209, 0) !!}</div>
                                </div>
                            @endif
                            @if(!empty($truck->color))
                            <div class="row">
                                <div class="col-xs-12">{{ trans('strings.frontend.color') }} : {!! trans('strings.colors.'.$truck->color) !!}</div>
                            </div>
                            @endif
                            @if(!empty($truck->euronorm))
                            <div class="row">
                                <div class="col-xs-12">{{ trans('trucks.fields.euronorm') }} : {!! $truck->euronorm !!}</div>
                            </div>
                            @endif
                            @if(!empty($truck->seats))
                                <div class="row">
                                    <div class="col-xs-12">{{ trans('strings.frontend.seats') }} : {!! $truck->seats !!}</div>
                                </div>
                            @endif

                        </div>
                    </div>

                        @foreach($truck->getAttributes() as $key => $value)
                            @if (strpos($key, 'construction') !== false && $value)
                                @if (strpos($key, '_id') == false)
                                <div class="row">
                                    <div class="col-xs-12">{!! trans('trucks.fields.'.$key) !!}
                                    @if ($value == 1)
                                         </div>
                                    @else
                                        @if (Lang::has('strings.construction_types.'.$value, App::getLocale()))
                                            {!! trans('strings.construction_types.'.$value) !!}</div>
                                        @else
                                            : {!! $value !!}</div>
                                        @endif
                                    @endif
                                </div>
                                @else
                                    @if ($truck->construction_lift != 0)
                                        <div class="row">
                                            <div class="col-xs-12">{!! trans('trucks.fields.'.$key) !!} : {!! $truck->construction_lift . ' '. $truck->constructionLiftPower->name !!} kg</div>
                                        </div>
                                    @endif
                                @endif
                            @endif
                        @endforeach


                        @foreach($truck->getAttributes() as $key => $value)
                            @if (strpos($key, 'weights') !== false && $value)
                            <div class="row">
                                <div class="col-xs-12">{!! trans('trucks.fields.'.$key) !!}
                                @if ($value == 1)
                                     </div>
                                @else
                                    : {!! $value !!}</div>
                                @endif
                            </div>
                            @endif
                        @endforeach

                        @foreach($truck->getAttributes() as $key => $value)
                            @if (strpos($key, 'equipment') !== false && $value)
                                <div class="row">
                                    <div class="col-xs-12">{!! trans('trucks.fields.'.$key) !!}
                                    @if ($value == 1)
                                         </div>
                                    @else
                                        : {!! $value !!}</div>
                                    @endif
                                </div>
                            @endif
                        @endforeach

                        @foreach($truck->getAttributes() as $key => $value)
                            @if (strpos($key, 'delivery_terms') !== false && $value)
                                <div class="row">
                                    <div class="col-xs-12">
                                        Inclusief : {!! trans('trucks.fields.'.$key) !!}
                                    </div>
                                </div>
                            @endif
                        @endforeach

                        <div class="row">
                            <div class="col-xs-12">
                                {!! $defaultText1 !!}
                            </div>
                        </div>

                    <br/><br/>

                </div>
            </div><!-- panel -->

        </div><!-- col-md-10 -->

    </div><!--row-->

    <script type="text/javascript">

    </script>

    <style>
        .modal-dialog {}
        .thumbnail {margin-bottom:6px;}

        .carousel-control.left,.carousel-control.right{
            background-image:none;
            margin-top:10%;
            width:5%;
        }
    </style>
@endsection

@section('after-scripts')
    {{ Html::script('js/frontend/lightbox.js') }}
@endsection



