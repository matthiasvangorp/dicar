@extends('frontend.layouts.app')

@section('title', trans('titles.used').' '.$truck->brand->name.' '.$truck->name.' '.trans('titles.truck').' '.trans('titles.for_sale'))
@section('meta_description', trans('meta.looking_for').' '.$truck->brand->name.' '.$truck->name.' '.trans('meta.truck').' '.trans('meta.buy_now'))

@section('content')
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <h1 class="dicar-orange">{!! $truck->brand->name !!} {!! $truck->name !!}</h1><span class="pull-right dicar-orange"><strong>{!! trans('trucks.fields.reference') !!} : {!! $truck->reference !!}</strong></span>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    @foreach( $truck->images as $image )
                                        <li data-target="#carousel-example-generic" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                                    @endforeach
                                </ol>

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    @foreach( $truck->images as $image )
                                        <div class="item {{ $loop->first ? ' active' : '' }}" >
                                            <img src="{!! URL::to('/photo/480x360/images/trucks/'.$truck->id.'/'.$image->filename) !!}" alt="{!! $truck->brand->name !!} {!! $truck->name !!}">
                                        </div>
                                    @endforeach
                                </div>

                                <!-- Controls -->
                                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            @if(!empty($truck->year))
                                <div class="row">
                                    <div class="col-xs-6">{!! trans('trucks.fields.year') !!} </div>
                                    <div class="col-xs-6"> {!! date_create_from_format('d/m/Y', $truck->year)->format('m/Y')!!}</div>
                                </div>
                            @endif
                            @if(!empty($truck->mileage))
                                <div class="row">
                                    <div class="col-xs-6">{!! trans('strings.frontend.mileage') !!} </div>
                                    <div class="col-xs-6"> {!! $truck->mileage !!}</div>
                                </div>
                            @endif
                            @if(!empty($truck->horsepower))
                                <div class="row">
                                    <div class="col-xs-6">{!! trans('strings.frontend.horsepower') !!} </div>
                                    <div class="col-xs-6"> {!! $truck->horsepower !!} / {!! round($truck->horsepower * 1.34102209, 0) !!}</div>
                                </div>
                            @endif
                            @if(!empty($truck->engine->name))
                                <div class="row">
                                    <div class="col-xs-6"> {!! trans('strings.frontend.engine') !!} </div>
                                    <div class="col-xs-6"> {!! $truck->engine->name !!}</div>
                                </div>
                            @endif
                            @if(!empty($truck->color))
                                <div class="row">
                                    <div class="col-xs-6">{!! trans('strings.frontend.color') !!} </div>
                                    <div class="col-xs-6"> {!! trans('strings.colors.'.$truck->color) !!}</div>
                                </div>
                            @endif
                            @if(!empty($truck->euronorm))
                                <div class="row">
                                    <div class="col-xs-6">{!! trans('trucks.fields.euronorm') !!} </div>
                                    <div class="col-xs-6"> {!! $truck->euronorm !!}</div>
                                </div>
                            @endif
                            @if(!empty($truck->used))
                                <div class="row">
                                    <div class="col-xs-6">{!! trans('strings.frontend.used') !!} </div>
                                    <div class="col-xs-6"> <i class="fa fa-check" aria-hidden="true" style="color:green"></i></div>
                                </div>
                            @endif
                            @if(!empty($truck->seats))
                                <div class="row">
                                    <div class="col-xs-6">{!! trans('strings.frontend.seats') !!} </div>
                                    <div class="col-xs-6"> {!! $truck->seats !!}</div>
                                </div>

                            @endif
                            <br/><br/>
                            <div class="row">
                                <div class="col-xs-12">
                                   <span class="price" style="font-size: 20px;">
                                        @if($truck->vat_margin == "vat")
                                            &euro;  {!! number_format($truck->price, 2, ",", ".") !!} +  &euro; {!! number_format(round(($truck->price * 0.21), 0),  2, ",", ".") !!} {!! trans('trucks.fields.vat') !!} =  &euro; {!! number_format(round(($truck->price * 1.21), 0), 2, ",", ".") !!}
                                        @else
                                           &euro;  {!! number_format($truck->price, 2, ",", ".") !!}
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <br/><br/><a href="#" class="btn btn-small btn-danger" id="interested_button">{!! trans('strings.frontend.interested') !!}</a>

                        </div>
                    </div>
                    <div class="row">&nbsp;</div>

                    <div class="row">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">{!! trans('strings.backend.general.general') !!}</a></li>
                            <li><a href="#tab2default" data-toggle="tab">{!! trans('strings.backend.general.information') !!}</a></li>
                            <li><a href="#tab3default" data-toggle="tab">{!! trans('strings.backend.general.images') !!}</a></li>
                            <li><a href="#tab4default" data-toggle="tab" id="interested_tab">{!! trans('strings.backend.general.interested') !!}</a></li>
                        </ul>
                    </div>



                </div>

                <div class="panel-body">
                    <div class="tab-content">
                    @role('Executive')
                    <div class="row">
                        <div class="pull-right">
                            <a href="{!! URL::to('export/' . $truck->id) !!}" title="Export 2dehands">
                                <i class="fa fa-file" aria-hidden="true"></i>
                            </a>
                            <a href="{!! URL::to('pdf/' . $truck->id) !!}" title="Vensterblad maken">
                                <i class="fa fa-map" aria-hidden="true"></i>
                            </a>
                            <a href="{!! URL::to('admin/trucks/' . $truck->id . '/edit') !!}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>

                        </div>
                    </div>
                    @endauth
                    <div class="tab-pane fade in active" id="tab1default">
                        @if(!empty($truck->year))
                            <div class="row">
                                <div class="col-xs-7">{!! trans('trucks.fields.year') !!} </div>
                                <div class="col-xs-5"> {!! date_create_from_format('d/m/Y', $truck->year)->format('m/Y')!!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->mileage))
                            <div class="row">
                                <div class="col-xs-7">{!! trans('strings.frontend.mileage') !!} </div>
                                <div class="col-xs-5"> {!! $truck->mileage !!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->horsepower))
                            <div class="row">
                                <div class="col-xs-7">{!! trans('strings.frontend.horsepower') !!} </div>
                                <div class="col-xs-5"> {!! $truck->horsepower !!} / {!! round($truck->horsepower * 1.34102209, 0) !!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->engine->name))
                            <div class="row">
                                <div class="col-xs-7"> {!! trans('strings.frontend.engine') !!} </div>
                                <div class="col-xs-5"> {!! $truck->engine->name !!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->color))
                            <div class="row">
                                <div class="col-xs-7">{!! trans('strings.frontend.color') !!} </div>
                                <div class="col-xs-5"> {!! trans('strings.colors.'.$truck->color) !!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->euronorm))
                            <div class="row">
                                <div class="col-xs-7">{!! trans('trucks.fields.euronorm') !!} </div>
                                <div class="col-xs-5"> {!! $truck->euronorm !!}</div>
                            </div>
                        @endif

                        @if(!empty($truck->seats))
                            <div class="row">
                                <div class="col-xs-7">{!! trans('strings.frontend.seats') !!} </div>
                                <div class="col-xs-5"> {!! $truck->seats !!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->remarks))
                        <div class="row">
                            <br/><br/>
                            <div class="col-xs-12">{!! $truck->remarks !!} <br/> </div>
                        </div>
                        @endif
                        <br/>
                        <div class="row">
                            <div class="col-xs-12">
                                {!! $defaultText1 !!}
                            </div>
                        </div>
                    </div>


                    <div class="tab-pane fade" id="tab2default">
                        <h2 class="dicar-orange">{!! trans('trucks.fields.construction') !!}</h2>

                            @foreach($truck->getAttributes() as $key => $value)
                                @if (strpos($key, 'construction') !== false && $value)
                                    @if (strpos($key, '_id') == false)
                                    <div class="row">
                                        <div class="col-xs-7">{!! trans('trucks.fields.'.$key) !!}</div>
                                        @if ($value == 1)
                                            <div class="col-xs-5"> <i class="fa fa-check" aria-hidden="true" style="color:green"></i></div>
                                        @else
                                            @if (Lang::has('strings.construction_types.'.$value, App::getLocale()))
                                                <div class="col-xs-5">{!! trans('strings.construction_types.'.$value) !!}</div>
                                            @else
                                                <div class="col-xs-5">{!! $value !!}</div>
                                            @endif
                                        @endif
                                    </div>
                                    @else
                                        @if ($truck->construction_lift != 0)
                                            <div class="row">
                                                <div class="col-xs-7">{!! trans('trucks.fields.'.$key) !!}</div>
                                                <div class="col-xs-5">{!! $truck->constructionLiftPower->name !!} kg</div>
                                            </div>
                                        @endif
                                    @endif
                                @endif
                            @endforeach


                        <h2 class="dicar-orange">{!! trans('trucks.fields.weights') !!}</h2>

                            @foreach($truck->getAttributes() as $key => $value)
                                @if (strpos($key, 'weights') !== false && $value)
                                <div class="row">
                                    <div class="col-xs-7">{!! trans('trucks.fields.'.$key) !!}</div>
                                    @if ($value == 1)
                                        <div class="col-xs-5"> <i class="fa fa-check" aria-hidden="true" style="color:green"></i></div>
                                    @else
                                        <div class="col-xs-5">{!! $value !!}</div>
                                    @endif
                                </div>
                                @endif
                            @endforeach

                        <h2 class="dicar-orange">{!! trans('trucks.fields.options') !!}</h2>
                            @foreach($truck->getAttributes() as $key => $value)
                                @if (strpos($key, 'equipment') !== false && $value)
                                    <div class="row">
                                        <div class="col-xs-7">{!! trans('trucks.fields.'.$key) !!}</div>
                                        @if ($value == 1)
                                            <div class="col-xs-5"> <i class="fa fa-check" aria-hidden="true" style="color:green"></i></div>
                                        @else
                                            <div class="col-xs-5">{!! $value !!}</div>
                                        @endif
                                    </div>
                                @endif
                            @endforeach

                        <h2 class="dicar-orange">{!! trans('trucks.fields.delivery_terms') !!}</h2>
                            @foreach($truck->getAttributes() as $key => $value)
                                @if (strpos($key, 'delivery_terms') !== false && $value)
                                    <div class="row">
                                        <div class="col-xs-7">{!! trans('trucks.fields.'.$key) !!}</div>
                                        @if ($value == 1)
                                            <div class="col-xs-5"> <i class="fa fa-check" aria-hidden="true" style="color:green"></i></div>
                                        @else
                                            <div class="col-xs-5">{!! $value !!}</div>
                                        @endif
                                    </div>
                                @endif
                            @endforeach
                            <div class="row">
                                <div class="col-xs-12">
                                    <br/>
                                    {!! $defaultText2 !!}
                                </div>
                            </div>
                    </div>
                    <div class="tab-pane fade" id="tab3default">
                        <div class="row">
                            @foreach($truck->images as $image)
                                <div class="col-sm-3 col-xs-6">
                                    <a href="#lightbox" data-toggle="modal" title="{!! $truck->brand->name !!} {!! $truck->name !!}" href="#">
                                        <img src="{!!  URL::to('/images/trucks/'.$truck->id.'/'.$image->filename) !!}"  width="320" height="240" style="margin-bottom:20px; border-color: #dc652c; height: 100%">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab4default">
                        <h2 class="dicar-orange">{!! trans('strings.sell.contact') !!} <span class="pull-right"><i class="fa fa-phone fa-1x"></i> +32 (0)494 52 95 17</span></h2>


                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>

                        {!! Form::open(['url' => App::getLocale().'/contact_us', 'method' =>  'POST', 'id' => 'contact_form' ]) !!}
                        {!! Form::hidden('truck', $truck->brand->name. ' ' .  $truck->name)!!}
                        {!! Form::hidden('reference', $truck->reference)!!}

                        <div class="form-group">
                            {!! Form::label(trans('strings.sell.name')) !!} <sup class="required"> <i class="fa fa-asterisk required" aria-hidden="true"></i></sup>
                            {!! Form::text('name', null,
                                array('required',
                                      'class'=>'form-control',
                                      'placeholder'=>trans('strings.sell.name'))) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label(trans('strings.sell.email')) !!} <sup class="required"> <i class="fa fa-asterisk required" aria-hidden="true"></i></sup>
                            {!! Form::text('email', null,
                                array('required',
                                      'class'=>'form-control',
                                      'placeholder'=>trans('strings.sell.email'))) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label(trans('strings.sell.phone')) !!}
                            {!! Form::text('phone', null,
                                array(
                                      'class'=>'form-control',
                                      'placeholder'=>trans('strings.sell.phone'))) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label(trans('strings.contact.message')) !!}
                            {!! Form::textarea('remarks', null,
                                array(
                                      'class'=>'form-control',
                                      'placeholder'=>trans('strings.contact.message'))) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit(trans('strings.sell.send'),
                              array('class'=>'btn btn-primary', 'id' => 'contact_button')) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div><!-- panel -->
            </div>
        </div>

    <div class="modal" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal">×</button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">
                    <div id="modalCarousel" class="carousel">

                        <div class="carousel-inner">

                        </div>

                        <a class="carousel-control left" href="#modaCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                        <a class="carousel-control right" href="#modalCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>

                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">{!! trans('strings.frontend.close') !!}</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    {!! Html::script('js/frontend/lightbox.js') !!}
    {!! Html::script('js/frontend/trucks/custom.js') !!}
@endsection



