@extends('frontend.layouts.app')

@section('title', trans('titles.used').' '.trans('titles.trucks').' '.trans('titles.for_sale'))
@section('meta_description', trans('meta.homepage'))

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>{!! trans('strings.frontend.secondhand_trucks') !!}</h1>
        </div>

        <div class="col-xs-12">
            {!!Html::ul($errors->all()) !!}
            {!! Form::open(['url' =>LaravelLocalization::getLocalizedURL(null, '/'), 'files' => false, 'method' =>  'POST', 'id' => 'filter']) !!}


            <div class="form-group col-md-3">
                {!! Form::label('brand', trans('trucks.fields.brand')) !!}
                {!! Form::select('brand', $brands, $selectedBrand, array('class' => 'form-control')) !!}
                {!! Form::hidden('sortBy', 'updated_at', array('id' => 'sortBy'))!!}
            </div>
            <div class="form-group col-md-3">
                {!! Form::label('euronorm', trans('trucks.fields.euronorm').' > ') !!}
                {!! Form::select('euronorm', ['0' => trans('trucks.fields.no_preference'), '3' => '3', '4' => '4', '5' => '5', '6' => '6'], $selectedEuronorm, array('class' => 'form-control')) !!}
            </div>
            <div class="form-group col-md-3">
                <br/>
                {!! Form::submit(trans('strings.frontend.filter'), array('class' => 'btn btn-primary')) !!}

            </div>
            {!! Form::close() !!}
            <div class="form-group col-md-3 pull-right">
                {!! Form::open(['url' =>LaravelLocalization::getLocalizedURL(null, '/'), 'files' => false, 'method' =>  'POST']) !!}
                {!! Form::label('sort', trans('strings.frontend.sort_by'))  !!}
                {!! Form::select('sort', $sort, $selectedSort, array('class' => 'form-control')) !!}
                {!! Form::close() !!}
            </div>
        </div>
        @foreach ($trucks as $truck)

        <div class="col-xs-12">

            <br/><br/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-truck dicar-orange" aria-hidden="true"></i>
                    <strong>
                        <a class="dicar-orange" href="{!!  URL::to('trucks/'.$truck->id) !!}">{!! $truck->brand->name !!} {!! $truck->name !!} {!! $truck->engine->name !!} {!! $truck->construction !!} </a>
                    </strong>
                    @role('Executive')
                    <div class="pull-right">
                        <a href="{!! URL::to('export/' . $truck->id) !!}" title="Export 2dehands">
                            <i class="fa fa-file" aria-hidden="true"></i>
                        </a>
                        <a href="{!! URL::to('pdf/' . $truck->id) !!}" title="Vensterblad maken">
                            <i class="fa fa-map" aria-hidden="true"></i>
                        </a>
                        <a href="{!! URL::to('admin/trucks/' . $truck->id . '/edit') !!}">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                    </div>
                    @endauth
                </div>

                <a href="{!!  URL::to('trucks/'.$truck->id) !!}">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                        @if(!empty($truck->mileage))
                            <div class="row">
                                <div class="col-xs-6">{!! trans('strings.frontend.mileage') !!} </div>
                                <div class="col-xs-6"> {!! $truck->mileage !!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->euronorm))
                            <div class="row">
                                <div class="col-xs-6">{!! trans('trucks.fields.euronorm') !!} </div>
                                <div class="col-xs-6"> {!! $truck->euronorm !!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->delivery_terms_guarantee && $truck->delivery_terms_guarantee))
                            <div class="row">
                                <div class="col-xs-6">{!! trans('trucks.fields.delivery_terms_guarantee') !!} </div>
                                <div class="col-xs-6"> <i class="fa fa-check" aria-hidden="true" style="color:green"></i></div>
                            </div>
                        @endif
                        @if(!empty($truck->engine))
                            <div class="row">
                                <div class="col-xs-6">{!! trans('strings.frontend.engine') !!} </div>
                                <div class="col-xs-6"> {!! $truck->engine->name !!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->horsepower))
                            <div class="row">
                                <div class="col-xs-6">{!! trans('strings.frontend.horsepower') !!} </div>
                                <div class="col-xs-6">{!! $truck->horsepower !!} / {!! round($truck->horsepower * 1.34102209, 0)!!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->color))
                            <div class="row">
                                <div class="col-xs-6">{!! trans('strings.frontend.color') !!} </div>
                                <div class="col-xs-6">{!! trans('strings.colors.'.$truck->color) !!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->seats))
                            <div class="row">
                                <div class="col-xs-6">{!! trans('strings.frontend.seats') !!} </div>
                                <div class="col-xs-6"> {!! $truck->seats !!}</div>
                            </div>
                        @endif
                        @if(!empty($truck->year))
                            <div class="row">
                                <div class="col-xs-6">{!! trans('trucks.fields.year') !!} </div>
                                <div class="col-xs-6"> {!! date_create_from_format('Y-m-d', $truck->year)->format('m/Y')!!}</div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-6">
                                <strong>{!! trans('trucks.fields.reference') !!} </strong>
                            </div>
                            <div class="col-xs-6">
                                <strong>{!! $truck->reference !!}</strong>
                            </div>
                        </div>

                    </div>
                        <div class="col-sm-6 col-md-3">
                        <span class="price" style="font-size: 20px;">
                            @if($truck->vat_margin == "vat")
                                &euro;  {!! $truck->price !!} +  &euro; {!! round(($truck->price * 0.21), 0) !!} {!! trans('trucks.fields.vat') !!} <br/>=  &euro; {!! round(($truck->price * 1.21), 0) !!}
                            @else
                                &euro;  {!! $truck->price !!} particulier/geen btw te betalen
                            @endif
                        </span>
                    </div>
                        <div class="col-sm-6 col-md-3">
                        @if ($truck->images() && $truck->images()->where('order', 1)->first())
                            <img height="180" width="240" class="thumbnail img-responsive" src="{!!  URL::to('/images/trucks/'.$truck->id.'/'.$truck->images()->where('order', 1)->first()->filename) !!}"/>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <p>{!! $truck->remarks !!}</p>
                    </div>
                </div>
                </div>
                </a>
            </div><!-- panel -->

        </div><!-- col-md-10 -->

        @endforeach



        @if (isset($soldTrucks))
            @foreach ($soldTrucks as $truck)

                <div class="col-xs-12">

                    <br/><br/>
                    <div class="panel panel-default">

                        <div class="panel-heading">
                            <i class="fa fa-truck dicar-orange" aria-hidden="true"></i>
                            <strong>
                                <a class="dicar-orange" href="{!!  URL::to('trucks/'.$truck->id) !!}">{!! $truck->brand->name !!} {!! $truck->name !!} {!! $truck->engine->name !!} {!! $truck->construction !!} </a>
                            </strong>
                            @role('Executive')
                            <div class="pull-right">
                                <a href="{!! URL::to('admin/trucks/' . $truck->id . '/edit') !!}">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                            </div>
                            @endauth
                        </div>

                        <a href="{!!  URL::to('trucks/'.$truck->id) !!}">
                            <div class="panel-body" style="overflow:hidden">

                                <div class="col-sm-6 col-md-6">
                                        @if(!empty($truck->seats))
                                            <div class="row">

                                                <div class="col-xs-6">{!! trans('strings.frontend.seats') !!} </div>
                                                <div class="col-xs-6">{!! $truck->seats !!}</div>
                                            </div>
                                        @endif
                                        @if(!empty($truck->engine))
                                            <div class="row">
                                                <div class="col-xs-6">{!! trans('strings.frontend.engine') !!} </div>
                                                <div class="col-xs-6">{!! $truck->engine->name !!}</div>
                                            </div>
                                        @endif
                                        @if(!empty($truck->horsepower))
                                            <div class="row">
                                                <div class="col-xs-6">{!! trans('strings.frontend.horsepower') !!} </div>
                                                <div class="col-xs-6"> {!! $truck->horsepower !!} / {!! round($truck->horsepower * 1.34102209, 0)!!}</div>
                                            </div>
                                        @endif
                                        @if(!empty($truck->color))
                                            <div class="row">
                                                <div class="col-xs-6">{!! trans('strings.frontend.color') !!} </div>
                                                <div class="col-xs-6">{!! trans('strings.colors.'.$truck->color) !!}</div>
                                            </div>
                                        @endif
                                        @if(!empty($truck->mileage))
                                            <div class="row">
                                                <div class="col-xs-6">{!! trans('strings.frontend.mileage') !!} </div>
                                                <div class="col-xs-6">{!! $truck->mileage !!}</div>
                                            </div>
                                        @endif
                                        @if(!empty($truck->euronorm))
                                            <div class="row">
                                                <div class="col-xs-6">{!! trans('trucks.fields.euronorm') !!} </div>
                                                <div class="col-xs-6"> {!! $truck->euronorm !!}</div>
                                            </div>
                                        @endif
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <strong>{!! trans('trucks.fields.reference') !!} </strong>
                                            </div>
                                            <div class="col-xs-6">
                                                <strong>{!! $truck->reference !!}</strong>
                                            </div>
                                        </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <span class="price" style="font-size: 20px;">
                                        @if($truck->vat_margin == "vat")
                                            &euro;  {!! number_format($truck->price, 0, ",", ".") !!} +  &euro; {!! number_format(round(($truck->price * 0.21), 0), 0, ",", ".") !!} {!! trans('trucks.fields.vat') !!} <br/>=  &euro; {!! number_format(round(($truck->price * 1.21), 0), 0, ",", ".") !!}
                                        @else
                                            &euro;  {!! number_format($truck->price, 0, ".", ".") !!}
                                        @endif
                                    </span>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="sold">{!! trans('trucks.fields.sold') !!}</div>
                                    @if ($truck->images() && $truck->images()->where('order', 1)->first())
                                        <img height="180" width="240" class="thumbnail img-responsive" src="{!!  URL::to('/images/trucks/'.$truck->id.'/'.$truck->images()->where('order', 1)->first()->filename) !!}"/>
                                    @endif
                                </div>
                            </div>
                        </a>
                    </div><!-- panel -->

                </div><!-- col-md-10 -->

            @endforeach
        @endif
    </div><!--row-->
@endsection

@section('after-scripts')
    {!! Html::script('js/frontend/trucks/sort.js') !!}
@endsection