@extends('frontend.layouts.pdf')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1 class="dicar-orange">{!! $truck->brand->name !!} {!! $truck->name !!}</h1>
            <span class="pull-right">{!! trans('trucks.fields.reference') !!} : {!! $truck->reference !!}</span>
        </div>
        <div class="col-xs-12">

            <div class="panel panel-default">

                <div class="panel-body">

                    <div class="row">
                        <div class="col-xs-8">
                            @if(!empty($truck->year))
                                <div class="row">
                                    <div class="col-xs-4">{!! trans('trucks.fields.year') !!} </div>
                                    <div class="col-xs-4"> {!! date_create_from_format('d/m/Y', $truck->year)->format('m/Y')!!}</div>
                                </div>
                            @endif
                            @if(!empty($truck->mileage))
                                <div class="row">
                                    <div class="col-xs-4">{{ trans('strings.frontend.mileage') }} </div>
                                    <div class="col-xs-4"> {!! $truck->mileage !!}</div>
                                </div>
                            @endif
                            @if(!empty($truck->engine->name))
                                <div class="row">
                                    <div class="col-xs-4"> {{ trans('strings.frontend.engine') }} </div>
                                    <div class="col-xs-4"> {!! $truck->engine->name !!}</div>
                                </div>
                            @endif
                            @if(!empty($truck->horsepower))
                                <div class="row">
                                    <div class="col-xs-4">{{ trans('strings.frontend.kw') }} </div>
                                    <div class="col-xs-4"> {!! $truck->horsepower !!}</div>
                                </div>
                            @endif
                            @if(!empty($truck->horsepower))
                                <div class="row">
                                    <div class="col-xs-4">{{ trans('strings.frontend.hp') }} </div>
                                    <div class="col-xs-4"> {!! round($truck->horsepower * 1.34102209, 0) !!}</div>
                                </div>
                            @endif
                            @if(!empty($truck->color))
                            <div class="row">
                                <div class="col-xs-4">{{ trans('strings.frontend.color') }} </div>
                                <div class="col-xs-4"> {!! trans('strings.colors.'.$truck->color) !!}</div>
                            </div>
                            @endif
                            @if(!empty($truck->euronorm))
                            <div class="row">
                                <div class="col-xs-4">{{ trans('trucks.fields.euronorm') }} </div>
                                <div class="col-xs-4"> {!! $truck->euronorm !!}</div>
                            </div>
                            @endif
                            @if(!empty($truck->seats))
                                <div class="row">
                                    <div class="col-xs-4">{{ trans('strings.frontend.seats') }} </div>
                                    <div class="col-xs-4"> {!! $truck->seats !!}</div>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-xs-8" style="font-size:16px">
                                    <strong>
                                        &euro;  {!! $truck->price !!} +  &euro; {!! round(($truck->price * 0.21), 0) !!} {!! trans('trucks.fields.vat') !!} =  &euro; {!! round(($truck->price * 1.21), 0) !!}
                                    </strong>
                                </div>
                            </div>


                        </div>
                        <div class="col-xs-4">
                            @if(!empty($truck->images()->where('order', 1)->first()))
                                <img src="{!!  URL::to('/photo/300x225/images/trucks/'.$truck->id.'/'.$truck->images()->where('order', 1)->first()->filename) !!}" style="margin-left:-100px;"/>
                            @endif
                        </div>
                    </div>
                        @if(!empty($truck->remarks))
                            <div class="row">
                                <br/><br/>
                                <div class="col-xs-12">{!! $truck->remarks !!} <br/> </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-12">
                                {!! $defaultText1 !!}
                            </div>
                        </div>

                    <h2 class="dicar-orange">{!! trans('trucks.fields.construction') !!}</h2>

                        @foreach($truck->getAttributes() as $key => $value)
                            @if (strpos($key, 'construction') !== false && $value)
                                @if (strpos($key, '_id') == false)
                                <div class="row">
                                    <div class="col-xs-7">{!! trans('trucks.fields.'.$key) !!}</div>
                                    @if ($value == 1)
                                        <div class="col-xs-5"> <i class="fa fa-check" aria-hidden="true" style="color:green"></i></div>
                                    @else
                                        @if (Lang::has('strings.construction_types.'.$value, App::getLocale()))
                                            <div class="col-xs-5">{!! trans('strings.construction_types.'.$value) !!}</div>
                                        @else
                                            <div class="col-xs-5">{!! $value !!}</div>
                                        @endif
                                    @endif
                                </div>
                                @else
                                    @if ($truck->construction_lift != 0)
                                        <div class="row">
                                            <div class="col-xs-7">{!! trans('trucks.fields.'.$key) !!}</div>
                                            <div class="col-xs-5">{!! $truck->construction_lift . ' '. $truck->constructionLiftPower->name !!} kg</div>
                                        </div>
                                    @endif
                                @endif
                            @endif
                        @endforeach


                    <h2 class="dicar-orange">{!! trans('trucks.fields.weights') !!}</h2>

                        @foreach($truck->getAttributes() as $key => $value)
                            @if (strpos($key, 'weights') !== false && $value)
                            <div class="row">
                                <div class="col-xs-7">{!! trans('trucks.fields.'.$key) !!}</div>
                                @if ($value == 1)
                                    <div class="col-xs-5"> <i class="fa fa-check" aria-hidden="true" style="color:green"></i></div>
                                @else
                                    <div class="col-xs-5">{!! $value !!}</div>
                                @endif
                            </div>
                            @endif
                        @endforeach

                    <h2 class="dicar-orange">{!! trans('trucks.fields.options') !!}</h2>
                        @foreach($truck->getAttributes() as $key => $value)
                            @if (strpos($key, 'equipment') !== false && $value)
                                <div class="row">
                                    <div class="col-xs-7">{!! trans('trucks.fields.'.$key) !!}</div>
                                    @if ($value == 1)
                                        <div class="col-xs-5"> <i class="fa fa-check" aria-hidden="true" style="color:green"></i></div>
                                    @else
                                        <div class="col-xs-5">{!! $value !!}</div>
                                    @endif
                                </div>
                            @endif
                        @endforeach

                    <h2 class="dicar-orange">{!! trans('trucks.fields.delivery_terms') !!}</h2>
                        @foreach($truck->getAttributes() as $key => $value)
                            @if (strpos($key, 'delivery_terms') !== false && $value)
                                <div class="row">
                                    <div class="col-xs-7">{!! trans('trucks.fields.'.$key) !!}</div>
                                    @if ($value == 1)
                                        <div class="col-xs-5"> <i class="fa fa-check" aria-hidden="true" style="color:green"></i></div>
                                    @else
                                        <div class="col-xs-5">{!! $value !!}</div>
                                    @endif
                                </div>
                            @endif
                        @endforeach
                        <div class="row">
                            <div class="col-xs-12">
                                <br/>
                                {!! $defaultText2 !!}
                            </div>
                        </div>

                    <br/><br/>

                </div>
            </div><!-- panel -->

        </div><!-- col-md-10 -->
        <span class="pull-right">{!! trans('trucks.fields.reference') !!} : {!! $truck->reference !!}</span>
    </div><!--row-->

    <script type="text/javascript">

    </script>

    <style>
        .modal-dialog {}
        .thumbnail {margin-bottom:6px;}

        .carousel-control.left,.carousel-control.right{
            background-image:none;
            margin-top:10%;
            width:5%;
        }
    </style>
@endsection

@section('after-scripts')
    {{ Html::script('js/frontend/lightbox.js') }}
@endsection



