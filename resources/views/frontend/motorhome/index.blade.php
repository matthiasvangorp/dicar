@extends('frontend.layouts.app')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>TEST 123</h1>
        </div>
        @foreach ($motorhomes as $motorhome)
        <div class="col-xs-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-home"></i> {!! $motorhome->name !!}
                </div>

                <div class="panel-body">
                    {{ trans('strings.frontend.sleepingPlaces') }} : {!! $motorhome->sleepingPlaces !!}<br/>
                    {{ trans('strings.frontend.seats') }} : {!! $motorhome->seats !!}<br/>

                    @foreach ($motorhome->images as $image)
                        <img src="{!!  URL::to('/photo/250x250/images/motorhomes/'.$motorhome->id.'/'.$image) !!}"/>
                    @endforeach
                </div>
            </div><!-- panel -->

        </div><!-- col-md-10 -->
        @endforeach

    </div><!--row-->
@endsection