@extends('frontend.layouts.app')

@section('content')
    <div class="row">

        <div class="col-sm-12">
            <h1>{!! trans('strings.sell.sell_your_truck') !!}</h1>

            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>

            {!! Form::open(['url' => '/sell_your_truck', 'method' =>  'POST']) !!}

            <div class="form-group">
                {!! Form::label(trans('strings.sell.name')) !!} <sup class="required"> <i class="fa fa-asterisk required" aria-hidden="true"></i></sup>
                {!! Form::text('name', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>trans('strings.sell.name'))) !!}
            </div>

            <div class="form-group">
                {!! Form::label(trans('strings.sell.email')) !!} <sup class="required"> <i class="fa fa-asterisk required" aria-hidden="true"></i></sup>
                {!! Form::text('email', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>trans('strings.sell.email'))) !!}
            </div>

            <div class="form-group">
                {!! Form::label(trans('strings.sell.address')) !!}
                {!! Form::text('address', null,
                    array(
                          'class'=>'form-control',
                          'placeholder'=>trans('strings.sell.address'))) !!}
            </div>

            <div class="form-group">
                {!! Form::label(trans('strings.sell.zipcode')) !!}
                {!! Form::text('zipcode', null,
                    array(
                          'class'=>'form-control',
                          'placeholder'=>trans('strings.sell.zipcode'))) !!}
            </div>

            <div class="form-group">
                {!! Form::label(trans('strings.sell.city')) !!}
                {!! Form::text('city', null,
                    array(
                          'class'=>'form-control',
                          'placeholder'=>trans('strings.sell.city'))) !!}
            </div>

            <div class="form-group">
                {!! Form::label(trans('strings.sell.phone')) !!}
                {!! Form::text('phone', null,
                    array(
                          'class'=>'form-control',
                          'placeholder'=>trans('strings.sell.phone'))) !!}
            </div>

            <div class="form-group">
                {!! Form::label(trans('strings.sell.brand')) !!}
                {!! Form::text('brand', null,
                    array(
                          'class'=>'form-control',
                          'placeholder'=>trans('strings.sell.brand'))) !!}
            </div>

            <div class="form-group">
                {!! Form::label(trans('strings.sell.model')) !!}
                {!! Form::text('model', null,
                    array(
                          'class'=>'form-control',
                          'placeholder'=>trans('strings.sell.model'))) !!}
            </div>

            <div class="form-group">
                {!! Form::label(trans('strings.sell.year')) !!}
                {!! Form::text('year', null,
                    array(
                          'class'=>'form-control',
                          'placeholder'=>trans('strings.sell.year'))) !!}
            </div>

            <div class="form-group">
                {!! Form::label(trans('strings.sell.kilometers')) !!}
                {!! Form::text('kilometers', null,
                    array(
                          'class'=>'form-control',
                          'placeholder'=>trans('strings.sell.kilometers'))) !!}
            </div>

            <div class="form-group">
                {!! Form::checkbox('maintenance_book', null, false,
                    array(
                          'placeholder'=>trans('strings.sell.maintenance_book'))) !!}
                {!! Form::label(trans('strings.sell.maintenance_book')) !!}
            </div>

            <div class="form-group">
                {!! Form::checkbox('accident_free', null, false,
                    array(
                          'placeholder'=>trans('strings.sell.accident_free'))) !!}
                {!! Form::label(trans('strings.sell.accident_free')) !!}
            </div>

            <div class="form-group">
                {!! Form::label(trans('strings.sell.horsepower')) !!}
                {!! Form::text('horsepower', null,
                    array(
                          'class'=>'form-control',
                          'placeholder'=>trans('strings.sell.horsepower'))) !!}
            </div>

            <div class="form-group">
                {!! Form::label(trans('strings.sell.color')) !!}
                {!! Form::text('color', null,
                    array(
                          'class'=>'form-control',
                          'placeholder'=>trans('strings.sell.color'))) !!}
            </div>

            <div class="form-group">
                {!! Form::label(trans('strings.sell.asking_price')) !!}
                {!! Form::text('asking_price', null,
                    array(
                          'class'=>'form-control',
                          'placeholder'=>trans('strings.sell.asking_price'))) !!}
            </div>

            <div class="form-group">
                {!! Form::checkbox('vat_inclusive', null, false,
                    array(
                          'placeholder'=>trans('strings.sell.vat_inclusive'))) !!}
                {!! Form::label(trans('strings.sell.vat_inclusive')) !!}
            </div>

            <div class="form-group">
                {!! Form::checkbox('private', null, false,
                    array(
                          'placeholder'=>trans('strings.sell.private'))) !!}
                {!! Form::label(trans('strings.sell.private')) !!}
            </div>

            <div class="form-group">
                {!! Form::label(trans('strings.sell.options')) !!}
                {!! Form::textarea('options', null,
                    array(
                          'class'=>'form-control',
                          'placeholder'=>trans('strings.sell.options'))) !!}
            </div>

            <div class="form-group">
                {!! Form::label(trans('strings.sell.remarks')) !!}
                {!! Form::textarea('remarks', null,
                    array(
                          'class'=>'form-control',
                          'placeholder'=>trans('strings.sell.remarks'))) !!}
            </div>



            <div class="form-group">
                {!! Form::submit(trans('strings.sell.send'),
                  array('class'=>'btn btn-primary')) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script('js/frontend/lightbox.js') }}
@endsection



