@extends('frontend.layouts.app')

@section('content')
    <div class="row">

        <div class="col-sm-12">
            <h1>{!! trans('strings.sell.thanks_for_contacting_us') !!}</h1>

        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script('js/frontend/lightbox.js') }}
@endsection



