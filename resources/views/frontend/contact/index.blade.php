@extends('frontend.layouts.app')

@section('content')
    <div class="row">

        <div class="col-sm-6">
            <div class="well">
                <h3 style="line-height:20%;"><i class="fa fa-home fa-1x" style="line-height:6%;color:#dc652c"></i> {!! trans('strings.contact.address') !!}:</h3>
                <address>
                    <strong>{!! trans('strings.contact.dicar_trucks') !!}</strong><br/>
                    <a href="http://maps.apple.com/?ll=51.1265,4.9405">Grote steenweg 2<br/>
                    B-2440 Geel<br/>
                    {!! trans('strings.contact.belgium') !!}</a>
                </address>
                {!! trans('strings.contact.directions') !!}
                <h3 style="line-height:20%;"><i class="fa fa-phone fa-1x" style="line-height:6%;color:#dc652c"></i> {!! trans('strings.contact.phone') !!}:</h3>
                <p style="margin-top:6%;"><!--+32 (0) 014 579995><br/-->
                <!--+32 (0) 494 52 83 98-->
                +32 (0)494 52 95 17<br/></p>
                <h3 style="line-height:20%;"><i class="fa fa-envelope fa-1x" style="line-height:6%;color:#dc652c"></i> {!! trans('strings.contact.email') !!}:</h3>
                <p><a href="mailto:maarten@dicar.be">maarten@dicar.be</a></p>
                <h3 style="line-height:20%;"><i class="fa fa-user fa-1x" style="line-height:6%;color:#dc652c"></i> {!! trans('strings.contact.contactperson') !!}:</h3>
                <p>Maarten Goossens</p>

                <h3 style="line-height:20%;"><i class="fa fa-clock-o fa-1x" style="line-height:6%;color:#dc652c"></i> {!! trans('strings.contact.hours') !!}:</h3>
                <p>
                    {!! trans('strings.contact.monday') !!}<br/>
                    {!! trans('strings.contact.tuesday') !!}<br/>
                    {!! trans('strings.contact.wednesday') !!}<br/>
                    {!! trans('strings.contact.thursday') !!}<br/>
                    {!! trans('strings.contact.friday') !!}<br/>
                    {!! trans('strings.contact.appointment') !!}<br/>
                </p>
                <br />
            </div>
        </div>
        <div class="col-sm-6">
            <div class="google-maps">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d80128.49205466942!2d4.872857442524661!3d51.126624400000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c14edca448fa55%3A0xc37dee9b4d3a5245!2sDicar+Vrachtwagens!5e0!3m2!1snl!2sbe!4v1501503838308" width="600" height="575" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script('js/frontend/lightbox.js') }}
@endsection



