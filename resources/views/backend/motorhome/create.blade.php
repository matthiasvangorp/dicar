@extends('frontend.layouts.app')

@section('content')
    <div class="row">

        @role('Executive')
            <div class="row">
                {!!Html::ul($errors->all()) !!}
                <H1>Create product</H1>
                {!! Form::open(['url' => '/admin/motorhomes', 'files' => true, 'method' =>  'POST']) !!}

                <div class="form-group">
                    {{ Form::label('name', 'Name', ['class' => 'col-lg-2 control-label']) }}
                    {{ Form::text('name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('sleepingPlaces', 'Sleeping places', ['class' => 'col-lg-2 control-label']) }}
                    {{ Form::text('sleepingPlaces', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required']) }}
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('seats', 'Seats', ['class' => 'col-lg-2 control-label']) }}
                    {{ Form::text('seats', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required']) }}
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('images', 'Images', ['class' => 'col-lg-2 control-label']) }}
                    <input type="file" name="images[]" multiple />
                </div><!--form control-->

            {!! Form::submit('Create the motorhome', array('class' => 'btn btn-primary')) !!}

            {!! Form::close() !!}


            </div>
        @endauth

    </div><!--row-->
@endsection