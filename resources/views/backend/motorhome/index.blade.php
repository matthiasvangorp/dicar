@extends('frontend.layouts.app')

@section('content')
    <div class="row">

        @role('Executive')
        @foreach ($motorhomes as $motorhome)
        <div class="col-xs-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-home"></i> {!! $motorhome->name !!}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-6">
                            Slaapplaatsen
                        </div>

                        <div class="col-xs-6">
                            {!! $motorhome->sleepingPlaces !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            Zitplaatsen
                        </div>

                        <div class="col-xs-6">
                            {!! $motorhome->seats !!}
                        </div>
                    </div>

                </div>
            </div><!-- panel -->

        </div><!-- col-md-10 -->
        @endforeach
        @endauth

    </div><!--row-->
@endsection