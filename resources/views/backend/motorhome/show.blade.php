@extends('frontend.layouts.app')

@section ('title', trans('labels.backend.access.users.management') . ' | ' . trans('labels.backend.access.users.view'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.view') }}</small>
    </h1>
@endsection

@section('content')
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <h1>{!! $motorhome->name !!}</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                Slaapplaatsen
            </div>

            <div class="col-xs-6">
                {!! $motorhome->sleepingPlaces !!}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                Zitplaatsen
            </div>

            <div class="col-xs-6">
                {!! $motorhome->seats !!}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                @foreach (explode(";", $motorhome->images) as $image)
                    <img src="{!!  URL::to('/photo/320x320/images/motorhomes/'.$motorhome->id.'/'.$image) !!}"/>
                @endforeach
            </div>
        </div>




    </div>
@endsection