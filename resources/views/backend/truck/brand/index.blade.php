@extends('frontend.layouts.app')

@section('content')
    <div class="row">

        <table>
            <tr>
                <th>Referentie</th>
                <th>Naam</th>
                <th>Bewerken</th>
            </tr>
        @role('Executive')
        @foreach ($brands as $id => $name)

             <tr>
                 <td>{!! $id !!}</td>
                 <td>{!! $name !!}</td>
                 <td>
                     {{ Form::open(array('url' => 'admin/truckBrands' . $id, 'class' => 'pull-right')) }}
                     {{ Form::hidden('_method', 'DELETE') }}
                     {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                     {{ Form::close() }}
                     <a class="btn btn-small btn-success" href="{{ URL::to('admin/truckBrands/' . $id) }}">Show</a>
                     <a class="btn btn-small btn-info" href="{{ URL::to('admin/truckBrands/' . $id . '/edit') }}">Edit</a>
                 </td>
             </tr>

        @endforeach
        @endauth
        </table>

    </div><!--row-->
@endsection