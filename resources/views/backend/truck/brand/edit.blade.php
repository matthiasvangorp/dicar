@extends('frontend.layouts.app')

@section('content')
    <div class="row">

        @role('Executive')
            <div class="row">
                {!!Html::ul($errors->all()) !!}
                <H1>Update brand</H1>
                {!! Form::open(['url' => array('/admin/truckBrands', $brand->id), 'files' => true, 'method' =>  'PUT']) !!}

                <div class="form-group">
                    {{ Form::label('name', 'Name', ['class' => 'col-lg-2 control-label']) }}
                    {{ Form::text('name', $brand->name, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus']) }}
                </div>

            {!! Form::submit('Update the brand', array('class' => 'btn btn-primary')) !!}

            {!! Form::close() !!}


            </div>
        @endauth

    </div><!--row-->
@endsection