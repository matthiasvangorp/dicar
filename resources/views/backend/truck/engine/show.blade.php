@extends('frontend.layouts.app')

@section ('title', trans('labels.backend.access.users.management') . ' | ' . trans('labels.backend.access.users.view'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.view') }}</small>
    </h1>
@endsection

@section('content')
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <h1>{!! $engine->name !!}</h1>
            </div>
        </div>

    </div>
@endsection