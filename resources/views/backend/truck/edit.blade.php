@extends('frontend.layouts.app')

@section('content')
    @role('Executive')
    <div class="row">
        {!! Form::open(['url' => array('/admin/trucks', $truck->id), 'files' => true, 'method' =>  'PUT']) !!}
        <div class="panel with-nav-tabs panel-default">
            <div class="panel-heading">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1default" data-toggle="tab">{!! trans('strings.backend.general.general') !!}</a></li>
                    <li><a href="#tab3default" data-toggle="tab">{!! trans('strings.backend.general.construction') !!}</a></li>
                    <li><a href="#tab4default" data-toggle="tab">{!! trans('strings.backend.general.weights') !!}</a></li>
                    <li><a href="#tab5default" data-toggle="tab">{!! trans('strings.backend.general.equipment') !!}</a></li>
                    <li><a href="#tab6default" data-toggle="tab">{!! trans('strings.backend.general.delivery_terms') !!}</a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab1default"><div class="col-xs-12">
                            <H1>{!! trans('trucks.fields.edit_truck') !!}</H1>
                            {!!Html::ul($errors->all()) !!}
                        </div>

                        <div class="col-xs-12" id="form-errors"></div>

                        <div class="form-group col-xs-12">
                            {{ Form::label('internal_remarks', trans('trucks.fields.internal_remarks'), ['class' => 'col-lg-12 control-label summernote']) }}
                            {{ Form::textarea('internal_remarks', $truck->internal_remarks, ['class' => 'form-control', 'maxlength' => '1000']) }}
                        </div>

                        <div class="form-group col-xs-12">
                            {{ Form::label('sold', trans('trucks.fields.sold'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('sold', 1, $truck->sold) }}
                        </div><!--form control-->

                        <div class="col-xs-12">
                            <div class="form-group col-xs-3">
                                {!! Form::label('brand', trans('trucks.fields.brand'))  !!}
                                {!! Form::select('brand', $brands, $truck->truckbrand_id, array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group col-xs-1 add-plus-button">
                                <a id='btn-add-brand' href="#" class="btn btn-success" title="{!! trans('trucks.fields.add_brand') !!}">+</a>
                            </div>

                            <div class="form-group col-xs-8">
                                {{ Form::label('name', trans('trucks.fields.name'), ['class' => 'col-lg-12 control-label']) }}
                                {{ Form::text('name', $truck->name, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus']) }}
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group col-xs-4">
                                {{ Form::label('chassis_number', trans('trucks.fields.chassis_number'), ['class' => 'col-lg-12 control-label']) }}
                                {{ Form::text('chassis_number', $truck->chassis_number, ['class' => 'form-control', 'maxlength' => '50', 'required' => 'required']) }}
                            </div>
                            <div class="form-group col-xs-4">
                                {{ Form::label('euronorm', trans('trucks.fields.euronorm'), ['class' => 'col-lg-12 control-label']) }}
                                {{ Form::text('euronorm', $truck->euronorm, ['class' => 'form-control', 'maxlength' => '1']) }}
                            </div>
                            <div class="form-group col-xs-4">
                                {{ Form::label('seats', trans('trucks.fields.seats'), ['class' => 'col-lg-12 control-label']) }}
                                {{ Form::text('seats', $truck->seats, ['class' => 'form-control', 'maxlength' => '191']) }}
                            </div><!--form control-->
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group col-xs-4">
                                {{ Form::label('purchase_price', trans('trucks.fields.purchase_price'), ['class' => 'col-lg-12 control-label']) }}
                                {{ Form::text('purchase_price', $truck->purchase_price, ['class' => 'form-control', 'maxlength' => '8']) }}
                            </div>
                            <div class="form-group col-xs-4">
                                {{ Form::label('price', trans('trucks.fields.price'), ['class' => 'col-lg-12 control-label']) }}
                                {{ Form::text('price', $truck->price, ['class' => 'form-control', 'maxlength' => '8']) }}
                            </div>
                            <div class="form-group col-xs-4">
                                {{ Form::label('merchant_price', trans('trucks.fields.merchant_price'), ['class' => 'col-lg-12 control-label']) }}
                                {{ Form::text('merchant_price', $truck->merchant_price, ['class' => 'form-control', 'maxlength' => '8']) }}
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group col-xs-4">
                                @if ($truck->damage == "no_damage")
                                    {!! Form::radio('damage', 'no_damage', true) !!}
                                @else
                                    {!! Form::radio('damage', 'no_damage', false) !!}
                                @endif
                                {!! Form::label('no_damage', trans('trucks.fields.no_damage'), ['class' => 'col-lg-6 control-label']) !!}
                                <br/><br/>
                                @if ($truck->damage == "accident_damage")
                                    {!! Form::radio('damage', 'accident_damage', true) !!}
                                @else
                                    {!! Form::radio('damage', 'accident_damage', false) !!}
                                @endif
                                {!! Form::label('accident_damage', trans('trucks.fields.accident_damage'), ['class' => 'col-lg-6 control-label']) !!}
                                <br/><br/>
                                @if ($truck->damage == "engine_damage")
                                    {!! Form::radio('damage', 'engine_damage', true) !!}
                                @else
                                    {!! Form::radio('damage', 'engine_damage', false) !!}
                                @endif
                                {!! Form::label('engine_damage', trans('trucks.fields.engine_damage'), ['class' => 'col-lg-6 control-label']) !!}
                            </div>
                            <div class="form-group col-xs-4">
                                @if ($truck->property_consignment == "property")
                                    {!! Form::radio('property_consignment', 'property', true) !!}
                                @else
                                    {!! Form::radio('property_consignment', 'property', false) !!}
                                @endif
                                {!! Form::label('property', trans('trucks.fields.property'), ['class' => 'col-lg-4 control-label']) !!}<br/><br/>
                                @if ($truck->property_consignment == "consignment")
                                    {!! Form::radio('property_consignment', 'consignment', true) !!}
                                @else
                                        {!! Form::radio('property_consignment', 'consignment', false) !!}
                                @endif
                                {!! Form::label('consignment', trans('trucks.fields.consignment'), ['class' => 'col-lg-4 control-label']) !!}
                            </div>
                            <div class="form-group col-xs-4">
                                @if ($truck->vat_margin == "vat")
                                    {!! Form::radio('vat_margin', 'vat', true) !!}
                                @else
                                    {!! Form::radio('vat_margin', 'vat', false) !!}
                                @endif
                                {!! Form::label('vat', trans('trucks.fields.vat'), ['class' => 'col-lg-4 control-label']) !!}<br/><br/>
                                @if ($truck->vat_margin == "margin")
                                    {!! Form::radio('vat_margin', 'margin', true) !!}
                                @else
                                    {!! Form::radio('vat_margin', 'margin', false) !!}
                                @endif
                                {!! Form::label('margin', trans('trucks.fields.margin'), ['class' => 'col-lg-4 control-label']) !!}
                            </div>
                        </div>


                        <div class="col-xs-12">
                            <div class="form-group col-xs-3">
                                {!! Form::label('engine', trans('trucks.fields.engine'))  !!}
                                {!! Form::select('engine', $engines, $truck->truckengine_id, array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group col-xs-1 add-plus-button">
                                <a id='btn-add-engine' href="#" class="btn btn-success" title="{!! trans('trucks.fields.add_engine') !!}">+</a>
                            </div>

                            <div class="form-group col-xs-4">
                                {{ Form::label('color', trans('trucks.fields.color')) }}
                                {!! Form::select('color', $colors, $truck->color, array('class' => 'form-control')) !!}
                            </div>

                            <div class="form-group col-xs-4">
                                {{ Form::label('year', trans('trucks.fields.year'), ['class' => 'col-lg-12 control-label']) }}
                                {{ Form::text('year', $truck->year, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => '01/2000']) }}
                            </div><!--form control-->
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group col-xs-4">
                                {{ Form::label('mileage', trans('trucks.fields.mileage'), ['class' => 'col-lg-12 control-label']) }}
                                {{ Form::text('mileage', $truck->mileage, ['class' => 'form-control', 'maxlength' => '191']) }}
                            </div><!--form control-->
                            <div class="form-group col-xs-4">
                                {{ Form::label('horsepower', trans('trucks.fields.horsepower'), ['class' => 'col-lg-12 control-label']) }}
                                {{ Form::text('horsepower', $truck->horsepower, ['class' => 'form-control', 'maxlength' => '191']) }}
                            </div><!--form control-->

                            <div class="form-group col-xs-4">
                                {{ Form::label('transmission', trans('trucks.fields.transmission'), ['class' => 'col-lg-12 control-label']) }}
                                {{ Form::text('transmission', $truck->transmission, ['class' => 'form-control', 'maxlength' => '191']) }}
                            </div><!--form control-->
                        </div>

                        <div class="col-xs-12">


                            <div class="form-group col-xs-4">
                                {{ Form::label('drivetrain', trans('trucks.fields.drivetrain'), ['class' => 'col-lg-12 control-label']) }}
                                {!! Form::select('drivetrain', $drivetrains, $truck->drivetrain, array('class' => 'form-control')) !!}
                            </div><!--form control-->
                            <div class="form-group col-xs-4">
                                {{ Form::label('used', trans('trucks.fields.used'), ['class' => 'col-lg-12 control-label']) }}
                                {{ Form::checkbox('used', 1, $truck->used) }}
                            </div><!--form control-->
                        </div>

                        <div class="form-group col-xs-12">
                            {{ Form::label('remarks', trans('trucks.fields.weights_remarks'), ['class' => 'col-lg-12 control-label summernote']) }}
                            {{ Form::textarea('remarks', $truck->remarks, ['class' => 'form-control', 'maxlength' => '1000']) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('defaultText', trans('trucks.fields.defaultText'), ['class' => 'col-lg-12 control-label']) }}
                            <span id="default_text_1">{!! $defaultText1 !!}</span> <a href="#" id="edit_default_text_1">{!! trans('trucks.fields.edit') !!}</a>
                        </div><!--form control-->

                        <div class="col-xs-12">
                            <div class="form-group">
                                {{ Form::label('images', trans('trucks.fields.images'), ['class' => 'col-lg-12 control-label']) }}
                                <input id="fileupload" type="file" name="images[]" data-url="{!! URL::to('/admin/trucks/upload') !!}" multiple>
                                <input type="hidden" id="image_order" name="image_order" value="{!! $imageOrder !!}" />
                                <input type="hidden" id="deleted_images" name="deleted_images" value="" />
                            </div><!--form control-->
                            <div id="progress" class="progress">
                                <div class="progress-bar progress-bar-success"></div>
                            </div>
                            <div id="files" class="files">
                                @foreach ($truck->images as $image)
                                    <div class="row" id="{!! $image->filename !!}">
                                        <i class="fa fa-fw fa-sort"></i>
                                        <img src="{!!  URL::to('photo/100x75/images/trucks/'.$truck->id.'/'.$image->filename) !!}">
                                        <span title="Remove" style="color:red; font-size: 30px; cursor: pointer;" class="glyphicon glyphicon-remove" onclick="removeImage(this, '{!! $image->filename !!}')"></span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab3default">
                        <div class="form-group col-xs-12">
                            {{ Form::label('construction_type', trans('trucks.fields.construction')) }}
                            {!! Form::select('construction_type', $construction_types, $truck->construction_type, array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group col-xs-12">
                            {{ Form::label('construction_lift', trans('trucks.fields.construction_lift'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('construction_lift', 1, $truck->construction_lift) }}
                        </div><!--form control-->
                        <div class="construction_lift_power">
                            <div class="form-group col-xs-11">
                                {!! Form::label('construction_lift_power_id', trans('trucks.fields.construction_lift_power'))  !!}
                                {!! Form::select('construction_lift_power_id', $constructionLiftPowers, $truck->construction_lift_power_id, array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group col-xs-1 add-plus-button">
                                <a id='btn-add-construction-lift-power' href="#" class="btn btn-success" title="{!! trans('trucks.fields.add_construction_lift_power') !!}">+</a>
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            {{ Form::label('construction_dimensions', trans('trucks.fields.construction_dimensions'), ['class' => 'col-lg-12 control-label']) }}
                            {{ Form::text('construction_dimensions', $truck->construction_dimensions, ['class' => 'form-control', 'maxlength' => '20']) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('construction_sidedoor', trans('trucks.fields.construction_sidedoor'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('construction_sidedoor', 1, $truck->construction_sidedoor) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('construction_remarks', trans('trucks.fields.construction_remarks'), ['class' => 'col-lg-12 control-label']) }}
                            {{ Form::textarea('construction_remarks', $truck->construction_remarks, ['class' => 'form-control', 'maxlength' => '1000']) }}
                        </div><!--form control-->

                    </div>
                    <div class="tab-pane fade" id="tab4default">
                        <div class="form-group col-xs-12">
                            {{ Form::label('weights_empty', trans('trucks.fields.weights_empty'), ['class' => 'col-lg-12 control-label']) }}
                            {{ Form::text('weights_empty', $truck->weights_empty, ['class' => 'form-control']) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('weights_mtm', trans('trucks.fields.weights_mtm'), ['class' => 'col-lg-12 control-label']) }}
                            {{ Form::text('weights_mtm', $truck->weights_mtm, ['class' => 'form-control']) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('weights_useful_load_capacity', trans('trucks.fields.weights_useful_load_capacity'), ['class' => 'col-lg-12 control-label']) }}
                            {{ Form::text('weights_useful_load_capacity', $truck->weights_useful_load_capacity, ['class' => 'form-control', 'maxlength' => '5']) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('weights_useful_tow_capacity', trans('trucks.fields.weights_useful_tow_capacity'), ['class' => 'col-lg-12 control-label']) }}
                            {{ Form::text('weights_useful_tow_capacity', null, ['class' => 'form-control', 'maxlength' => '5']) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('weights_max_trailer_load', trans('trucks.fields.weights_max_trailer_load'), ['class' => 'col-lg-12 control-label']) }}
                            {{ Form::text('weights_max_trailer_load', $truck->weights_max_trailer_load, ['class' => 'form-control', 'maxlength' => '5']) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('weights_driving_license', trans('trucks.fields.weights_driving_license'), ['class' => 'col-lg-12 control-label']) }}
                            {{ Form::select('weights_driving_license', ['B' => 'B', 'C' =>'C'], $truck->weights_driving_license, ['class' => 'form-control']) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('weights_eurovignet', trans('trucks.fields.weights_eurovignet'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('weights_eurovignet', 1, $truck->weights_eurovignet) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('weights_remarks', trans('trucks.fields.weights_remarks'), ['class' => 'col-lg-12 control-label']) }}
                            {{ Form::textarea('weights_remarks', $truck->weights_remarks, ['class' => 'form-control', 'maxlength' => '1000']) }}
                        </div><!--form control-->
                    </div>
                    <div class="tab-pane fade" id="tab5default">
                        <div class="form-group col-xs-12">
                            {{ Form::text('filter_equipment', null, ['class' => 'form-control', 'placeholder' => trans('trucks.fields.start_typing_to_filter'), 'id' => 'filter_equipment']) }}
                        </div>
                        <div class="form-group col-xs-12">
                            {{ Form::label('chassis_air_suspension', trans('trucks.fields.chassis_air_suspension'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('chassis_air_suspension', 1, $truck->chassis_air_suspension) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('chassis_cruise_control', trans('trucks.fields.chassis_cruise_control'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('chassis_cruise_control', 1, $truck->chassis_cruise_control )}}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_airbags', trans('trucks.fields.equipment_airbags'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_airbags', 1, $truck->equipment_airbags, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_airbags'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_rockinger', trans('trucks.fields.equipment_rockinger'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_rockinger', 1, $truck->equipment_rockinger), ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_rockinger'))] }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_roofspoiler', trans('trucks.fields.equipment_roofspoiler'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_roofspoiler', 1, $truck->equipment_roofspoiler, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_roofspoiler'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_automatic_airco', trans('trucks.fields.equipment_automatic_airco'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_automatic_airco', 1, $truck->equipment_automatic_airco, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_automatic_airco'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_airco', trans('trucks.fields.equipment_airco'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_airco', 1, $truck->equipment_airco, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_airco'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_airsprung_seat', trans('trucks.fields.equipment_airsprung_seat'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_airsprung_seat', 1, $truck->equipment_airsprung_seat, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_airsprung_seat'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_pto', trans('trucks.fields.equipment_pto'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_pto', 1, $truck->equipment_pto, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_pto'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_abs', trans('trucks.fields.equipment_abs'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_abs', 1, $truck->equipment_abs, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_abs'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_rear_camera_color', trans('trucks.fields.equipment_rear_camera_color'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_rear_camera_color', 1, $truck->equipment_rear_camera_color, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_rear_camera_color'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_rear_camera', trans('trucks.fields.equipment_rear_camera'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_rear_camera', 1, $truck->equipment_rear_camera, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_rear_camera'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_alarm', trans('trucks.fields.equipment_alarm'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_alarm', 1, $truck->equipment_alarm, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_alarm'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_motor_break', trans('trucks.fields.equipment_motor_break'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_motor_break', 1, $truck->equipment_motor_break, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_motor_break'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_retarder', trans('trucks.fields.equipment_retarder'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_retarder', 1, $truck->equipment_retarder, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_retarder'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_aluminum_rims', trans('trucks.fields.equipment_aluminum_rims'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_aluminum_rims', 1, $truck->equipment_aluminum_rims, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_aluminum_rims'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_cd_player', trans('trucks.fields.equipment_cd_player'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_cd_player', 1, $truck->equipment_cd_player, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_cd_player'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_crane', trans('trucks.fields.equipment_crane'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_crane', 1, $truck->equipment_crane, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_crane'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_sleeping_cabin', trans('trucks.fields.equipment_sleeping_cabin'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_sleeping_cabin', 1, $truck->equipment_sleeping_cabin, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_sleeping_cabin'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_central_locking', trans('trucks.fields.equipment_central_locking'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_central_locking', 1, $truck->equipment_central_locking, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_central_locking'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_double_rear_tires', trans('trucks.fields.equipment_double_rear_tires'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_double_rear_tires', 1, $truck->equipment_double_rear_tires, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_double_rear_tires'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_roof_rails', trans('trucks.fields.equipment_roof_rails'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_roof_rails', 1, $truck->equipment_roof_rails, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_roof_rails'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_electric_windows', trans('trucks.fields.equipment_electric_windows'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_electric_windows', 1, $truck->equipment_electric_windows, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_electric_windows'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_electric_mirrors', trans('trucks.fields.equipment_electric_mirrors'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_electric_mirrors', 1, $truck->equipment_electric_mirrors, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_electric_mirrors'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_gps', trans('trucks.fields.equipment_gps'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_gps', 1, $truck->equipment_gps, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_gps'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_headlamp_sprayers', trans('trucks.fields.equipment_headlamp_sprayers'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_headlamp_sprayers', 1, $truck->equipment_headlamp_sprayers, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_headlamp_sprayers'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_multifunctional_wheel', trans('trucks.fields.equipment_multifunctional_wheel'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_multifunctional_wheel', 1, $truck->equipment_multifunctional_wheel, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_multifunctional_wheel'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_non_smoker', trans('trucks.fields.equipment_non_smoker'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_non_smoker', 1, $truck->equipment_non_smoker, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_non_smoker'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_sunroof', trans('trucks.fields.equipment_sunroof'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_sunroof', 1, $truck->equipment_sunroof, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_sunroof'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_pdf', trans('trucks.fields.equipment_pdf'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_pdf', 1, $truck->equipment_pdf, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_pdf'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_rainsensor', trans('trucks.fields.equipment_rainsensor'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_rainsensor', 1, $truck->equipment_rainsensor, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_rainsensor'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_stability_control', trans('trucks.fields.equipment_stability_control'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_stability_control', 1, $truck->equipment_stability_control, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_stability_control'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_night_heating', trans('trucks.fields.equipment_night_heating'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_night_heating', 1, $truck->equipment_night_heating, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_night_heating'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_power_steering', trans('trucks.fields.equipment_power_steering'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_power_steering', 1, $truck->equipment_power_steering, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_power_steering'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_phone', trans('trucks.fields.equipment_phone'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_phone', 1, $truck->equipment_phone, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_phone'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_towing_hook', trans('trucks.fields.equipment_towing_hook'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_towing_hook', 1, $truck->equipment_towing_hook, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_towing_hook'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_detachable_towing_hook', trans('trucks.fields.equipment_detachable_towing_hook'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_detachable_towing_hook', 1, $truck->equipment_detachable_towing_hook, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_detachable_towing_hook'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_tv', trans('trucks.fields.equipment_tv'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_tv', 1, $truck->equipment_tv, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_tv'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_lowered_suspension', trans('trucks.fields.equipment_lowered_suspension'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_lowered_suspension', 1, $truck->equipment_lowered_suspension, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_lowered_suspension'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_heated_mirrors', trans('trucks.fields.equipment_heated_mirrors'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_heated_mirrors', 1, $truck->equipment_heated_mirrors, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_heated_mirrors'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_heated_seats', trans('trucks.fields.equipment_heated_seats'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_heated_seats', 1, $truck->equipment_heated_seats, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_heated_seats'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_xenon_headlights', trans('trucks.fields.equipment_xenon_headlights'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_xenon_headlights', 1, $truck->equipment_xenon_headlights, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_xenon_headlights'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_sunvisor', trans('trucks.fields.equipment_sunvisor'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_sunvisor', 1, $truck->equipment_sunvisor, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_sunvisor'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_reserve_differential', trans('trucks.fields.equipment_reserve_differential'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_reserve_differential', 1, $truck->equipment_reserve_differential, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_reserve_differential'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_electropack', trans('trucks.fields.equipment_electropack'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('equipment_electropack', 1, $truck->equipment_electropack, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_electropack'))]) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('equipment_terms_remarks', trans('trucks.fields.equipment_terms_remarks'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::textarea('equipment_terms_remarks', $truck->equipment_terms_remarks, ['class' => 'form-control', 'maxlength' => '1000']) }}
                        </div><!--form control-->

                    </div>
                    <div class="tab-pane fade" id="tab6default">
                        <div class="form-group col-xs-12">
                            {{ Form::label('delivery_terms_technical_inspection', trans('trucks.fields.delivery_terms_technical_inspection'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('delivery_terms_technical_inspection', 1, $truck->delivery_terms_technical_inspection) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('delivery_terms_legal_kit', trans('trucks.fields.delivery_terms_legal_kit'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('delivery_terms_legal_kit', 1, $truck->delivery_terms_legal_kit) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('delivery_terms_cleaning', trans('trucks.fields.delivery_terms_cleaning'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('delivery_terms_cleaning', 1, $truck->delivery_terms_cleaning) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('delivery_terms_calibration_tachograph', trans('trucks.fields.delivery_terms_calibration_tachograph'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('delivery_terms_calibration_tachograph', 1, $truck->delivery_terms_calibration_tachograph) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('delivery_terms_pvg', trans('trucks.fields.delivery_terms_pvg'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('delivery_terms_pvg', 1, $truck->delivery_terms_pvg) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('delivery_terms_technical_checkup', trans('trucks.fields.delivery_terms_technical_checkup'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('delivery_terms_technical_checkup', 1, $truck->delivery_terms_technical_checkup) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('delivery_terms_guarantee', trans('trucks.fields.delivery_terms_guarantee'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('delivery_terms_guarantee', 1, $truck->delivery_terms_guarantee) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('delivery_terms_new_tires', trans('trucks.fields.delivery_terms_new_tires'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::checkbox('delivery_terms_new_tires', 1, $truck->delivery_terms_new_tires) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-12">
                            {{ Form::label('delivery_terms_remarks', trans('trucks.fields.delivery_terms_remarks'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::textarea('delivery_terms_remarks', $truck->delivery_terms_remarks, ['class' => 'form-control', 'maxlength' => '1000']) }}
                        </div><!--form control-->
                        <div class="form-group col-xs-2">
                            {{ Form::checkbox('default_text_2', 1, $truck->default_text_2) }}
                        </div>
                        <div class="form-group col-xs-10">
                            <span id="default_text_2">{!! $defaultText2 !!}</span>
                            <a href="#" id="edit_default_text_2">{!! trans('trucks.fields.edit') !!}</a>
                        </div><!--form control-->
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                {!! Form::submit(trans('trucks.fields.edit_truck'), array('class' => 'btn btn-primary')) !!}
            </div>
        </div>


        {!! Form::close() !!}
    </div>

    <!-- Modal (Pop up when add brand button clicked) -->
    <div class="modal fade" id="modalBrands" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">{!! trans('trucks.fields.add_brand') !!}</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => '/admin/truckBrands', 'files' => true, 'method' =>  'POST']) !!}

                    <div class="form-group">
                        {!! Form::label('brandName', trans('trucks.fields.name')) !!}
                        {!! Form::text('brandName', null, array('class' => 'form-control')) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary ajaxbutton" id="btn-save-brand" value="add">{!! trans('trucks.fields.add_brand') !!}</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal (Pop up when add engine button clicked) -->
    <div class="modal fade" id="modalEngines" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">{!! trans('trucks.fields.add_engine') !!}</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => '/admin/truckEngines', 'files' => true, 'method' =>  'POST']) !!}

                    <div class="form-group">
                        {!! Form::label('engineName', trans('trucks.fields.name')) !!}
                        {!! Form::text('engineName', null, array('class' => 'form-control')) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary ajaxbutton" id="btn-save-engine" value="add">{!! trans('trucks.fields.add_engine') !!}</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal (Pop up when add construction lift power button clicked) -->
    <div class="modal fade" id="modalConstructionLiftPowers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">{!! trans('trucks.fields.add_construction_lift_power') !!}</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => '/admin/truckConstructionLiftPower', 'files' => true, 'method' =>  'POST']) !!}

                    <div class="form-group">
                        {!! Form::label('constructionLiftPowerName', trans('trucks.fields.construction_lift_power')) !!}
                        {!! Form::text('constructionLiftPowerName', null, array('class' => 'form-control')) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary ajaxbutton" id="btn-save-construction-lift-power" value="add">{!! trans('trucks.fields.add_construction_lift_power') !!}</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal (Pop up when edit default text clicked) -->
    <div class="modal fade" id="modalDefaultText" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">{!! trans('trucks.fields.defaultText') !!}</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => '/admin/defaultText', 'files' => false, 'method' =>  'POST']) !!}

                    <div class="form-group">
                        {{ Form::label('default_text_edit', trans('trucks.fields.defaultText'), ['class' => 'col-lg-2 control-label']) }}
                        {{ Form::textarea('default_text_edit', $defaultText1, ['class' => 'form-control']) }}
                        {{ Form::hidden('default_text_type', '', array('id' => 'default_text_type')) }}
                    </div>

                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary ajaxbutton" id="btn-save-default_text" value="edit">{!! trans('trucks.fields.edit') !!}</button>
                </div>
            </div>
        </div>
    </div>

    @endauth

@endsection

@section('after-scripts')
    {!! Html::script('js/backend/plugin/jquery-file-upload/vendor/jquery.ui.widget.js') !!}
    {!! Html::script('js/backend/plugin/jquery-file-upload/jquery.iframe-transport.js') !!}
    {!! Html::script('js/backend/plugin/jquery-file-upload/jquery.fileupload.js') !!}
    {!! Html::script('js/backend/plugin/jquery-ui/jquery-ui.js') !!}
    {!! Html::script('js/backend/trucks/ajax-crud.js') !!}
@endsection

@section('css')
    {!! HTML::style('css/backend/plugin/jquery-file-upload/style.css') !!}
    {!! HTML::style('css/backend/plugin/jquery-file-upload/jquery.fileupload.css') !!}
    {!! HTML::style('css/backend/plugin/jquery-ui/jquery-ui.theme.css') !!}
    {!! HTML::style('css/backend/plugin/jquery-ui/jquery-ui.structure.css') !!}
@endsection