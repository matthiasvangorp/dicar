@extends('frontend.layouts.app')

@section('content')
    <div class="row">

        <a class="btn btn-success" href="{!!  URL::to('admin/trucks/create') !!}">{!! trans('strings.backend.general.add_new_truck') !!}</a>
        <br/><br/>
    </div>

    <div class="row">
        <strong>
            <div class="col-xs-1">
                {!! trans('strings.backend.general.reference') !!}
            </div>
            <div class="col-xs-1">
                {!! trans('strings.backend.general.brand') !!}
            </div>
            <div class="col-xs-2">
                {!! trans('strings.backend.general.name') !!}
            </div>
            <div class="col-xs-1">
                {!! trans('trucks.fields.mileage') !!}
            </div>
            <div class="col-xs-1">
                {!! trans('trucks.fields.year') !!}
            </div>
            <div class="col-xs-1">
                {!! trans('trucks.fields.purchase_price') !!}
            </div>
            <div class="col-xs-1">
                {!! trans('trucks.fields.price') !!}
            </div>
            <div class="col-xs-1">
                {!! trans('trucks.fields.color') !!}
            </div>
            <div class="col-xs-1">
                {!! trans('strings.backend.general.insert_date') !!}
            </div>
            <div class="col-xs-2">
                {!! trans('strings.backend.general.edit') !!}
            </div>
        </strong>
    </div>
        @role('Executive')
        @foreach ($trucks as $truck)
            <div class="row" @if(!$truck->sold) style="color:black" @endif>
                <div class="col-xs-1">
                    {!! $truck->reference !!}
                </div>
                <div class="col-xs-1">{!! $truck->brand->name !!}</div>
                <div class="col-xs-2">{!! $truck->name !!}</div>
                <div class="col-xs-1">{!! number_format($truck->mileage / 1000, 0, ',', '.') !!}</div>
                <div class="col-xs-1">{!! substr($truck->year, 0,4)!!}</div>
                <div class="col-xs-1">{!! number_format($truck->purchase_price, 0, ',', '.')  !!}</div>
                <div class="col-xs-1">{!! number_format($truck->price, 0, ',', '.')  !!}</div>
                <div class="col-xs-1">{!! trans('strings.colors.'.$truck->color) !!}</div>
                <div class="col-xs-1">{!! date('d/m/Y', strtotime($truck->created_at)) !!}</div>
                <div class="col-xs-1">
                    <a class="btn btn-small btn-success" href="{{ URL::to('trucks/' . $truck->id) }}" title="{!! trans('strings.backend.general.show') !!}"><i class="fas fa-eye"></i></a><br/>
                    <a class="btn btn-small btn-info" href="{{ URL::to('admin/trucks/' . $truck->id . '/edit') }}" title="{!! trans('strings.backend.general.edit') !!}"><i class="far fa-edit"></i></a>
                    {{ Form::open(array('url' => 'admin/trucks/' . $truck->id)) }}
                    {{ Form::hidden('_method', 'DELETE') }}

                    <button type="submit" value="{!! trans('strings.backend.general.delete') !!}" class="btn btn-danger">
                        <i class="fas fa-trash"></i>
                    </button>
                    {{ Form::close() }}
                </div>
            </div>

        @endforeach
        @endauth


        <br/><br/>
        <a class="btn btn-success" href="{!!  URL::to('admin/trucks/create') !!}">{!! trans('strings.backend.general.add_new_truck') !!}</a>

    </div><!--row-->
@endsection