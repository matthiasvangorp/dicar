@extends('frontend.layouts.app')

@section ('title', trans('labels.backend.access.users.management') . ' | ' . trans('labels.backend.access.users.view'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.view') }}</small>
    </h1>
@endsection

@section('content')
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <h1>{!! $truck->brand->name !!} {!! $truck->name!!}</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                {!! trans('trucks.fields.year')  !!}
            </div>

            <div class="col-xs-6">
                {!! $truck->year !!}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                {!! trans('trucks.fields.mileage')  !!}
            </div>

            <div class="col-xs-6">
                {!! $truck->mileage !!}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                {!! trans('trucks.fields.horsepower')  !!}
            </div>

            <div class="col-xs-6">
                {!! $truck->horsepower !!}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                {!! trans('trucks.fields.seats')  !!}
            </div>

            <div class="col-xs-6">
                {!! $truck->seats !!}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                {!! trans('trucks.fields.engine')  !!}
            </div>

            <div class="col-xs-6">
                {!! $truck->engine->name !!}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                {!! trans('trucks.fields.color')  !!}
            </div>

            <div class="col-xs-6">
                {!! trans('strings.colors.'.$truck->color) !!}
            </div>
        </div>



        <div class="row">
            <div class="col-xs-6">
                {!! trans('trucks.fields.construction')  !!}
            </div>

            <div class="col-xs-6">
                {!! $truck->construction !!}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                {!! trans('trucks.fields.dimensions')  !!}
            </div>

            <div class="col-xs-6">
                {!! $truck->dimensions !!}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                {!! trans('trucks.fields.tailgate')  !!}
            </div>

            <div class="col-xs-6">
                {!! $truck->tailgate !!}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                {!! trans('trucks.fields.MTM')  !!}
            </div>

            <div class="col-xs-6">
                {!! $truck->mtm !!}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                {!! trans('trucks.fields.empty_weight')  !!}
            </div>

            <div class="col-xs-6">
                {!! $truck->empty_weight !!}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                {!! trans('trucks.fields.horsepower')  !!}
            </div>

            <div class="col-xs-6">
                {!! $truck->horsepower !!}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                {!! trans('trucks.fields.transmission')  !!}
            </div>

            <div class="col-xs-6">
                {!! $truck->transmission !!}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                {!! trans('trucks.fields.used')  !!}
            </div>

            <div class="col-xs-6">
                {!! $truck->used !!}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                @foreach ($truck->images as $image)
                    <img src="{!!  URL::to('/photo/320x240/images/trucks/'.$truck->id.'/'.$image->filename) !!}"/>
                @endforeach
            </div>
        </div>




    </div>
@endsection