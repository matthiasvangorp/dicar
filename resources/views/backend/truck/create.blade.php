@extends('frontend.layouts.app')

@section('content')
        @role('Executive')
            <div class="row">
                {!! Form::open(['url' => '/admin/trucks', 'files' => true, 'method' =>  'POST']) !!}
                <div class="panel with-nav-tabs panel-default">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">{!! trans('strings.backend.general.general') !!}</a></li>
                            <li><a href="#tab3default" data-toggle="tab">{!! trans('strings.backend.general.construction') !!}</a></li>
                            <li><a href="#tab4default" data-toggle="tab">{!! trans('strings.backend.general.weights') !!}</a></li>
                            <li><a href="#tab5default" data-toggle="tab">{!! trans('strings.backend.general.equipment') !!}</a></li>
                            <li><a href="#tab6default" data-toggle="tab">{!! trans('strings.backend.general.delivery_terms') !!}</a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab1default">
                                <div class="col-xs-12">
                                    <H1>{!! trans('trucks.fields.create_new_truck') !!}</H1>
                                    {!!Html::ul($errors->all()) !!}
                                </div>
                                <div class="col-xs-12" id="form-errors"></div>


                                <div class="form-group col-xs-12">
                                    {{ Form::label('internal_remarks', trans('trucks.fields.internal_remarks'), ['class' => 'col-lg-12 control-label summernote']) }}
                                    {{ Form::textarea('internal_remarks', null, ['class' => 'form-control', 'maxlength' => '1000']) }}
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group col-xs-3">
                                        {!! Form::label('brand', trans('trucks.fields.brand'))  !!}
                                        {!! Form::select('brand', $brands, null, array('class' => 'form-control')) !!}
                                    </div>
                                    <div class="form-group col-xs-1 add-plus-button">
                                        <a id='btn-add-brand' href="#" class="btn btn-success" title="{!! trans('trucks.fields.add_brand') !!}">+</a>
                                    </div>

                                    <div class="form-group col-xs-8">
                                        {{ Form::label('name', trans('trucks.fields.name'), ['class' => 'col-lg-12 control-label']) }}
                                        {{ Form::text('name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus']) }}
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group col-xs-4">
                                        {{ Form::label('chassis_number', trans('trucks.fields.chassis_number'), ['class' => 'col-lg-12 control-label']) }}
                                        {{ Form::text('chassis_number', null, ['class' => 'form-control', 'maxlength' => '50', 'required' => 'required']) }}
                                    </div>
                                    <div class="form-group col-xs-4">
                                        {{ Form::label('euronorm', trans('trucks.fields.euronorm'), ['class' => 'col-lg-12 control-label']) }}
                                        {{ Form::text('euronorm', null, ['class' => 'form-control', 'maxlength' => '1']) }}
                                    </div>

                                    <div class="form-group col-xs-4">
                                        {{ Form::label('seats', trans('trucks.fields.seats'), ['class' => 'col-lg-12 control-label']) }}
                                        {{ Form::text('seats', null, ['class' => 'form-control', 'maxlength' => '191']) }}
                                    </div><!--form control-->

                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group col-xs-4">
                                        {{ Form::label('purchase_price', trans('trucks.fields.purchase_price'), ['class' => 'col-lg-12 control-label']) }}
                                        {{ Form::text('purchase_price', null, ['class' => 'form-control', 'maxlength' => '8']) }}
                                    </div>

                                    <div class="form-group col-xs-4">
                                        {{ Form::label('price', trans('trucks.fields.price'), ['class' => 'col-lg-12 control-label']) }}
                                        {{ Form::text('price', null, ['class' => 'form-control', 'maxlength' => '8']) }}
                                    </div>

                                    <div class="form-group col-xs-4">
                                        {{ Form::label('merchant_price', trans('trucks.fields.merchant_price'), ['class' => 'col-lg-12 control-label']) }}
                                        {{ Form::text('merchant_price', null, ['class' => 'form-control', 'maxlength' => '8']) }}
                                    </div>


                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group col-xs-4">
                                        {!! Form::radio('damage', 'no_damage', true) !!}
                                        {!! Form::label('no_damage', trans('trucks.fields.no_damage'), ['class' => 'col-lg-6 control-label']) !!}
                                        <br/><br/>
                                        {!! Form::radio('damage', 'accident_damage') !!}
                                        {!! Form::label('accident_damage', trans('trucks.fields.accident_damage'), ['class' => 'col-lg-6 control-label']) !!}
                                        <br/><br/>
                                        {!! Form::radio('damage', 'engine_damage') !!}
                                        {!! Form::label('engine_damage', trans('trucks.fields.engine_damage'), ['class' => 'col-lg-6 control-label']) !!}
                                    </div>
                                    <div class="form-group col-xs-4">
                                        {!! Form::radio('property_consignment', 'property', true) !!}
                                        {!! Form::label('property', trans('trucks.fields.property'), ['class' => 'col-lg-4 control-label']) !!}<br/><br/>
                                        {!! Form::radio('property_consignment', 'consignment') !!}
                                        {!! Form::label('consignment', trans('trucks.fields.consignment'), ['class' => 'col-lg-4 control-label']) !!}
                                    </div>
                                    <div class="form-group col-xs-4">
                                        {!! Form::radio('vat_margin', 'vat', true) !!}
                                        {!! Form::label('vat', trans('trucks.fields.vat'), ['class' => 'col-lg-4 control-label']) !!}<br/><br/>
                                        {!! Form::radio('vat_margin', 'margin') !!}
                                        {!! Form::label('margin', trans('trucks.fields.margin'), ['class' => 'col-lg-4 control-label']) !!}
                                    </div>
                                </div>


                                <div class="col-xs-12">
                                    <div class="form-group col-xs-3">
                                            {!! Form::label('engine', trans('trucks.fields.engine'))  !!}
                                            {!! Form::select('engine', $engines, null, array('class' => 'form-control')) !!}
                                    </div>
                                    <div class="form-group col-xs-1 add-plus-button">
                                        <a id='btn-add-engine' href="#" class="btn btn-success" title="{!! trans('trucks.fields.add_engine') !!}">+</a>
                                    </div>

                                    <div class="form-group col-xs-4">
                                        {{ Form::label('color', trans('trucks.fields.color')) }}
                                        {!! Form::select('color', $colors, 'white', array('class' => 'form-control')) !!}
                                    </div>

                                    <div class="form-group col-xs-4">
                                        {{ Form::label('year', trans('trucks.fields.year'), ['class' => 'col-lg-12 control-label']) }}
                                        {{ Form::text('year', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => '01/2000']) }}
                                    </div><!--form control-->
                                </div>

                                <div class="col-xs-12">
                                    <div class="form-group col-xs-4">
                                        {{ Form::label('mileage', trans('trucks.fields.mileage'), ['class' => 'col-lg-12 control-label']) }}
                                        {{ Form::text('mileage', null, ['class' => 'form-control', 'maxlength' => '191']) }}
                                    </div><!--form control-->
                                    <div class="form-group col-xs-4">
                                        {{ Form::label('horsepower', trans('trucks.fields.horsepower'), ['class' => 'col-lg-12 control-label']) }}
                                        {{ Form::text('horsepower', null, ['class' => 'form-control', 'maxlength' => '191']) }}
                                    </div><!--form control-->

                                    <div class="form-group col-xs-4">
                                        {{ Form::label('transmission', trans('trucks.fields.transmission'), ['class' => 'col-lg-12 control-label']) }}
                                        {{ Form::text('transmission', null, ['class' => 'form-control', 'maxlength' => '191']) }}
                                    </div><!--form control-->
                                </div>
                                <div class="col-xs-12">


                                    <div class="form-group col-xs-4">
                                        {{ Form::label('drivetrain', trans('trucks.fields.drivetrain'), ['class' => 'col-lg-12 control-label']) }}
                                        {!! Form::select('drivetrain', $drivetrains, 'back', array('class' => 'form-control')) !!}
                                    </div><!--form control-->
                                    <div class="form-group col-xs-4">
                                        {{ Form::label('used', trans('trucks.fields.used'), ['class' => 'col-lg-12 control-label']) }}
                                        {{ Form::checkbox('used', 1, true) }}
                                    </div><!--form control-->

                                </div>

                                <div class="form-group col-xs-12">
                                    {{ Form::label('remarks', trans('trucks.fields.weights_remarks'), ['class' => 'col-lg-12 control-label summernote']) }}
                                    {{ Form::textarea('remarks', null, ['class' => 'form-control', 'maxlength' => '1000']) }}
                                </div>
                                <div class="form-group col-xs-12">
                                    {{ Form::label('defaultText', trans('trucks.fields.defaultText'), ['class' => 'col-lg-12 control-label']) }}
                                    <span id="default_text_1">{!! $defaultText1 !!}</span> <a href="#" id="edit_default_text_1">{!! trans('trucks.fields.edit') !!}</a>
                                </div><!--form control-->

                                <div class="col-xs-12">
                                    <div class="form-group">
                                        {{ Form::label('images', trans('trucks.fields.images'), ['class' => 'col-lg-12 control-label']) }}
                                        <input id="fileupload" type="file" name="images[]" data-url="upload" multiple>
                                        <input type="hidden" id="image_order" name="image_order" value="" />
                                    </div><!--form control-->
                                    <div id="progress" class="progress">
                                        <div class="progress-bar progress-bar-success"></div>
                                    </div>
                                    <div id="files" class="files"></div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab3default">
                                <div class="form-group col-xs-12">
                                    {{ Form::label('construction_type', trans('trucks.fields.construction')) }}
                                    {!! Form::select('construction_type', $construction_types, null, array('class' => 'form-control')) !!}
                                </div>
                                <div class="form-group col-xs-12">
                                    {{ Form::label('construction_lift', trans('trucks.fields.construction_lift'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('construction_lift', 1, false) }}
                                </div><!--form control-->
                                <div class="construction_lift_power">
                                    <div class="form-group col-xs-11">
                                        {!! Form::label('construction_lift_power_id', trans('trucks.fields.construction_lift_power_id'))  !!}
                                        {!! Form::select('construction_lift_power_id', $constructionLiftPowers, null, array('class' => 'form-control')) !!}
                                    </div>
                                    <div class="form-group col-xs-1 add-plus-button">
                                        <a id='btn-add-construction-lift-power' href="#" class="btn btn-success" title="{!! trans('trucks.fields.add_construction_lift_power') !!}">+</a>
                                    </div>
                                </div>

                                <div class="form-group col-xs-12">
                                    {{ Form::label('construction_dimensions', trans('trucks.fields.construction_dimensions'), ['class' => 'col-lg-12 control-label']) }}
                                    {{ Form::text('construction_dimensions', null, ['class' => 'form-control', 'maxlength' => '20']) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('construction_sidedoor', trans('trucks.fields.construction_sidedoor'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('construction_sidedoor', 1, false) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('construction_remarks', trans('trucks.fields.construction_remarks'), ['class' => 'col-lg-12 control-label']) }}
                                    {{ Form::textarea('construction_remarks', null, ['class' => 'form-control', 'maxlength' => '1000']) }}
                                </div><!--form control-->

                            </div>
                            <div class="tab-pane fade" id="tab4default">
                                <div class="form-group col-xs-12">
                                    {{ Form::label('weights_empty', trans('trucks.fields.weights_empty'), ['class' => 'col-lg-12 control-label']) }}
                                    {{ Form::text('weights_empty', null, ['class' => 'form-control']) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('weights_mtm', trans('trucks.fields.weights_mtm'), ['class' => 'col-lg-12 control-label']) }}
                                    {{ Form::text('weights_mtm', null, ['class' => 'form-control']) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('weights_useful_load_capacity', trans('trucks.fields.weights_useful_load_capacity'), ['class' => 'col-lg-12 control-label']) }}
                                    {{ Form::text('weights_useful_load_capacity', null, ['class' => 'form-control', 'maxlength' => '5']) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('weights_useful_tow_capacity', trans('trucks.fields.weights_useful_tow_capacity'), ['class' => 'col-lg-12 control-label']) }}
                                    {{ Form::text('weights_useful_tow_capacity', null, ['class' => 'form-control', 'maxlength' => '5']) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('weights_max_trailer_load', trans('trucks.fields.weights_max_trailer_load'), ['class' => 'col-lg-12 control-label']) }}
                                    {{ Form::text('weights_max_trailer_load', null, ['class' => 'form-control', 'maxlength' => '5']) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('weights_driving_license', trans('trucks.fields.weights_driving_license'), ['class' => 'col-lg-12 control-label']) }}
                                    {{ Form::select('weights_driving_license', ['B' => 'B', 'C' =>'C'], 'C', ['class' => 'form-control']) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('weights_eurovignet', trans('trucks.fields.weights_eurovignet'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('weights_eurovignet', 1, false) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('weights_remarks', trans('trucks.fields.weights_remarks'), ['class' => 'col-lg-12 control-label']) }}
                                    {{ Form::textarea('weights_remarks', null, ['class' => 'form-control', 'maxlength' => '1000']) }}
                                </div><!--form control-->
                            </div>
                            <div class="tab-pane fade" id="tab5default">
                                <div class="form-group col-xs-12">
                                    {{ Form::text('filter_equipment', null, ['class' => 'form-control', 'placeholder' => trans('trucks.fields.start_typing_to_filter'), 'id' => 'filter_equipment']) }}
                                </div>
                                <div class="form-group col-xs-12">
                                    {{ Form::label('chassis_air_suspension', trans('trucks.fields.chassis_air_suspension'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('chassis_air_suspension', 1, false) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('chassis_cruise_control', trans('trucks.fields.chassis_cruise_control'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('chassis_cruise_control', 1, false )}}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_airbags', trans('trucks.fields.equipment_airbags'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_airbags', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_airbags'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_rockinger', trans('trucks.fields.equipment_rockinger'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_rockinger', 1, false), ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_rockinger'))] }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_roofspoiler', trans('trucks.fields.equipment_roofspoiler'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_roofspoiler', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_roofspoiler'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_automatic_airco', trans('trucks.fields.equipment_automatic_airco'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_automatic_airco', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_automatic_airco'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_airco', trans('trucks.fields.equipment_airco'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_airco', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_airco'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_airsprung_seat', trans('trucks.fields.equipment_airsprung_seat'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_airsprung_seat', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_airsprung_seat'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_pto', trans('trucks.fields.equipment_pto'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_pto', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_pto'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_abs', trans('trucks.fields.equipment_abs'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_abs', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_abs'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_rear_camera_color', trans('trucks.fields.equipment_rear_camera_color'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_rear_camera_color', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_rear_camera_color'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_rear_camera', trans('trucks.fields.equipment_rear_camera'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_rear_camera', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_rear_camera'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_alarm', trans('trucks.fields.equipment_alarm'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_alarm', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_alarm'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_motor_break', trans('trucks.fields.equipment_motor_break'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_motor_break', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_motor_break'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_retarder', trans('trucks.fields.equipment_retarder'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_retarder', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_retarder'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_aluminum_rims', trans('trucks.fields.equipment_aluminum_rims'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_aluminum_rims', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_aluminum_rims'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_cd_player', trans('trucks.fields.equipment_cd_player'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_cd_player', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_cd_player'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_crane', trans('trucks.fields.equipment_crane'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_crane', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_crane'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_sleeping_cabin', trans('trucks.fields.equipment_sleeping_cabin'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_sleeping_cabin', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_sleeping_cabin'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_central_locking', trans('trucks.fields.equipment_central_locking'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_central_locking', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_central_locking'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_double_rear_tires', trans('trucks.fields.equipment_double_rear_tires'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_double_rear_tires', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_double_rear_tires'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_roof_rails', trans('trucks.fields.equipment_roof_rails'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_roof_rails', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_roof_rails'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_electric_windows', trans('trucks.fields.equipment_electric_windows'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_electric_windows', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_electric_windows'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_electric_mirrors', trans('trucks.fields.equipment_electric_mirrors'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_electric_mirrors', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_electric_mirrors'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_gps', trans('trucks.fields.equipment_gps'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_gps', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_gps'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_headlamp_sprayers', trans('trucks.fields.equipment_headlamp_sprayers'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_headlamp_sprayers', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_headlamp_sprayers'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_multifunctional_wheel', trans('trucks.fields.equipment_multifunctional_wheel'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_multifunctional_wheel', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_multifunctional_wheel'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_non_smoker', trans('trucks.fields.equipment_non_smoker'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_non_smoker', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_non_smoker'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_sunroof', trans('trucks.fields.equipment_sunroof'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_sunroof', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_sunroof'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_pdf', trans('trucks.fields.equipment_pdf'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_pdf', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_pdf'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_rainsensor', trans('trucks.fields.equipment_rainsensor'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_rainsensor', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_rainsensor'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_stability_control', trans('trucks.fields.equipment_stability_control'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_stability_control', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_stability_control'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_night_heating', trans('trucks.fields.equipment_night_heating'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_night_heating', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_night_heating'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_power_steering', trans('trucks.fields.equipment_power_steering'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_power_steering', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_power_steering'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_phone', trans('trucks.fields.equipment_phone'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_phone', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_phone'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_towing_hook', trans('trucks.fields.equipment_towing_hook'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_towing_hook', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_towing_hook'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_detachable_towing_hook', trans('trucks.fields.equipment_detachable_towing_hook'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_detachable_towing_hook', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_detachable_towing_hook'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_tv', trans('trucks.fields.equipment_tv'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_tv', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_tv'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_lowered_suspension', trans('trucks.fields.equipment_lowered_suspension'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_lowered_suspension', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_lowered_suspension'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_heated_mirrors', trans('trucks.fields.equipment_heated_mirrors'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_heated_mirrors', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_heated_mirrors'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_heated_seats', trans('trucks.fields.equipment_heated_seats'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_heated_seats', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_heated_seats'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_xenon_headlights', trans('trucks.fields.equipment_xenon_headlights'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_xenon_headlights', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_xenon_headlights'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_sunvisor', trans('trucks.fields.equipment_sunvisor'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_sunvisor', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_sunvisor'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_reserve_differential', trans('trucks.fields.equipment_reserve_differential'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_reserve_differential', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_reserve_differential'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_electropack', trans('trucks.fields.equipment_electropack'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('equipment_electropack', 1, false, ['id' => str_replace(' ', '_', trans('trucks.fields.equipment_electropack'))]) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('equipment_terms_remarks', trans('trucks.fields.equipment_terms_remarks'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::textarea('equipment_terms_remarks', null, ['class' => 'form-control', 'maxlength' => '1000']) }}
                                </div><!--form control-->

                            </div>
                            <div class="tab-pane fade" id="tab6default">
                                <div class="form-group col-xs-12">
                                    {{ Form::label('delivery_terms_technical_inspection', trans('trucks.fields.delivery_terms_technical_inspection'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('delivery_terms_technical_inspection', 1, true) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('delivery_terms_legal_kit', trans('trucks.fields.delivery_terms_legal_kit'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('delivery_terms_legal_kit', 1, true) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('delivery_terms_cleaning', trans('trucks.fields.delivery_terms_cleaning'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('delivery_terms_cleaning', 1, true) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('delivery_terms_calibration_tachograph', trans('trucks.fields.delivery_terms_calibration_tachograph'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('delivery_terms_calibration_tachograph', 1, false) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('delivery_terms_pvg', trans('trucks.fields.delivery_terms_pvg'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('delivery_terms_pvg', 1, true) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('delivery_terms_technical_checkup', trans('trucks.fields.delivery_terms_technical_checkup'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('delivery_terms_technical_checkup', 1, true) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('delivery_terms_guarantee', trans('trucks.fields.delivery_terms_guarantee'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('delivery_terms_guarantee', 1, false) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('delivery_terms_new_tires', trans('trucks.fields.delivery_terms_new_tires'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::checkbox('delivery_terms_new_tires', 1, false) }}
                                </div><!--form control-->
                                <div class="form-group col-xs-12">
                                    {{ Form::label('delivery_terms_remarks', trans('trucks.fields.delivery_terms_remarks'), ['class' => 'col-lg-2 control-label']) }}
                                    {{ Form::textarea('delivery_terms_remarks', null, ['class' => 'form-control', 'maxlength' => '1000']) }}
                                </div>
                                <div class="form-group col-xs-2">
                                    {{ Form::checkbox('default_text_2', 1, true) }}
                                </div>
                                <div class="form-group col-xs-10">
                                    <span id="default_text_2">{!! $defaultText2 !!}</span>
                                    <a href="#" id="edit_default_text_2">{!! trans('trucks.fields.edit') !!}</a>
                                </div><!--form control-->
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        {!! Form::submit(trans('trucks.fields.create_new_truck'), array('class' => 'btn btn-primary', 'id'=>'submit_button')) !!}
                    </div>
                </div>


                {!! Form::close() !!}
            </div>

        <!-- Modal (Pop up when add brand button clicked) -->
        <div class="modal fade" id="modalBrands" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">{!! trans('trucks.fields.add_brand') !!}</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['url' => '/admin/truckBrands', 'files' => true, 'method' =>  'POST']) !!}

                        <div class="form-group">
                            {!! Form::label('brandName', trans('trucks.fields.name')) !!}
                            {!! Form::text('brandName', null, array('class' => 'form-control')) !!}
                        </div>

                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary ajaxbutton" id="btn-save-brand" value="add">{!! trans('trucks.fields.add_brand') !!}</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal (Pop up when add engine button clicked) -->
        <div class="modal fade" id="modalEngines" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">{!! trans('trucks.fields.add_engine') !!}</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['url' => '/admin/truckEngines', 'files' => true, 'method' =>  'POST']) !!}

                        <div class="form-group">
                            {!! Form::label('engineName', trans('trucks.fields.name')) !!}
                            {!! Form::text('engineName', null, array('class' => 'form-control')) !!}
                        </div>

                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary ajaxbutton" id="btn-save-engine" value="add">{!! trans('trucks.fields.add_engine') !!}</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal (Pop up when add construction lift power button clicked) -->
        <div class="modal fade" id="modalConstructionLiftPowers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">{!! trans('trucks.fields.add_construction_lift_power') !!}</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['url' => '/admin/truckConstructionLiftPower', 'files' => true, 'method' =>  'POST']) !!}

                        <div class="form-group">
                            {!! Form::label('constructionLiftPowerName', trans('trucks.fields.construction_lift_power')) !!}
                            {!! Form::text('constructionLiftPowerName', null, array('class' => 'form-control')) !!}
                        </div>

                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary ajaxbutton" id="btn-save-construction-lift-power" value="add">{!! trans('trucks.fields.add_construction_lift_power') !!}</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal (Pop up when edit default text clicked) -->
        <div class="modal fade" id="modalDefaultText" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">{!! trans('trucks.fields.defaultText') !!}</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['url' => '/admin/defaultText', 'files' => false, 'method' =>  'POST']) !!}

                        <div class="form-group">
                            {{ Form::label('default_text_edit', trans('trucks.fields.defaultText'), ['class' => 'col-lg-2 control-label']) }}
                            {{ Form::textarea('default_text_edit', $defaultText1, ['class' => 'form-control']) }}
                            {{ Form::hidden('default_text_type', '', array('id' => 'default_text_type')) }}
                        </div>

                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary ajaxbutton" id="btn-save-default_text" value="edit">{!! trans('trucks.fields.edit') !!}</button>
                    </div>
                </div>
            </div>
        </div>

        @endauth

@endsection

@section('after-scripts')
    {!! Html::script('js/backend/plugin/jquery-file-upload/vendor/jquery.ui.widget.js') !!}
    {!! Html::script('js/backend/plugin/jquery-file-upload/jquery.iframe-transport.js') !!}
    {!! Html::script('js/backend/plugin/jquery-file-upload/jquery.fileupload.js') !!}
    {!! Html::script('js/backend/plugin/jquery-ui/jquery-ui.js') !!}
    {!! Html::script('js/backend/trucks/ajax-crud.js') !!}
@endsection

@section('css')
    {!! HTML::style('css/backend/plugin/jquery-file-upload/style.css') !!}
    {!! HTML::style('css/backend/plugin/jquery-file-upload/jquery.fileupload.css') !!}
    {!! HTML::style('css/backend/plugin/jquery-ui/jquery-ui.theme.css') !!}
    {!! HTML::style('css/backend/plugin/jquery-ui/jquery-ui.structure.css') !!}
@endsection