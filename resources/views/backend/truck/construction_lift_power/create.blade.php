@extends('frontend.layouts.app')

@section('content')
    <div class="row">

        @role('Executive')
            <div class="row">
                {!!Html::ul($errors->all()) !!}
                <H1>Create new brand</H1>
                {!! Form::open(['url' => '/admin/truckConstructionLiftPowers', 'files' => true, 'method' =>  'POST']) !!}

                <div class="form-group">
                    {{ Form::label('name', trans('trucks.fields.construction_lift'), ['class' => 'col-lg-2 control-label']) }}
                    {{ Form::text('name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus']) }}
                </div>


            {!! Form::submit('Create the lift power', array('class' => 'btn btn-primary')) !!}

            {!! Form::close() !!}


            </div>

        @endauth

    </div><!--row-->
@endsection
