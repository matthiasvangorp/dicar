<ul class="dropdown-menu" role="menu">
    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
        @if ($localeCode != App::getLocale())
            <li><a href="{!! url('lang/'.$localeCode) !!}" ref="alternate" hreflang="{!! $localeCode !!}"><span class="lang-sm lang-lbl-'{!!$localeCode!!}" lang="{!!$localeCode!!}"></span> </a></li>
        @endif
    @endforeach
</ul>