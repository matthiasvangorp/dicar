{!! $content['title'] !!}<br/><br>

Naam : {!! $content['name'] !!}<br/>
Email : {!! $content['email'] !!}<br/>
Adres : {!! $content['address'] !!}<br/>
Postcode : {!! $content['zipcode'] !!}<br/>
Stad : {!! $content['city'] !!}<br/>
Telefoon : {!! $content['phone'] !!}<br/>
Merk : {!! $content['brand'] !!}<br/>
Model : {!! $content['model'] !!}<br/>
Bouwjaar : {!! $content['year'] !!}<br/>
Aantal km : {!! $content['kilometers'] !!}<br/>
Onderhoudsboekje : {!! $content['maintenance_book'] !!}<br/>
Ongevalvrij : {!! $content['accident_free'] !!}<br/>
Naam : {!! $content['name'] !!}<br/>
PK : {!! $content['horsepower'] !!}<br/>
Kleur : {!! $content['color'] !!}<br/>
Vraagprijs : {!! $content['asking_price'] !!}<br/>
BTW inclusief : {!! $content['vat_inclusive'] !!}<br/>
Particulier : {!! $content['private'] !!}<br/>
Opties : {!! $content['options'] !!}<br/>
Opmerkingen : {!! $content['remarks'] !!}<br/>

