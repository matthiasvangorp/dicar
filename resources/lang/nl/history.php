<?php

return [

    /*
    |--------------------------------------------------------------------------
    | History Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain strings associated to the
    | system adding lines to the history table.
    |
    */

    'backend' => [
        'none'            => 'Er is geen recente geschiedenis.',
        'none_for_type'   => 'Er is geen geschiedenis voor dit type.',
        'none_for_entity' => 'Er is geen geschiedenis voor deze :entity.',
        'recent_history'  => 'Recente Geschiedenis',

        'roles' => [
            'created' => 'rol aangemaakt',
            'deleted' => 'rol verwijdert',
            'updated' => 'rol aangepast',
        ],
        'users' => [
            'changed_password'    => 'wachtwoord verandert voor gebruiker',
            'created'             => 'gebruiker aangemaakt',
            'deactivated'         => 'gebruiker gedeactiveerd',
            'deleted'             => 'gebruiker verwijdert',
            'permanently_deleted' => 'gebruiker permanent verwijdert',
            'updated'             => 'gebruiker aangepast',
            'reactivated'         => 'gebruiker geheractiveerd',
            'restored'            => 'gebruiker hersteld',
        ],
        'motorhomes' => [
          'created'             => 'motorhome aangemaakt',
          'deleted'             => 'motorhome verwijderd',
          'permanently_deleted' => 'motorhome permanent verwijderd',
          'updated'             => 'motorhome aangepast',
          'restored'            => 'motorhome hersteld',
        ],
        'trucks' => [
          'created'             => 'vrachtwagen aangemaakt',
          'deleted'             => 'vrachtwagen verwijderd',
          'permanently_deleted' => 'vrachtwagen permanent verwijderd',
          'updated'             => 'vrachtwagen aangepast',
          'restored'            => 'vrachtwagen hersteld',
        ],
    ],
];
