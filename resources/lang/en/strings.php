<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Strings Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in strings throughout the system.
    | Regardless where it is placed, a string can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'users' => [
                'delete_user_confirm'  => 'Are you sure you want to delete this user permanently? Anywhere in the application that references this user\'s id will most likely error. Proceed at your own risk. This can not be un-done.',
                'if_confirmed_off'     => '(If confirmed is off)',
                'restore_user_confirm' => 'Restore this user to its original state?',
            ],
        ],

        'dashboard' => [
            'title'   => 'Administrative Dashboard',
            'welcome' => 'Welcome',
        ],

        'general' => [
            'all_rights_reserved' => 'All Rights Reserved.',
            'are_you_sure'        => 'Are you sure you want to do this?',
            'boilerplate_link'    => 'Laravel 5 Boilerplate',
            'site_name'           => 'Dicar trucks',
            'continue'            => 'Continue',
            'member_since'        => 'Member since',
            'minutes'             => ' minutes',
            'search_placeholder'  => 'Search...',
            'timeout'             => 'You were automatically logged out for security reasons since you had no activity in ',
            'show'                => 'Show',
            'edit'                => 'Edit',
            'delete'              => 'Delete',
            'export_delete'       => 'Delete Mobile',
            'name'                => 'Name',
            'brand'               => 'Brand',
            'reference'           => 'Reference',
            'add_new_truck'       => 'Add new truck',
            'general'             => 'General',
            'chassis'             => 'Chassis',
            'construction'        => 'Construction',
            'weights'             => 'Weights',
            'equipment'           => 'Equipment',
            'delivery_terms'      => 'Delivery Terms',
            'brand_already_exists'=> 'Brand already exists',
            'images'              => 'Images',
            'information'         => 'Information',
            'interested'          => 'Interested ? ',
            'insert_date'         => 'Date added',
            'export'              => 'Export',

            'see_all' => [
                'messages'      => 'See all messages',
                'notifications' => 'View all',
                'tasks'         => 'View all tasks',
            ],

            'status' => [
                'online'  => 'Online',
                'offline' => 'Offline',
            ],

            'you_have' => [
                'messages'      => '{0} You don\'t have messages|{1} You have 1 message|[2,Inf] You have :number messages',
                'notifications' => '{0} You don\'t have notifications|{1} You have 1 notification|[2,Inf] You have :number notifications',
                'tasks'         => '{0} You don\'t have tasks|{1} You have 1 task|[2,Inf] You have :number tasks',
            ],
        ],

        'search' => [
            'empty'      => 'Please enter a search term.',
            'incomplete' => 'You must write your own search logic for this system.',
            'title'      => 'Search Results',
            'results'    => 'Search Results for :query',
        ],

        'welcome' => '<p>This is the AdminLTE theme by <a href="https://almsaeedstudio.com/" target="_blank">https://almsaeedstudio.com/</a>. This is a stripped down version with only the necessary styles and scripts to get it running. Download the full version to start adding components to your dashboard.</p>
<p>All the functionality is for show with the exception of the <strong>Access Management</strong> to the left. This boilerplate comes with a fully functional access control library to manage users/roles/permissions.</p>
<p>Keep in mind it is a work in progress and their may be bugs or other issues I have not come across. I will do my best to fix them as I receive them.</p>
<p>Hope you enjoy all of the work I have put into this. Please visit the <a href="https://github.com/rappasoft/laravel-5-boilerplate" target="_blank">GitHub</a> page for more information and report any <a href="https://github.com/rappasoft/Laravel-5-Boilerplate/issues" target="_blank">issues here</a>.</p>
<p><strong>This project is very demanding to keep up with given the rate at which the master Laravel branch changes, so any help is appreciated.</strong></p>
<p>- Anthony Rappa</p>',
    ],

    'emails' => [
        'auth' => [
            'error'                   => 'Whoops!',
            'greeting'                => 'Hello!',
            'regards'                 => 'Regards,',
            'trouble_clicking_button' => 'If you’re having trouble clicking the ":action_text" button, copy and paste the URL below into your web browser:',
            'thank_you_for_using_app' => 'Thank you for using our application!',

            'password_reset_subject'    => 'Reset Password',
            'password_cause_of_email'   => 'You are receiving this email because we received a password reset request for your account.',
            'password_if_not_requested' => 'If you did not request a password reset, no further action is required.',
            'reset_password'            => 'Click here to reset your password',

            'click_to_confirm' => 'Click here to confirm your account:',
        ],
    ],

    'frontend' => [
        'test' => 'Test',
        'interested' => 'Interested ?',

        'tests' => [
            'based_on' => [
                'permission' => 'Permission Based - ',
                'role'       => 'Role Based - ',
            ],

            'js_injected_from_controller' => 'Javascript Injected from a Controller',

            'using_blade_extensions' => 'Using Blade Extensions',

            'using_access_helper' => [
                'array_permissions'     => 'Using Access Helper with Array of Permission Names or ID\'s where the user does have to possess all.',
                'array_permissions_not' => 'Using Access Helper with Array of Permission Names or ID\'s where the user does not have to possess all.',
                'array_roles'           => 'Using Access Helper with Array of Role Names or ID\'s where the user does have to possess all.',
                'array_roles_not'       => 'Using Access Helper with Array of Role Names or ID\'s where the user does not have to possess all.',
                'permission_id'         => 'Using Access Helper with Permission ID',
                'permission_name'       => 'Using Access Helper with Permission Name',
                'role_id'               => 'Using Access Helper with Role ID',
                'role_name'             => 'Using Access Helper with Role Name',
            ],

            'view_console_it_works'          => 'View console, you should see \'it works!\' which is coming from FrontendController@index',
            'you_can_see_because'            => 'You can see this because you have the role of \':role\'!',
            'you_can_see_because_permission' => 'You can see this because you have the permission of \':permission\'!',
        ],

        'user' => [
            'change_email_notice' => 'If you change your e-mail you will be logged out until you confirm your new e-mail address.',
            'email_changed_notice' => 'You must confirm your new e-mail address before you can log in again.',
            'profile_updated'  => 'Profile successfully updated.',
            'password_updated' => 'Password successfully updated.',
        ],

        'welcome_to' => 'Welcome to :place',
        'sleepingPlaces' => 'Sleeping places',
        'seats' => 'Seats',
        'site_name'           => 'Dicar trucks',
        'secondhand_trucks' => 'Secondhand trucks',
        'engine'            => 'Engine',
        'color'             => 'Color',
        'mileage'           => 'Mileage',
        'construction'      => 'Construction',
        'dimensions'        => 'Dimensions',
        'tailgate'          => 'Tailgate',
        'mtm'               => 'MAM',
        'close'             => 'Close',
        'filter'            => 'Filter',
        'sort_by'           => 'Sort by',
        'used'              => 'Used',
        'horsepower'        => 'KW/HP',
        'hp'                => 'HP',
        'kw'                => 'KW',
      ],
      'colors' => [
        'white'   => 'White',
        'black'   => 'Black',
        'green'   => 'Green',
        'beige'   => 'Beige',
        'blue'    => 'Blue',
        'red'     => 'Red',
        'yellow'  => 'Yellow',
        'grey'    => 'Grey'
      ],
      'construction_types' => [
        'trunck' => 'Trunck',
        'open_trailer' => 'Open trailer',
        'van' => 'Van',
        'van_high_long' => 'Van high + long',
        'chassis_cab' => 'Chassis cab',
        'sail' => 'Sail',
        'trunck_lift' => 'Trunck + lift',
        'trunck_doors' => 'Trunck + doors'
      ],
      'drivetrains' => [
        'front' => 'Front',
        'back' => 'Back',
      ],
      'contact' => [
        'address' => 'Adress',
        'dicar_trucks' => 'Dicar Trucks',
        'directions' => 'Just along the exit Geel-west of the E313 Antwerp-Hasselt',
        'phone' => 'Phone',
        'email' => 'Emailaddress',
        'contactperson' => 'Contact',
        'hours' => 'Business hours',
        'monday' => 'Monday: 10h - 19h',
        'tuesday' => 'Tuesday: 10h - 19h',
        'wednesday' => 'Wednesday: 10h - 19h',
        'thursday' => 'Thursday: 10h - 19h',
        'friday' => 'Friday: 10h - 19h',
        'saturday' => 'Saturday: 9h - 16h',
        'appointment' => 'Dear visitor, to be able to serve you faster and better, please call a phone call or email for an appointment.',
        'belgium' => 'Belgium',
        'message' => 'Message',
        'interest' => 'Interest in',
        'contact_us' => 'Contact us'
      ],
      'sell' => [
        'sell_your_truck' => 'Sell your truck',
        'contact' => 'Contact',
        'name' => 'Name',
        'address' => 'Address',
        'zipcode' => 'Zipdcode',
        'city' => 'City',
        'phone' => 'Phone',
        'email' => 'Email',
        'brand' => 'Brand',
        'model' => 'Model',
        'year' => 'Construction year',
        'kilometers' => 'Number of kilometers',
        'maintenance_book' => 'Maintenance book',
        'accident_free' => 'Accident free',
        'horsepower' => 'Horsepower',
        'color' => 'Color',
        'asking_price'=> 'Asking Price',
        'vat_inclusive' => 'VAT Included',
        'private' => 'I am a private person',
        'options' => 'Options',
        'remarks' => 'Remarks',
        'send' => 'Send',
        'thanks_for_contacting_us' => 'Thank you for contacting us'
      ],
      'footer' => [
        'dicar_motorhomes' => 'Dicar Motorhomes',
        'designed' => 'Designed and developed by',
        'seo_website_promotie' => 'SEO Website Promotion',
      ]



];
