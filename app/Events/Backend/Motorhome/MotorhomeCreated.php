<?php

namespace App\Events\Backend\Motorhome;

use App\Models\Motorhome\Motorhome;
use Illuminate\Queue\SerializesModels;
use App\Models\Access\User\User;

/**
 * Class MotorhomeCreated.
 */
class MotorhomeCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $event;

  /**
   * MotorhomeCreated constructor.
   * @param \stdClass $event
   */
    public function __construct(\stdClass $event)
    {
        $this->event = $event;
    }
}
