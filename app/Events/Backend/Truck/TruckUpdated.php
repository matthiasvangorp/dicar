<?php

namespace App\Events\Backend\Truck;

use Illuminate\Queue\SerializesModels;
use App\Models\Access\User\User;

/**
 * Class TruckUpdated.
 */
class TruckUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $event;

  /**
   * TruckUpdated constructor.
   * @param \stdClass $event
   */
    public function __construct(\stdClass $event)
    {
        $this->event = $event;
    }
}
