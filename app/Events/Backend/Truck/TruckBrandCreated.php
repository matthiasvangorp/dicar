<?php

namespace App\Events\Backend\Truck;

use Illuminate\Queue\SerializesModels;
use App\Models\Access\User\User;

/**
 * Class TruckBrandCreated.
 */
class TruckBrandCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $event;

  /**
   * TruckCreated constructor.
   * @param \stdClass $event
   */
    public function __construct(\stdClass $event)
    {
        $this->event = $event;
    }
}
