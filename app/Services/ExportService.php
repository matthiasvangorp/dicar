<?php
namespace App\Services;

use App\Repositories\Backend\Truck\TruckRepository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class ExportService
{
    /**
     * ExportController constructor.
     * @param \App\Repositories\Backend\Truck\TruckRepository $trucks
     * @param $username
     * @param $password
     */
    public function __construct(
      TruckRepository $trucks
    )
    {
        $this->trucks = $trucks;
        $this->username = config('mobile.username');
        $this->password = config('mobile.password');
    }

    /**
     * @var TruckRepository
     */
    protected $trucks;




    public function createAd($truckId){
        $sellerID = $this->getSellerId();
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://services.mobile.de/seller-api/sellers/$sellerID/ads");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

        if (App::environment() != "production") {
            curl_setopt($ch, CURLOPT_PROXY,'http://api.test.sandbox.mobile.de:8080');
        }

        $ad = $this->trucks->createAd($truckId, $sellerID);

        $headers = array();
        $headers[] = "Accept: application/vnd.de.mobile.api+json";
        $headers[] = "Content-Type: application/vnd.de.mobile.api+json";
        if (App::environment() != "production") {
            $headers[] = "X-MOBILE-SELLER-TOKEN: $this->username:$this->password";
        }
        else {
            $headers[] = 'Authorization: Basic ' . base64_encode("$this->username:$this->password");
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_HEADER, 1);

        $serializedAd = json_encode($ad->jsonSerialize());
        curl_setopt($ch, CURLOPT_POSTFIELDS, $serializedAd);

        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $data=explode("\r\n",$result);



        if ($httpcode != 201){
            $errors = json_decode(end($data));
            Log::error("Error while posting truck $truckId to Mobile.de (httpcode : $httpcode)");
            Log::error(json_encode($ad->jsonSerialize()));
            Log::error(serialize($errors));
            flash("Probleem met exporteren truck. Contacteer Matthias zodat hij het kan oplossen.")->error();
            return redirect('admin/trucks/');

        }

        curl_close($ch);


        foreach ($data as $key=>$value){
            if (starts_with($value, "Location")){
                $locationArray = explode("/", $data[$key]);
            }
        }
        $adId = end($locationArray); //get the last parameter from the location url header

        return $adId;
    }



    public function updateAd($truckId){
        $adId = $this->trucks->getAdId($truckId);
        $sellerID = $this->getSellerId();
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://services.mobile.de/seller-api/sellers/$sellerID/ads/$adId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

        if (App::environment() != "production") {
            curl_setopt($ch, CURLOPT_PROXY,'http://api.test.sandbox.mobile.de:8080');
        }

        $ad = $this->trucks->createAd($truckId, $sellerID);

        $headers = array();
        $headers[] = "Accept: application/vnd.de.mobile.api+json";
        $headers[] = "Content-Type: application/vnd.de.mobile.api+json";
        if (App::environment() != "production") {
            $headers[] = "X-MOBILE-SELLER-TOKEN: $this->username:$this->password";
        }
        else {
            $headers[] = 'Authorization: Basic ' . base64_encode("$this->username:$this->password");
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_HEADER, 1);

        $serializedAd = json_encode($ad->jsonSerialize());
        curl_setopt($ch, CURLOPT_POSTFIELDS, $serializedAd);

        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $data=explode("\r\n",$result);



        if ($httpcode != 200){
            $errors = json_decode(end($data));
            Log::error("Error while posting truck $truckId to Mobile.de (httpcode : $httpcode)");
            Log::error(json_encode($ad->jsonSerialize()));
            Log::error(serialize($errors));
            flash("Probleem met exporteren truck. Contacteer Matthias zodat hij het kan oplossen.")->error();
            return redirect('admin/trucks/');

        }

        curl_close($ch);

        return $adId;
    }


    public function uploadPictures($adId, $truckId){
        if ($adId) {
            $sellerID = $this->getSellerId();
            $ch = curl_init();

            $url = "https://services.mobile.de/seller-api/sellers/$sellerID/ads/$adId/images";

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

            if (App::environment() != "production") {
                curl_setopt($ch, CURLOPT_PROXY,
                  'http://api.test.sandbox.mobile.de:8080');
            }


            $images = $this->trucks->getImagesForExport($truckId);
            $i = 0;
            foreach ($images as $image) {
                $i++;
                if ($i <= 15) {
                    $imageFile = public_path() . '/images/trucks/' . $truckId . '/' . $image->filename;
                    $files[] = $imageFile;
                }
            }

            $postfields = $this->curl_custom_postfields($files);


            $headers = array();
            $headers[] = "Accept: application/vnd.de.mobile.api+json";
            $headers[] = "Content-Type: multipart/form-data; boundary=$postfields[1]";
            if (App::environment() != "production") {
                $headers[] = "X-MOBILE-SELLER-TOKEN: $this->username:$this->password";
            } else {
                $headers[] = 'Authorization: Basic ' . base64_encode("$this->username:$this->password");
            }


            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields[0]);

            $result = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if ($httpcode != 200) {
                $errors = json_decode(end($data));
                Log::error("Error while putting images for truck $truckId to Mobile.de (httpcode : $httpcode)");
                Log::error(serialize($errors));
                flash("Probleem met exporteren afbeeldingen truck. Contacteer Matthias zodat hij het kan oplossen.")->error();
                return redirect('admin/trucks/');

            }

            $data = explode("\r\n", $result);
        }

        return true;
    }


    private function curl_custom_postfields (array $files = array())    {

        // build file parameters
        foreach ($files as $k => $v) {
            switch (true) {
                case false === $v = realpath(filter_var($v)):
                case !is_file($v):
                case !is_readable($v):
                    continue; // or return false, throw new InvalidArgumentException
            }
            $data = file_get_contents($v);
            $body[] = implode("\r\n", array(
              "Content-Disposition: form-data; name=\"image\"",
              "Content-Type: image/jpeg",
              "",
              $data,
            ));
        }

        // generate safe boundary
        do {
            $boundary = "---------------------" . md5(mt_rand() . microtime());
        } while (preg_grep("/{$boundary}/", $body));

        // add boundary for each parameters
        array_walk($body, function (&$part) use ($boundary) {
            $part = "--{$boundary}\r\n{$part}";
        });

        // add final boundary
        $body[] = "--{$boundary}--";
        $body[] = "";

        // set options
        return array(implode("\r\n", $body), $boundary);
    }

    public function getSellerId(){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://services.mobile.de/seller-api/sellers");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        if (App::environment() != "production") {
            curl_setopt($ch, CURLOPT_PROXY,'http://api.test.sandbox.mobile.de:8080');
        }
        $headers = array();
        $headers[] = "Accept: application/vnd.de.mobile.api+json";
        if (App::environment() != "production") {
            $headers[] = "X-MOBILE-SELLER-TOKEN: $this->username:$this->password";
        }
        else {
            curl_setopt($ch, CURLOPT_USERPWD, "$this->username" . ":" . "$this->password");
        }


        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpcode != 200){
            $errors = json_decode(end($data));
            Log::error("Error while getting sellerId (httpcode : $httpcode)");
            Log::error(serialize($errors));
            flash("Probleem met exporteren vrachtwagen. Contacteer Matthias zodat hij het kan oplossen.")->error();
            return redirect('admin/trucks/');
        }

        $data=json_decode($result);

        curl_close($ch);


        $sellerID = $data->sellers[0]->mobileSellerId;

        return $sellerID;
    }

    public function deleteImages($adId){
        $sellerID = $this->getSellerId();
        $ch = curl_init();

        $url = "https://services.mobile.de/seller-api/sellers/$sellerID/ads/$adId/images";

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

        if (App::environment() != "production") {
            curl_setopt($ch, CURLOPT_PROXY,'http://api.test.sandbox.mobile.de:8080');
        }
        $headers = array();
        $headers[] = "Accept: application/vnd.de.mobile.api+json";
        if (App::environment() != "production") {
            $headers[] = "X-MOBILE-SELLER-TOKEN: $this->username:$this->password";
        }
        else {
            curl_setopt($ch, CURLOPT_USERPWD, "$this->username" . ":" . "$this->password");
        }


        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpcode != 204){
            $errors = json_decode(end($data));
            Log::error("Error while deleting pictures (httpcode : $httpcode)");
            Log::error(serialize($errors));
            flash("Probleem met exporteren vrachtwagen. Contacteer Matthias zodat hij het kan oplossen.")->error();
            return redirect('admin/trucks/');
        }

    }



    public function exportTruck($truckId){
        /*
         * check if add already exists.
         */
        $adId = $this->trucks->getAdId($truckId);
        if ($adId){
            //update
            $adId = $this->updateAd($truckId);

            //remove all current images
            $this->deleteImages($adId);

            //upload new pictures
            $result = $this->uploadPictures($adId, $truckId);

            return $adId;

        }
        else {
            //insert

            /** Upload the truck information */
            $adId = $this->createAd($truckId);
            /** Finished uploading the truck information */

            //You’ll receive mobileAdId as reference after the ad is created. Use this endpoint to enrich it with images.

            $result = $this->uploadPictures($adId, $truckId);

            $this->trucks->saveAdId($truckId, $adId);

            return $adId;
        }


    }


    public function deleteTruck($truckId){
        $adId = $this->trucks->getAdId($truckId);
        if ($adId) {
            $sellerID = $this->getSellerId();
            $ch = curl_init();

            $url = "https://services.mobile.de/seller-api/sellers/$sellerID/ads/$adId";

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

            if (App::environment() != "production") {
                curl_setopt($ch, CURLOPT_PROXY,
                  'http://api.test.sandbox.mobile.de:8080');
            }
            $headers = array();
            $headers[] = "Accept: application/vnd.de.mobile.api+json";
            if (App::environment() != "production") {
                $headers[] = "X-MOBILE-SELLER-TOKEN: $this->username:$this->password";
            } else {
                curl_setopt($ch, CURLOPT_USERPWD,
                  "$this->username" . ":" . "$this->password");
            }


            $result = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($httpcode != 204 && $httpcode != 200) {
                $errors = json_decode(end($data));
                Log::error("Error while deleting pictures (httpcode : $httpcode)");
                Log::error(serialize($errors));
                flash("Probleem met exporteren vrachtwagen. Contacteer Matthias zodat hij het kan oplossen.")->error();
                return redirect('admin/trucks/');
            }

            $this->trucks->removeAdId($truckId);
            return $adId;
        }
        else {
            return $truckId;
        }

    }
}