<?php

namespace App\Repositories\Backend\Truck;

use App\Exceptions\GeneralException;
use App\Models\Truck\TruckImage;
use App\Repositories\BaseRepository;
use App\Models\Access\User\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Events\Backend\Truck\TruckCreated;

/**
 * Class TruckRepository.
 */
class TruckImageRepository implements TruckContract
{
    /**
     * Associated Repository Model.
     */
  const MODEL = TruckImage::class;

  private $paginationType = 'simplePaginate';


  /**
   * @param string $order_by
   * @param string $sort
   *
   * @return mixed
   */
  /**
   * @param null $limit
   * @param bool $paginate
   * @param int  $pagination
   *
   * @return string|\Symfony\Component\Translation\TranslatorInterface
   */
  public function render($limit = null, $paginate = true, $pagination = 10)
  {
    $images = TruckImage::pluck('name', 'id');
    return $images;
  }


  public function renderOne($id){
    $image = TruckImage::find($id);
    return $image;
  }

  /**
   * @param $history
   * @param bool $paginate
   *
   * @return string
   */
  public function buildList($history, $paginate = true)
  {
    return view('backend.history.partials.list', ['history' => $history, 'paginate' => $paginate])
      ->render();
  }

  /**
   * @param $query
   * @param $limit
   * @param $paginate
   * @param $pagination
   *
   * @return mixed
   */
  public function buildPagination($query, $limit, $paginate, $pagination)
  {
    if ($paginate && is_numeric($pagination)) {
      return $query->{$this->paginationType}($pagination);
    } else {
      if ($limit && is_numeric($limit)) {
        $query->take($limit);
      }

      return $query->get();
    }
  }


  /**
   * @param array $input
   */
  public function create($input)
  {
    $data = $input['data'];

    $image = $this->createImageStub($data);

    DB::transaction(function () use ($image) {
      if ($image->save()) {

        $image->save();

      } else {
        throw new GeneralException(trans('exceptions.backend.access.users.create_error'));
      }
    });

    return $image;

  }


    /**
     * @param array $input
     */
    public function update ($input){

    }


  /**
   * @param  $input
   *
   * @return mixed
   */
  protected function createImageStub($input)
  {
    $image = self::MODEL;
    $image = new $image;
    $image->filename = $input['filename'];

    return $image;
  }

}
