<?php

namespace App\Repositories\Backend\Truck;


use App\Models\Truck\TruckConstructionLiftPower;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Models\Access\User\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Events\Backend\Truck\TruckCreated;

/**
 * Class TruckRepository.
 */
class TruckConstructionLiftPowerRepository implements TruckContract
{
    /**
     * Associated Repository Model.
     */
  const MODEL = TruckConstructionLiftPower::class;

  private $paginationType = 'simplePaginate';


  /**
   * @param string $order_by
   * @param string $sort
   *
   * @return mixed
   */
  /**
   * @param null $limit
   * @param bool $paginate
   * @param int  $pagination
   *
   * @return string|\Symfony\Component\Translation\TranslatorInterface
   */
  public function render($limit = null, $paginate = true, $pagination = 10)
  {
    $constructionLiftPowers = TruckConstructionLiftPower::orderBy('name')->pluck('name', 'id');
    return $constructionLiftPowers;
  }


  public function renderOne($id){
    $constructionLiftPower = TruckConstructionLiftPower::find($id);
    return $constructionLiftPower;
  }

  /**
   * @param $history
   * @param bool $paginate
   *
   * @return string
   */
  public function buildList($history, $paginate = true)
  {
    return view('backend.history.partials.list', ['history' => $history, 'paginate' => $paginate])
      ->render();
  }

  /**
   * @param $query
   * @param $limit
   * @param $paginate
   * @param $pagination
   *
   * @return mixed
   */
  public function buildPagination($query, $limit, $paginate, $pagination)
  {
    if ($paginate && is_numeric($pagination)) {
      return $query->{$this->paginationType}($pagination);
    } else {
      if ($limit && is_numeric($limit)) {
        $query->take($limit);
      }

      return $query->get();
    }
  }


  /**
   * @param array $input
   */
  public function create($input)
  {
    $data = $input['data'];

    $constructionLiftPower = $this->createConstructionLiftPowerStub($data);

    DB::transaction(function () use ($constructionLiftPower) {
      if ($constructionLiftPower->save()) {
          $constructionLiftPower->save();

      } else {
        throw new GeneralException(trans('exceptions.backend.access.users.create_error'));
      }
    });

    return $constructionLiftPower;

  }


    /**
     * @param array $input
     */
    public function update ($input){

    }


  /**
   * @param  $input
   *
   * @return mixed
   */
  protected function createConstructionLiftPowerStub($input)
  {
    $constructionLiftPower = self::MODEL;
    $constructionLiftPower = new $constructionLiftPower;
    $constructionLiftPower->name = $input['name'];

    return $constructionLiftPower;
  }

    public function find($input){
      $result = TruckConstructionLiftPower::where('name',$input['name'])->first();
      return $result;
    }

}
