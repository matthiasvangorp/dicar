<?php

namespace App\Repositories\Backend\Truck;


use App\Models\Truck\TruckEngine;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Models\Access\User\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Events\Backend\Truck\TruckCreated;

/**
 * Class TruckRepository.
 */
class TruckEngineRepository implements TruckContract
{
    /**
     * Associated Repository Model.
     */
  const MODEL = TruckEngine::class;

  private $paginationType = 'simplePaginate';


  /**
   * @param string $order_by
   * @param string $sort
   *
   * @return mixed
   */
  /**
   * @param null $limit
   * @param bool $paginate
   * @param int  $pagination
   *
   * @return string|\Symfony\Component\Translation\TranslatorInterface
   */
  public function render($limit = null, $paginate = true, $pagination = 10)
  {
    $engines = TruckEngine::orderBy('name')->pluck('name', 'id');
    return $engines;
  }


  public function renderOne($id){
    $engine = TruckEngine::find($id);
    return $engine;
  }

  /**
   * @param $history
   * @param bool $paginate
   *
   * @return string
   */
  public function buildList($history, $paginate = true)
  {
    return view('backend.history.partials.list', ['history' => $history, 'paginate' => $paginate])
      ->render();
  }

  /**
   * @param $query
   * @param $limit
   * @param $paginate
   * @param $pagination
   *
   * @return mixed
   */
  public function buildPagination($query, $limit, $paginate, $pagination)
  {
    if ($paginate && is_numeric($pagination)) {
      return $query->{$this->paginationType}($pagination);
    } else {
      if ($limit && is_numeric($limit)) {
        $query->take($limit);
      }

      return $query->get();
    }
  }


  /**
   * @param array $input
   */
  public function create($input)
  {
    $data = $input['data'];

    $engine = $this->createEngineStub($data);

    DB::transaction(function () use ($engine) {
      if ($engine->save()) {
        $engine->save();

      } else {
        throw new GeneralException(trans('exceptions.backend.access.users.create_error'));
      }
    });

    return $engine;

  }


    /**
     * @param array $input
     */
    public function update ($input){

    }


  /**
   * @param  $input
   *
   * @return mixed
   */
  protected function createEngineStub($input)
  {
    $engine = self::MODEL;
    $engine = new $engine;
    $engine->name = $input['name'];

    return $engine;
  }

    public function find($input){
      $result = TruckEngine::where('name',$input['name'])->first();
      return $result;
    }

}
