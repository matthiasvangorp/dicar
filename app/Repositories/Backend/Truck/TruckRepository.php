<?php

namespace App\Repositories\Backend\Truck;

use App\Events\Backend\Truck\TruckUpdated;
use App\Models\Mobile\Advertisement;
use App\Models\Mobile\Price;
use App\Models\Mobile\TruckOver7500;
use App\Models\Mobile\VanUpTo7500;
use App\Models\Truck\Truck;
use App\Exceptions\GeneralException;
use App\Models\Truck\TruckBrand;
use App\Models\Truck\TruckImage;
use App\Repositories\BaseRepository;
use App\Models\Access\User\User;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Events\Backend\Truck\TruckCreated;

/**
 * Class TruckRepository.
 */
class TruckRepository implements TruckContract
{
    /**
     * Associated Repository Model.
     */
  const MODEL = Truck::class;

  private $paginationType = 'simplePaginate';


  /**
   * @param string $order_by
   * @param string $sort
   *
   * @return mixed
   */
  /**
   * @param null $limit
   * @param bool $paginate
   * @param int  $pagination
   *
   * @return string|\Symfony\Component\Translation\TranslatorInterface
   */
  public function render($limit = null, $paginate = true, $pagination = 10)
  {
      $trucks = Truck::join('truckBrands as tb', 'trucks.truckbrand_id', '=', 'tb.id')
          ->orderBy('tb.name', 'asc')
          ->select('trucks.*')->get();
      //$trucks = $this->buildPagination($trucks, $limit, $paginate, $pagination);
    foreach ($trucks as &$truck){
      $imagesString = $truck->images;
      $imagesArray = explode(';', $imagesString);
      $truck->image = $imagesArray[0];
    }

    return $trucks;
  }


  public function renderOne($id){
    $truck = Truck::find($id);
    $year = date_create_from_format('Y-m-d', $truck->year);
    $truck->year = $year->format('m/Y');
    return $truck;
  }

  /**
   * @param $history
   * @param bool $paginate
   *
   * @return string
   */
  public function buildList($history, $paginate = true)
  {
    return view('backend.history.partials.list', ['history' => $history, 'paginate' => $paginate])
      ->render();
  }

  /**
   * @param $query
   * @param $limit
   * @param $paginate
   * @param $pagination
   *
   * @return mixed
   */
  public function buildPagination($query, $limit, $paginate, $pagination)
  {
    if ($paginate && is_numeric($pagination)) {
      return $query->{$this->paginationType}($pagination);
    } else {
      if ($limit && is_numeric($limit)) {
        $query->take($limit);
      }

      return $query->get();
    }
  }


  /**
   * @param array $input
   */
  public function create($input)
  {
    $data = $input['data'];

    $truck = $this->createTruckStub($data);

    DB::transaction(function () use ($truck, $data) {
      if ($truck->save()) {
        //truck saved, now save the images
        $order = 0;
        foreach (explode(',', $data['image_order']) as $image){
            if ($image != "") {
                $order++;
                $tempImageDirectory = public_path() . "/images/trucks/temp/";
                $imageDirectory = public_path() . "/images/trucks/" . $truck->id . "/";
                if (!is_dir($imageDirectory)) {
                    mkdir($imageDirectory, 0777, true);
                }
                rename($tempImageDirectory . $image, $imageDirectory . $image);
                //update the truckimage wit the truck id
                $truckImage = TruckImage::where('filename', $image)->first();
                $truckImage->truck_id = $truck->id;
                $truckImage->order = $order;
                $truckImage->save();
            }
        }


        $year = date_create_from_format('m/Y', $data['year']);

        $truck->truckengine_id  = $data['engine'];
        $truck->reference = $this->createReference($data['chassis_number']);

        $truck->color   = $data['color'];
        $truck->year    = $year->format('Y-m-d');
        $truck->mileage = $data['mileage'];
        $truck->price   = $data['price'];
        $truck->purchase_price   = $data['purchase_price'];
        $truck->merchant_price   = $data['merchant_price'];
        $truck->property_consignment = $data['property_consignment'];
        $truck->vat_margin   = $data['vat_margin'];
        $truck->damage   = $data['damage'];
        $truck->chassis_number   = $data['chassis_number'];
        $truck->euronorm = $data['euronorm'];
        $truck->remarks = $data['remarks'];
        $truck->default_text_2 = $data['default_text_2'];
        if (isset($data['used'])) {
          $truck->used = $data['used'];
        }
        else {
          $truck->used = false;
        }
        $truck->horsepower = $data['horsepower'];
        $truck->truckbrand_id = $data['brand'];

        foreach($data as $field => $value){
          if (strpos($field, 'construction') !== false || strpos($field, 'equipment') !== false || strpos($field, 'weights') !== false
          || strpos($field, 'delivery_terms') !== false) {
            if (!starts_with($field, 'filter')) {
              $truck->$field = $value;
            }
          }
        }

        $truck->internal_remarks = $data['internal_remarks'];



        $truck->save();


        $event = new \stdClass();
        $event->truck = $truck;
        $event->user = Auth::user();
        event(new TruckCreated($event));
      }

    });

    return $truck->id;
  }


    /**
     * @param array $input
     */
    public function update ($input){
      $truck = $input['truck'];
      $data = $input['data'];

      DB::transaction(function () use ($truck, $data) {

          //clear all boolean values because laravel doesn't send checkboxes with value 0

          //first clear all values


          $table_info_columns = DB::select('describe trucks');

          foreach($table_info_columns as $column){
              if ($column->Type == "tinyint(1)"){
                  $field = $column->Field;
                  $truck->$field = false;
              }
          }



        //remove the deleted images
        foreach (explode(',', $data['deleted_images']) as $deletedImage) {
          $truckImage = TruckImage::where('filename', $deletedImage)->first();
          if (!is_null($truckImage)) {
            $truckImage->delete();
          }
        }
        $order = 0;
        foreach (explode(',', $data['image_order']) as $image){
            if ($image != "") {
                $order++;
                $tempImageDirectory = public_path() . "/images/trucks/temp/";
                $imageDirectory = public_path() . "/images/trucks/" . $truck->id . "/";
                if (!is_dir($imageDirectory)) {
                    mkdir($imageDirectory, 0777, true);
                }
                if (!file_exists($imageDirectory . $image)) {
                    rename($tempImageDirectory . $image,
                      $imageDirectory . $image);
                }
                if (!is_dir($imageDirectory)) {
                    mkdir($imageDirectory, 0777, true);
                }
                //update the truckimage with the truck id
                $truckImage = TruckImage::where('filename', $image)->first();
                $truckImage->truck_id = $truck->id;
                $truckImage->order = $order;
                $truckImage->save();
            }
        }


        $year = date_create_from_format('m/Y', $data['year']);

        $truck->truckbrand_id = $data['brand'];
        $truck->name = $data['name'];
        $truck->seats = $data['seats'];
        $truck->truckengine_id  = $data['engine'];
        $truck->reference = $this->createReference($data['chassis_number']);
        $truck->color   = $data['color'];
        $truck->year    = $year->format('Y-m-d');
        $truck->mileage = $data['mileage'];
        $truck->price   = $data['price'];
        $truck->purchase_price   = $data['purchase_price'];
        $truck->merchant_price   = $data['merchant_price'];
        $truck->property_consignment = $data['property_consignment'];
        $truck->vat_margin   = $data['vat_margin'];
        $truck->damage   = $data['damage'];
        $truck->chassis_number   = $data['chassis_number'];
        $truck->euronorm = $data['euronorm'];
        $truck->remarks = $data['remarks'];
        $truck->default_text_2 = $data['default_text_2'];
        if (isset($data['sold'])) {
            $truck->sold = $data['sold'];
        }
        else {
            $truck->sold = false;
        }
        if (isset($data['used'])) {
          $truck->used = $data['used'];
        }
        else {
          $truck->used = false;
        }
        $truck->horsepower = $data['horsepower'];
        $truck->truckbrand_id = $data['brand'];

          $truck->internal_remarks = $data['internal_remarks'];




        foreach($data as $field => $value){
          if (strpos($field, 'construction') !== false || strpos($field, 'equipment') !== false || strpos($field, 'weights') !== false
            || strpos($field, 'delivery_terms') !== false) {
            if (!starts_with($field, 'filter')) {
              $truck->$field = $value;
            }
          }
        }

        $truck->save();


        $event = new \stdClass();
        $event->truck = $truck;
        $event->user = Auth::user();
        event(new TruckUpdated($event));

        return true;
      });

    }


  /**
   * @param  $input
   *
   * @return mixed
   */
  protected function createTruckStub($input)
  {
    $truck = self::MODEL;
    $truck = new $truck;
    $truck->truckbrand_id = $input['brand'];
    $truck->name = $input['name'];
    $truck->seats = $input['seats'];
    $truck->truckengine_id  = $input['engine'];


    return $truck;
  }


  protected function createReference($chassis_number){
      if (strlen($chassis_number) == 4){
          return $chassis_number;
      }
      for ($i =  4; $i <= strlen($chassis_number); $i++){
          $reference = substr($chassis_number, -1 * $i);
          //check if the reference number is already in use
          $result = Truck::where('reference', '=', $reference)->first();
          if ($result === null){
              return $reference;
          }
      }
      return false;
  }

    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = array('*')) {
        $truck = self::MODEL;
        return $truck::where($attribute, '=', $value)->first($columns);
    }

    public function delete($id){
        $truckImages = TruckImage::where('truck_id', $id)->get();
        foreach ($truckImages as $truckImage){
            $truckImage->delete();
        }
        $truck = Truck::find($id);
        $truck->delete();
    }


    public function createAd($id, $sellerId)
    {
        $truck = Truck::find($id);

        $price = new Price;
        if ($truck->vat_margin == "vat") {
            $price->setVatRate("21.00");
            $price->setConsumerPriceGross(number_format($truck->price, 2, ".",
              ""));
            $price->setDealerPriceGross(number_format($truck->merchant_price, 2,
              ".", ""));
            $price->setConsumerPriceNet(number_format(round(($truck->price * 1.21),
              0), 2, ".", ""));
            $price->setDealerPriceNet(number_format(round(($truck->merchant_price * 1.21),
              0), 2, ".", ""));
        } else {
            $price->setVatRate("0.0");
            $price->setConsumerPriceGross($truck->price);
            $price->setDealerPriceGross($truck->merchant_price);
            $price->setConsumerPriceNet($truck->price);
            $price->setDealerPriceGross($truck->merchant_price);
        }
        if ($truck->weights_mtm < 7490) {
            $ad = new VanUpTo7500($sellerId);
        } else {
            $ad = new TruckOver7500($sellerId);
        }

        $ad->setPrice($price);
        $ad->setCategory($truck->construction_type);
        $ad->setMake(strtoupper($truck->brand->name));
        $ad->setFirstRegistration(DateTime::createFromFormat('Y-m-d',
          $truck->year)->format('Ym'));
        $ad->setMileage($truck->mileage);
        $ad->setEmissionClass($truck->euronorm);
        $ad->setModelDescription($truck->name);
        if ($truck->damage == "no_damage") {
            $ad->setDamageUnrepaired(false);
        } else {
            $ad->setDamageUnrepaired(true);
        }
        $ad->setAbs($truck->equipment_abs);
        $ad->setAirco($truck->equipment_airco);
        $ad->setAutomaticAirco($truck->equipment_automatic_airco);
        $ad->setCentralLocking($truck->equipment_central_locking);
        $ad->setCrane($truck->equipment_crane);
        $ad->setCruiseControl($truck->chassis_cruise_control);
        $ad->setDriversSleepingCompartment($truck->equipment_sleeping_cabin);
        $ad->setElectricExteriorMirrors($truck->equipment_heated_mirrors);
        $ad->setElectricHeatedSeats($truck->equipment_heated_seats);
        $ad->setElectricWindows($truck->equipment_electric_windows);
        $ad->setHandsFreePhoneSystem($truck->equipment_phone);
        $ad->setLicensedWeight($truck->weights_mtm);
        $ad->setLiftingCapacity($truck->constructionLiftPower->name);
        $ad->setLoadCapacity($truck->weights_useful_load_capacity);
        $ad->setInternalNumber($truck->reference);
        $ad->setExteriorColor($truck->color);
        $ad->setMultifunctionalWheel($truck->equipment_multifunctional_wheel);
        $ad->setNavigationSystem($truck->equipment_gps);
        $ad->setNonSmokerVehicle($truck->equipment_non_smoker);
        $ad->setPowerAssistedSteering($truck->equipment_power_steering);
        $ad->setPower($truck->horsepower);
        $ad->setRetarder($truck->equipment_retarder);
        $ad->setSeats($truck->seats);
        $ad->setTailLift($truck->construction_lift);
        $ad->setTv($truck->equipment_tv);
        $ad->setXenonHeadlights($truck->equipment_xenon_headlights);
        return $ad;
    }


    public function getImagesForExport($id){
        $truck = Truck::find($id);
        $images = $truck->images()->get();
        return $images;
    }



    public function saveAdId($id, $adId){
        $truck = Truck::find($id);
        $truck->mobile_ad_id = $adId;
        $truck->save();
    }

    public function removeAdId($id){
        $truck = Truck::find($id);
        $truck->mobile_ad_id = null;
        $truck->save();
    }


    public function getTrucksForExport(){
        $trucks = Truck::where('sold', '=', 0)->get();
        return $trucks;
    }

    public function getAdId($id){
        $truck = Truck::find($id);
        return $truck->mobile_ad_id;
    }

}
