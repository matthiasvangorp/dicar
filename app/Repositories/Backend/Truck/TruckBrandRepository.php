<?php

namespace App\Repositories\Backend\Truck;


use App\Models\Truck\TruckBrand;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Models\Access\User\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Events\Backend\Truck\TruckCreated;

/**
 * Class TruckRepository.
 */
class TruckBrandRepository implements TruckContract
{
    /**
     * Associated Repository Model.
     */
  const MODEL = TruckBrand::class;

  private $paginationType = 'simplePaginate';


  /**
   * @param string $order_by
   * @param string $sort
   *
   * @return mixed
   */
  /**
   * @param null $limit
   * @param bool $paginate
   * @param int  $pagination
   *
   * @return string|\Symfony\Component\Translation\TranslatorInterface
   */
  public function render($limit = null, $paginate = true, $pagination = 10)
  {
    $brands = TruckBrand::orderBy('name')->pluck('name', 'id');
    return $brands;
  }


  public function renderOne($id){
    $brand = TruckBrand::find($id);
    return $brand;
  }

  /**
   * @param $history
   * @param bool $paginate
   *
   * @return string
   */
  public function buildList($history, $paginate = true)
  {
    return view('backend.history.partials.list', ['history' => $history, 'paginate' => $paginate])
      ->render();
  }

  /**
   * @param $query
   * @param $limit
   * @param $paginate
   * @param $pagination
   *
   * @return mixed
   */
  public function buildPagination($query, $limit, $paginate, $pagination)
  {
    if ($paginate && is_numeric($pagination)) {
      return $query->{$this->paginationType}($pagination);
    } else {
      if ($limit && is_numeric($limit)) {
        $query->take($limit);
      }

      return $query->get();
    }
  }


  /**
   * @param array $input
   */
  public function create($input)
  {
    $data = $input['data'];

    $brand = $this->createBrandStub($data);

    DB::transaction(function () use ($brand) {
      if ($brand->save()) {
        $brand->save();

      } else {
        throw new GeneralException(trans('exceptions.backend.access.users.create_error'));
      }
    });

    return $brand;

  }


    /**
     * @param array $input
     */
    public function update ($input){

    }


  /**
   * @param  $input
   *
   * @return mixed
   */
  protected function createBrandStub($input)
  {
    $brand = self::MODEL;
    $brand = new $brand;
    $brand->name = $input['name'];

    return $brand;
  }

    public function find($input){
      $result = TruckBrand::where('name',$input['name'])->first();
      return $result;
    }

}
