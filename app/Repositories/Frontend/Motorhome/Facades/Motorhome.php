<?php

namespace App\Repositories\Frontend\Motorhome\Facades;

use Illuminate\Support\Facades\Facade;

class Motorhome extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'motorhome';
    }
}
