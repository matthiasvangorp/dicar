<?php

namespace App\Repositories\Frontend\Motorhome;

/**
 * Interface MotorhomeContract.
 */
interface MotorhomeContract
{

    public function render($limit = null, $paginate = true, $pagination = 10);

    /**
     * @param $type
     * @param null $limit
     * @param bool $paginate
     * @param int  $pagination
     *
     * @return mixed
     */

    public function buildList($history, $paginate = true);

    /**
     * @param $query
     * @param $limit
     * @param $paginate
     * @param $pagination
     *
     * @return mixed
     */
    public function buildPagination($query, $limit, $paginate, $pagination);
}
