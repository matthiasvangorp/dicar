<?php

namespace App\Repositories\Frontend\Motorhome;

use App\Models\Motorhome\Motorhome;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Models\Access\User\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Events\Backend\Motorhome\MotorhomeCreated;

/**
 * Class MotorhomeRepository.
 */
class MotorhomeRepository implements MotorhomeContract
{
    /**
     * Associated Repository Model.
     */
  const MODEL = Motorhome::class;

  private $paginationType = 'simplePaginate';


  /**
   * @param string $order_by
   * @param string $sort
   *
   * @return mixed
   */
  /**
   * @param null $limit
   * @param bool $paginate
   * @param int  $pagination
   *
   * @return string|\Symfony\Component\Translation\TranslatorInterface
   */
  public function render($limit = null, $paginate = true, $pagination = 10)
  {
    $motorhomes = Motorhome::get();
    //$motorhomes = $this->buildPagination($motorhomes, $limit, $paginate, $pagination);
    foreach ($motorhomes as &$motorhome){
      $test = $motorhome;
      $imagesString = $motorhome->images;
      $imagesArray = explode(';', $imagesString);
      $motorhome->images = $imagesArray;
    }

    return $motorhomes;
  }


  public function renderOne($id){
    $motorhome = Motorhome::find($id);
    return $motorhome;
  }

  /**
   * @param $history
   * @param bool $paginate
   *
   * @return string
   */
  public function buildList($history, $paginate = true)
  {
    return view('backend.history.partials.list', ['history' => $history, 'paginate' => $paginate])
      ->render();
  }

  /**
   * @param $query
   * @param $limit
   * @param $paginate
   * @param $pagination
   *
   * @return mixed
   */
  public function buildPagination($query, $limit, $paginate, $pagination)
  {
    if ($paginate && is_numeric($pagination)) {
      return $query->{$this->paginationType}($pagination);
    } else {
      if ($limit && is_numeric($limit)) {
        $query->take($limit);
      }

      return $query->get();
    }
  }


  /**
   * @param array $input
   */
  public function create($input)
  {
    $data = $input['data'];
    $images = $input['images'];

    $motorhome = $this->createMotorhomeStub($data);

    DB::transaction(function () use ($motorhome, $data, $images) {
      if ($motorhome->save()) {
        //motorhome saved, now save the images
        $imageString = "";
        foreach ($images as $image){
          if ($image->isValid()){

            $imageDirectory = public_path() . "/images/motorhomes/".$motorhome->id."/";
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renameing image
            $image->move($imageDirectory, $fileName);
            $imageString .= $fileName.";";
          }
        }

        $imageString = substr($imageString, 0, -1); //remove the last ;

        $motorhome->images = $imageString;
        $motorhome->save();


        $event = new \stdClass();
        $event->motorhome = $motorhome;
        $event->user = Auth::user();
        event(new MotorhomeCreated($event));

        return true;
      }

      throw new GeneralException(trans('exceptions.backend.access.users.create_error'));
    });
  }


  /**
   * @param  $input
   *
   * @return mixed
   */
  protected function createMotorhomeStub($input)
  {
    $motorhome = self::MODEL;
    $motorhome = new $motorhome;
    $motorhome->name = $input['name'];
    $motorhome->sleepingPlaces = $input['sleepingPlaces'];
    $motorhome->seats = $input['seats'];

    return $motorhome;
  }

}
