<?php

namespace App\Repositories\Frontend\Truck;

use App\Events\Backend\Truck\TruckUpdated;
use App\Models\Truck\Truck;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Models\Access\User\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Events\Backend\Truck\TruckCreated;

/**
 * Class TruckRepository.
 */
class TruckRepository implements TruckContract
{
    /**
     * Associated Repository Model.
     */
  const MODEL = Truck::class;

  private $paginationType = 'simplePaginate';


  /**
   * @param string $order_by
   * @param string $sort
   *
   * @return mixed
   */
  /**
   * @param null $limit
   * @param bool $paginate
   * @param int  $pagination
   *
   * @return string|\Symfony\Component\Translation\TranslatorInterface
   */
  public function render($limit = null, $paginate = true, $pagination = 10)
  {
      $trucks = Truck::join('truckBrands as tb', 'trucks.truckbrand_id', '=', 'tb.id')->where('trucks.sold', 0)
          ->orderBy('tb.name', 'asc')
          ->select('trucks.*')->get();    // just to avoid fetching anything from joined table
    return $trucks;
  }

    public function getManTrucks()
    {
        $trucks = Truck::join('truckBrands as tb', 'trucks.truckbrand_id', '=', 'tb.id')
            ->where('trucks.sold', 0)
            ->where('tb.id', 2)
            ->orderBy('tb.name', 'asc')
            ->select('trucks.*')->get();    // just to avoid fetching anything from joined table
        return $trucks;
    }

    public function renderSoldTrucks($limit = null, $paginate = true, $pagination = 10)
    {
        $trucks = Truck::join('truckBrands as tb', 'trucks.truckbrand_id', '=', 'tb.id')->where('trucks.sold', 1)
            ->orderBy('tb.name', 'asc')
            ->select('trucks.*')->get();
        //$trucks = $this->buildPagination($trucks, $limit, $paginate, $pagination);
        return $trucks;
    }


  public function filter($request){
    $ascOrDesc = 'desc';
    if ($request->sortBy == 'price'){
        $ascOrDesc = 'asc';
    }
    if ($request->sortBy == 'alphabetic'){
        $trucks = Truck::join('truckBrands as tb', 'trucks.truckbrand_id', '=', 'tb.id')->where('trucks.sold', 0)
            ->orderBy('tb.name', 'asc')
            ->select('trucks.*')->get();
        return $trucks;
    }
    if ($request->brand > 0 && $request->euronorm > 0) {
      $trucks = Truck::orderBy($request->sortBy, $ascOrDesc)
        ->where('truckbrand_id', $request->brand)
        ->where('euronorm', '>=', $request->euronorm)
        ->get();
    }
    elseif ($request->brand > 0){
      $trucks = Truck::orderBy($request->sortBy, $ascOrDesc)
        ->where('truckbrand_id', $request->brand)
        ->get();
    }
    elseif ($request->euronorm > 0){
      $trucks = Truck::orderBy($request->sortBy, $ascOrDesc)
        ->where('euronorm', '>=', $request->euronorm)
        ->get();
    }
    else {
      $trucks = Truck::orderBy($request->sortBy, $ascOrDesc)->get();
    }
    return $trucks;
  }


  public function renderOne($id){
    $truck = Truck::find($id);
    $year = date_create_from_format('Y-m-d', $truck->year);
    $truck->year = $year->format('d/m/Y');
    return $truck;
  }

  /**
   * @param $history
   * @param bool $paginate
   *
   * @return string
   */
  public function buildList($history, $paginate = true)
  {
    return view('backend.history.partials.list', ['history' => $history, 'paginate' => $paginate])
      ->render();
  }

  /**
   * @param $query
   * @param $limit
   * @param $paginate
   * @param $pagination
   *
   * @return mixed
   */
  public function buildPagination($query, $limit, $paginate, $pagination)
  {
    if ($paginate && is_numeric($pagination)) {
      return $query->{$this->paginationType}($pagination);
    } else {
      if ($limit && is_numeric($limit)) {
        $query->take($limit);
      }

      return $query->get();
    }
  }


  /**
   * @param array $input
   */
  public function create($input)
  {
    $data = $input['data'];
    $images = $input['images'];

    $truck = $this->createTruckStub($data);

    DB::transaction(function () use ($truck, $data, $images) {
      if ($truck->save()) {
        //truck saved, now save the images
        $imageString = "";
        foreach ($images as $image){
          if ($image->isValid()){

            $imageDirectory = public_path() . "/images/trucks/".$truck->id."/";
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renameing image
            $image->move($imageDirectory, $fileName);
            $imageString .= $fileName.";";
          }
        }

        $imageString = substr($imageString, 0, -1); //remove the last ;

        $year = date_create_from_format('d/m/Y', $data['year']);

        $truck->images  = $imageString;
        $truck->engine  = $data['engine'];
        $truck->color   = $data['color'];
        $truck->year    = $year->format('Y-m-d');
        $truck->mileage = $data['mileage'];
        $truck->construction = $data['construction'];
        $truck->dimensions = $data['dimensions'];
        $truck->tailgate = $data['tailgate'];
        $truck->mtm = $data['mtm'];
        $truck->empty_weight = $data['empty_weight'];
        $truck->horsepower = $data['horsepower'];
        $truck->transmission = $data['transmission'];
        $truck->drivetrain = $data['drivetrain'];
        $truck->used = $data['used'];

        $truck->save();


        $event = new \stdClass();
        $event->truck = $truck;
        $event->user = Auth::user();
        event(new TruckCreated($event));

        return true;
      }

      throw new GeneralException(trans('exceptions.backend.access.users.create_error'));
    });
  }


    /**
     * @param array $input
     */
    public function update ($input){
      $truck = $input['truck'];
      $data = $input['data'];
      $images = $input['images'];

      DB::transaction(function () use ($truck, $data, $images) {
          $imageString = "";
          if (!empty($images)) {
            foreach ($images as $image) {
              if ($image->isValid()) {

                $imageDirectory = public_path() . "/images/trucks/" . $truck->id . "/";
                $extension = $image->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,
                    99999) . '.' . $extension; // renameing image
                $image->move($imageDirectory, $fileName);
                $imageString .= $fileName . ";";
              }
            }
          }

          $imageString = substr($imageString, 0, -1); //remove the last ;

          $year = date_create_from_format('d/m/Y', $data['year']);

          if (!empty($images)) {
            $truck->images = $imageString;
          }

          $truck->truckbrand_id = $data['brand'];
          $truck->engine  = $data['engine'];
          $truck->color   = $data['color'];
          $truck->year    = $year->format('Y-m-d');
          $truck->mileage = $data['mileage'];
          $truck->construction = $data['construction'];
          $truck->dimensions = $data['dimensions'];
          $truck->tailgate = $data['tailgate'];
          $truck->mtm = $data['mtm'];
          $truck->empty_weight = $data['empty_weight'];
          $truck->horsepower = $data['horsepower'];
          $truck->transmission = $data['transmission'];
          $truck->drivetrain = $data['drivetrain'];
          $truck->used = $data['used'];

          $truck->save();


          $event = new \stdClass();
          $event->truck = $truck;
          $event->user = Auth::user();
          event(new TruckUpdated($event));

          return true;
      });

    }


  /**
   * @param  $input
   *
   * @return mixed
   */
  protected function createTruckStub($input)
  {
    $truck = self::MODEL;
    $truck = new $truck;
    $truck->truckbrand_id = $input['brand'];
    $truck->name = $input['name'];
    $truck->seats = $input['seats'];

    return $truck;
  }


  public function returnSort(){
      return [
        'alphabetic' => trans('trucks.fields.alphabetic'),
        'updated_at' => trans('trucks.fields.updated_at'),
        'price' => trans('trucks.fields.price'),
        'year' => trans('trucks.fields.year'),
        'euronorm' => trans('trucks.fields.euronorm'),
      ];
  }

}
