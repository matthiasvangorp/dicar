<?php

namespace App\Models\Mobile;

use Jenssegers\Model\Model;
use App\Models\Mobile\Price;

class Advertisement extends Model
{
    public function __construct($mobileSellerId)
    {
        $this->mobileSellerId = $mobileSellerId;
    }

    /*
    * mobile-platform Ad ID
    * @var string
    */
    protected $mobileAdId = '';


    /*
    * mobile-platform Seller ID
    * @var string
    */
    protected $mobileSellerId = '';

    /*
    * e.g. 2015-06-12T11:58:32+02:00. See Date Time Representation for details.
    * @var string
    */
    protected $creationDate = '';


    /*
    * e.g. 2015-06-12T11:58:32+02:00. See Date Time Representation for details.
    * @var string
    */
    protected $modificationDate = '';


    /*
    * e.g. 2015-06-12T11:58:32+02:00. See Date Time Representation for details.
    * @var string
    */
    protected $renewalDate = '';


    /*
    * URL of the ad’s detail page on mobile.de
    * @var string
    */
    protected $detailPageUrl = '';

    /*
    * The GENERAL classification of vehicles e.g. Car or Motorbike. See https://services.mobile.de/docs/seller-api.html#class_refdata for possible values.
    * @var string
    */
    protected $vehicleClass = '';


    /*
    * The DETAILED classification of vehicles e.g. Cabrio or Limousine. See https://services.mobile.de/docs/seller-api.html#category_refdata for possible values.
    * @var string
    */
    protected $category = '';

    /*
    * Make of the vehicle e.g. Mercedes, Toyota. See https://services.mobile.de/docs/seller-api.html#make_refdata for possible values.
    * @var string
    */
    protected $make = '';

    /*
    * Model of the vehicle e.g. X5, Prius. See https://services.mobile.de/docs/seller-api.html#model_refdata for possible values.
    * @var string
    */
    protected $model = '';

    /*
    * Detailed description of the model. Also used as ad title. Example Golf III always parked in garage. This field is restricted to a subset of UTF-8 characters.
    * @var string
    */
    protected $modelDescription = '';

    /*
    * Is the vehicle new or used? See https://services.mobile.de/docs/seller-api.html#condition_refdata for possible values.
    * @var string
    */
    protected $condition = '';


    /*
    * Type of used car. See https://services.mobile.de/docs/seller-api.html#usageType_refdata for possible values.
    * @var string
    */
    protected $usageType = '';

    /*
    * yyyyMM. See Date Time Representation for details.
    * @var string
    */
    protected $firstRegistration = '';

    /*
    * mileage in km
    * @var Int
    */
    protected $mileage = 0;

    /*
    * The vehicle identification number (aka VIN or FIN). See http://en.wikipedia.org/wiki/Vehicle_Identification_Number
    * @var string
    */
    protected $vin = '';


    /*
    * A reference key used by the seller. Also known as internal-number or internal-ID. If it is present, then it must be unique per seller. An empty string is treated same as if the element was not present. This field is restricted to a subset of UTF-8 characters.
    * @var string
    */
    protected $internalNumber = '';

    /*
    * Cubic-capacity of the engine. Notation integer.
    * @var Int
    */
    protected $cubicCapacity = 0;


    /*
    * Cubic-capacity of the engine. Notation integer.
    * @var Int
    */
    protected $power = 0;


    /*
    * Stickshift, automatic? See https://services.mobile.de/docs/seller-api.html#gearbox_refdata for possible values.
    * @var string
    */
    protected $gearbox = '';

    /*
    * Type of fuel. See https://services.mobile.de/docs/seller-api.html#fuel_refdata for possible values.
    * @var string
    */
    protected $fuel = '';


    /*
    * Number of axles, integer. Information only for commercial vehicles
    * @var Int
    */
    protected $axles = 0;

    /*
    * Four wheel drive
    * @var Boolean
    */
    protected $fourWheelDrive = false;

    /*
    * The actuation of the vehicle. See https://services.mobile.de/docs/seller-api.html#wheelFormula_refdata for possible values.
    * @var String
    */
    protected $wheelFormula = '';

    /*
    * A plug-in hybrid electric vehicle (PHEV) is a hybrid electric vehicle that uses rechargeable batteries, or another energy storage device, that can be recharged by plugging it in to an external source of electric power.
    * @var Boolean
    */
    protected $hybridPlugin = false;

    /*
    * EURO 1, 2, 3, 4, …​. See https://services.mobile.de/docs/seller-api.html#emissionClass_refdata for possible values.
    * @var String
    */
    protected $emissionClass = '';


    /*
    * The mobile.de color name. This is a fixed set of colors. https://services.mobile.de/docs/seller-api.html#color_refdata for possible values
    * @var String
    */
    protected $exteriorColor = '';

    /*
    * Metallic
    * @var Boolean
    */
    protected $metallic = false;

    /*
    * number of seats. For categories Car, Bus, Truck up to 7,5t.
    * @var Int
    */
    protected $seats = 0;

    /*
    * Nonsmoker vehicle
    * @var Boolean
    */
    protected $nonSmokerVehicle = false;

    /*
    * See http://services.mobile.de/manual/damage.html
    * @var Boolean
    */
    protected $damageUnrepaired = false;

    /*
    * See http://services.mobile.de/manual/damage.html
    * @var Boolean
    */
    protected $accidentDamaged = false;

    /*
    * Abs
    * @var Boolean
    */
    protected $abs = false;

    /*
    * Information about the airbags of the vehicle. See https://services.mobile.de/docs/seller-api.html#airbag_refdata for possible values.
    * @var String
    */
    protected $airbag = false;

    /*
    * Airo
    * @var Boolean
    */
    protected $airco = false;

    /*
    * Airo
    * @var Boolean
    */
    protected $automaticAirco = false;

    /*
    * Air suspension
    * @var Boolean
    */
    protected $airSuspension = false;

    /*
    * Rain sensor
    * @var Boolean
    */
    protected $automaticRainSensor = false;


    /*
    * CD Player
    * @var Boolean
    */
    protected $cdPlayer = false;


    /*
    * Central locking
    * @var Boolean
    */
    protected $centralLocking = false;

    /*
    * Information on the climatisation of the vehicle. See https://services.mobile.de/docs/seller-api.html#climatisation_refdata for possible values.
    * @var Boolean
    */
    protected $climatisation = false;

    /*
    * Crane
    * @var String
    */
    protected $crane = false;

    /*
    * Information on the climatisation of the vehicle. See https://services.mobile.de/docs/seller-api.html#climatisation_refdata for possible values.
    * @var Boolean
    */
    protected $cruiseControl = false;

    /*
    * The external dimension of the vehicle.
    * @var Dimension
    */
    protected $dimension;

    /*
    * Drivers sleeping compartment.
    * @var Boolean
    */
    protected $driversSleepingCompartment = false;

    /*
    * Electric mirrors
    * @var Boolean
    */
    protected $electricExteriorMirrors = false;

    /*
    * Electric heated seats
    * @var Boolean
    */
    protected $electricHeatedSeats = false;

    /*
    * Electric windows
    * @var Boolean
    */
    protected $electricWindows = false;


    /*
    * Electric windows
    * @var Boolean
    */
    protected $handsFreePhoneSystem = false;



    /*
    * Notation in kg, integer. Information only for commercial vehicles.
    * @var Int
    */
    protected $licensedWeight;

    /*
    * Notation in kg, integer. Information only for commercial vehicles.
    * @var Int
    */
    protected $liftingCapacity;

    /*
    * Notation in kg, integer. Information only for commercial vehicles.
    * @var Int
    */
    protected $loadCapacity;

    /*
    * Electric windows
    * @var Boolean
    */
    protected $multifunctionalWheel = false;


    /*
    * Is the car having a build in navigation system. Not be be set for portable navigation systems
    * @var Boolean
    */
    protected $navigationSystem = false;

    /*
    * Information about the parking assistants of the vehicle. See https://services.mobile.de/docs/seller-api.html#parkingAssistant_refdata for possible values.
    * @var array
    */
    protected $parkingAssistants;

    /*
    * Power assisted steering
    * @var Boolean
    */
    protected $powerAssistedSteering = false;

    /*
    * Used if the vehicle has an retader or intarder
    * @var Boolean
    */
    protected $tailLift = false;

    /*
    * Used if the vehicle has an retader or intarder
    * @var Boolean
    */
    protected $windshield = false;

    /*
    * Tv
    * @var Boolean
    */
    protected $tv = false;

    /*
    * Used if the vehicle has an retader or intarder
    * @var Boolean
    */
    protected $xenonHeadlights = false;

    /*
    * Free text description of the vehicle
    * @var String
    */
    protected $description;


    /*
    * List of images
    * @var array
    */
    protected $images = [];


    /*
    * Price information. Check details of the Price type for more information.
    * @var Price
    */
    protected $price;

    /*
    * Price information. Check details of the Price type for more information.
    * @var Seller
    */
    protected $seller;

    /**
     * @return string
     */
    public function getMobileAdId()
    {
        return $this->mobileAdId;
    }

    /**
     * @param string $mobileAdId
     */
    public function setMobileAdId( $mobileAdId )
    {
        $this->mobileAdId = $mobileAdId;
    }

    /**
     * @return string
     */
    public function getMobileSellerId()
    {
        return $this->mobileSellerId;
    }

    /**
     * @param string $mobileSellerId
     */
    public function setMobileSellerId( $mobileSellerId )
    {
        $this->mobileSellerId = $mobileSellerId;
    }

    /**
     * @return string
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param string $creationDate
     */
    public function setCreationDate( $creationDate )
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return string
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * @param string $modificationDate
     */
    public function setModificationDate( $modificationDate )
    {
        $this->modificationDate = $modificationDate;
    }

    /**
     * @return string
     */
    public function getRenewalDate()
    {
        return $this->renewalDate;
    }

    /**
     * @param string $renewalDate
     */
    public function setRenewalDate( $renewalDate )
    {
        $this->renewalDate = $renewalDate;
    }

    /**
     * @return string
     */
    public function getDetailPageUrl()
    {
        return $this->detailPageUrl;
    }

    /**
     * @param string $detailPageUrl
     */
    public function setDetailPageUrl( $detailPageUrl )
    {
        $this->detailPageUrl = $detailPageUrl;
    }

    /**
     * @return string
     */
    public function getVehicleClass()
    {
        return $this->vehicleClass;
    }

    /**
     * @param string $vehicleClass
     */
    public function setVehicleClass( $vehicleClass )
    {
        $this->vehicleClass = $vehicleClass;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory( $category )
    {
        switch ($category){
            case "trunck":
                $this->category = "BoxVan";    //'trunck', 'open_trailer', 'van', 'van_high_long', 'chassis_cab', 'sail', 'trunck_lift', 'trunck_doors'
                break;
            case  "open_trailer" :
                $this->category = "StakeBodyVan";    //'trunck', 'open_trailer', 'van', 'van_high_long', 'chassis_cab', 'sail', 'trunck_lift', 'trunck_doors'
                break;
            case  "van" :
                $this->category = "BoxTypeDeliveryVan";    //'trunck', 'open_trailer', 'van', 'van_high_long', 'chassis_cab', 'sail', 'trunck_lift', 'trunck_doors'
                break;
            case  "van_high_long" :
                $this->category = "HighAndLongBoxTypeDeliveryVan";    //'trunck', 'open_trailer', 'van', 'van_high_long', 'chassis_cab', 'sail', 'trunck_lift', 'trunck_doors'
                break;
            case  "chassis_cab" :
                $this->category = "ChassisVan";    //'trunck', 'open_trailer', 'van', 'van_high_long', 'chassis_cab', 'sail', 'trunck_lift', 'trunck_doors'
                break;
            case  "sail" :
                $this->category = "StakeBodyWithTarpaulinVan";    //'trunck', 'open_trailer', 'van', 'van_high_long', 'chassis_cab', 'sail', 'trunck_lift', 'trunck_doors'
                break;
            case  "trunck_lift" :
                $this->category = "BoxVan";    //'trunck', 'open_trailer', 'van', 'van_high_long', 'chassis_cab', 'sail', 'trunck_lift', 'trunck_doors'
                break;
            case  "trunck_doors" :
                $this->category = "BoxVan";    //'trunck', 'open_trailer', 'van', 'van_high_long', 'chassis_cab', 'sail', 'trunck_lift', 'trunck_doors'
                break;
        }
    }

    /**
     * @return string
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * @param string $make
     */
    public function setMake( $make )
    {
        switch (strtoupper($make)){
            case "MERCEDES":
                $this->make = "MERCEDES-BENZ";    //'trunck', 'open_trailer', 'van', 'van_high_long', 'chassis_cab', 'sail', 'trunck_lift', 'trunck_doors'
                break;
            case  "VOLKSWAGEN" :
                $this->make = "VW";    //'trunck', 'open_trailer', 'van', 'van_high_long', 'chassis_cab', 'sail', 'trunck_lift', 'trunck_doors'
                break;
            default:
                $this->make = strtoupper($make);
        }
    }

    /**
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param string $model
     */
    public function setModel( $model )
    {
        $this->model = $model;
    }

    /**
     * @return string
     */
    public function getModelDescription()
    {
        return $this->modelDescription;
    }

    /**
     * @param string $modelDescription
     */
    public function setModelDescription( $modelDescription )
    {
        $this->modelDescription = $modelDescription;
    }

    /**
     * @return string
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param string $condition
     */
    public function setCondition( $condition )
    {
        $this->condition = $condition;
    }

    /**
     * @return string
     */
    public function getUsageType()
    {
        return $this->usageType;
    }

    /**
     * @param string $usageType
     */
    public function setUsageType( $usageType )
    {
        $this->usageType = $usageType;
    }

    /**
     * @return string
     */
    public function getFirstRegistration()
    {
        return $this->firstRegistration;
    }

    /**
     * @param string $firstRegistration
     */
    public function setFirstRegistration( $firstRegistration )
    {
        $this->firstRegistration = $firstRegistration;
    }

    /**
     * @return int
     */
    public function getMileage()
    {
        return $this->mileage;
    }

    /**
     * @param int $mileage
     */
    public function setMileage( $mileage )
    {
        $this->mileage = $mileage;
    }

    /**
     * @return string
     */
    public function getVin()
    {
        return $this->vin;
    }

    /**
     * @param string $vin
     */
    public function setVin( $vin )
    {
        $this->vin = $vin;
    }

    /**
     * @return string
     */
    public function getInternalNumber()
    {
        return $this->internalNumber;
    }

    /**
     * @param string $internalNumber
     */
    public function setInternalNumber( $internalNumber )
    {
        $this->internalNumber = $internalNumber;
    }

    /**
     * @return mixed
     */
    public function getCubicCapacity()
    {
        return $this->cubicCapacity;
    }

    /**
     * @param mixed $cubicCapacity
     */
    public function setCubicCapacity( $cubicCapacity )
    {
        $this->cubicCapacity = $cubicCapacity;
    }

    /**
     * @return int
     */
    public function getPower()
    {
        return $this->power;
    }

    /**
     * @param int $power
     */
    public function setPower( $power )
    {
        $this->power = $power;
    }

    /**
     * @return string
     */
    public function getGearbox()
    {
        return $this->gearbox;
    }

    /**
     * @param string $gearbox
     */
    public function setGearbox( $gearbox )
    {
        $this->gearbox = $gearbox;
    }

    /**
     * @return string
     */
    public function getFuel()
    {
        return $this->fuel;
    }

    /**
     * @param string $fuel
     */
    public function setFuel( $fuel )
    {
        $this->fuel = $fuel;
    }

    /**
     * @return int
     */
    public function getAxles()
    {
        return $this->axles;
    }

    /**
     * @param int $axles
     */
    public function setAxles( $axles )
    {
        $this->axles = $axles;
    }

    /**
     * @return bool
     */
    public function isFourWheelDrive()
    {
        return boolval($this->fourWheelDrive);
    }

    /**
     * @param bool $fourWheelDrive
     */
    public function setFourWheelDrive( $fourWheelDrive )
    {
        $this->fourWheelDrive = $fourWheelDrive;
    }

    /**
     * @return string
     */
    public function getWheelFormula()
    {
        return $this->wheelFormula;
    }

    /**
     * @param string $wheelFormula
     */
    public function setWheelFormula( $wheelFormula )
    {
        $this->wheelFormula = $wheelFormula;
    }

    /**
     * @return bool
     */
    public function isHybridPlugin()
    {
        return boolval($this->hybridPlugin);
    }

    /**
     * @param bool $hybridPlugin
     */
    public function setHybridPlugin( $hybridPlugin )
    {
        $this->hybridPlugin = $hybridPlugin;
    }

    /**
     * @return string
     */
    public function getEmissionClass()
    {
        return "EURO".$this->emissionClass;
    }

    /**
     * @param string $emissionClass
     */
    public function setEmissionClass( $emissionClass )
    {
        $this->emissionClass = $emissionClass;
    }

    /**
     * @return string
     */
    public function getExteriorColor()
    {
        return $this->exteriorColor;
    }

    /**
     * @param string $exteriorColor
     */
    public function setExteriorColor( $exteriorColor )
    {
        $this->exteriorColor = strtoupper($exteriorColor);
    }

    /**
     * @return bool
     */
    public function isMetallic()
    {
        return boolval($this->metallic);
    }

    /**
     * @param bool $metallic
     */
    public function setMetallic( $metallic )
    {
        $this->metallic = $metallic;
    }

    /**
     * @return int
     */
    public function getSeats()
    {
        return $this->seats;
    }

    /**
     * @param int $seats
     */
    public function setSeats( $seats )
    {
        $this->seats = $seats;
    }

    /**
     * @return bool
     */
    public function isNonSmokerVehicle()
    {
        return boolval($this->nonSmokerVehicle);
    }

    /**
     * @param bool $nonSmokerVehicle
     */
    public function setNonSmokerVehicle( $nonSmokerVehicle )
    {
        $this->nonSmokerVehicle = $nonSmokerVehicle;
    }

    /**
     * @return bool
     */
    public function isDamageUnrepaired()
    {
        return boolval($this->damageUnrepaired);
    }

    /**
     * @param bool $damageUnrepaired
     */
    public function setDamageUnrepaired( $damageUnrepaired )
    {
        $this->damageUnrepaired = $damageUnrepaired;
    }

    /**
     * @return bool
     */
    public function isAccidentDamaged()
    {
        return boolval($this->accidentDamaged);
    }

    /**
     * @param bool $accidentDamaged
     */
    public function setAccidentDamaged( $accidentDamaged )
    {
        $this->accidentDamaged = $accidentDamaged;
    }

    /**
     * @return bool
     */
    public function isAbs()
    {
        return boolval($this->abs);
    }

    /**
     * @param bool $abs
     */
    public function setAbs( $abs )
    {
        $this->abs = $abs;
    }

    /**
     * @return bool
     */
    public function isAirbag()
    {
        return boolval($this->airbag);
    }

    /**
     * @param bool $airbag
     */
    public function setAirbag( $airbag )
    {
        $this->airbag = $airbag;
    }

    /**
     * @return bool
     */
    public function isAirSuspension()
    {
        return boolval($this->airSuspension);
    }

    /**
     * @param bool $airSuspension
     */
    public function setAirSuspension( $airSuspension )
    {
        $this->airSuspension = $airSuspension;
    }

    /**
     * @return bool
     */
    public function isAutomaticRainSensor()
    {
        return boolval($this->automaticRainSensor);
    }

    /**
     * @param bool $automaticRainSensor
     */
    public function setAutomaticRainSensor( $automaticRainSensor )
    {
        $this->automaticRainSensor = $automaticRainSensor;
    }

    /**
     * @return bool
     */
    public function isCdPlayer()
    {
        return boolval($this->cdPlayer);
    }

    /**
     * @param bool $cdPlayer
     */
    public function setCdPlayer( $cdPlayer )
    {
        $this->cdPlayer = $cdPlayer;
    }

    /**
     * @return bool
     */
    public function isCentralLocking()
    {
        return boolval($this->centralLocking);
    }

    /**
     * @param bool $centralLocking
     */
    public function setCentralLocking( $centralLocking )
    {
        $this->centralLocking = $centralLocking;
    }

    /**
     * @return bool
     */
    public function isClimatisation()
    {
        return boolval($this->climatisation);
    }

    /**
     * @param bool $climatisation
     */
    public function setClimatisation( $climatisation )
    {
        $this->climatisation = $climatisation;
    }

    /**
     * @return bool
     */
    public function isCruiseControl()
    {
        return boolval($this->cruiseControl);
    }

    /**
     * @param bool $cruiseControl
     */
    public function setCruiseControl( $cruiseControl )
    {
        $this->cruiseControl = $cruiseControl;
    }

    /**
     * @return mixed
     */
    public function getDimension()
    {
        return $this->dimension;
    }

    /**
     * @param mixed $dimension
     */
    public function setDimension( $dimension )
    {
        $this->dimension = $dimension;
    }

    /**
     * @return bool
     */
    public function isDriversSleepingCompartment(): bool
    {
        return boolval($this->driversSleepingCompartment);
    }

    /**
     * @param bool $driversSleepingCompartment
     */
    public function setDriversSleepingCompartment(
      bool $driversSleepingCompartment
    ) {
        $this->driversSleepingCompartment = $driversSleepingCompartment;
    }

    /**
     * @return bool
     */
    public function isElectricExteriorMirrors(): bool
    {
        return boolval($this->electricExteriorMirrors);
    }

    /**
     * @param bool $electricExteriorMirrors
     */
    public function setElectricExteriorMirrors( bool $electricExteriorMirrors )
    {
        $this->electricExteriorMirrors = $electricExteriorMirrors;
    }

    /**
     * @return bool
     */
    public function isElectricHeatedSeats(): bool
    {
        return boolval($this->electricHeatedSeats);
    }

    /**
     * @param bool $electricHeatedSeats
     */
    public function setElectricHeatedSeats( bool $electricHeatedSeats )
    {
        $this->electricHeatedSeats = $electricHeatedSeats;
    }

    /**
     * @return bool
     */
    public function isElectricWindows(): bool
    {
        return boolval($this->electricWindows);
    }

    /**
     * @param bool $electricWindows
     */
    public function setElectricWindows( bool $electricWindows )
    {
        $this->electricWindows = $electricWindows;
    }

    /**
     * @return bool
     */
    public function isHandsFreePhoneSystem(): bool
    {
        return boolval($this->handsFreePhoneSystem);
    }

    /**
     * @param bool $handsFreePhoneSystem
     */
    public function setHandsFreePhoneSystem( bool $handsFreePhoneSystem )
    {
        $this->handsFreePhoneSystem = $handsFreePhoneSystem;
    }





    /**
     * @return mixed
     */
    public function getLicensedWeight()
    {
        return $this->licensedWeight;
    }

    /**
     * @param mixed $licensedWeight
     */
    public function setLicensedWeight( $licensedWeight )
    {
        $this->licensedWeight = $licensedWeight;
    }

    /**
     * @return mixed
     */
    public function getLiftingCapacity()
    {
        return $this->liftingCapacity;
    }

    /**
     * @param mixed $liftingCapacity
     */
    public function setLiftingCapacity( $liftingCapacity )
    {
        $this->liftingCapacity = $liftingCapacity;
    }

    /**
     * @return mixed
     */
    public function getLoadCapacity()
    {
        return $this->loadCapacity;
    }

    /**
     * @param mixed $loadCapacity
     */
    public function setLoadCapacity( $loadCapacity )
    {
        $this->loadCapacity = $loadCapacity;
    }

    /**
     * @return bool
     */
    public function isMultifunctionalWheel(): bool
    {
        return boolval($this->multifunctionalWheel);
    }

    /**
     * @param bool $multifunctionalWheel
     */
    public function setMultifunctionalWheel( bool $multifunctionalWheel )
    {
        $this->multifunctionalWheel = $multifunctionalWheel;
    }

    /**
     * @return bool
     */
    public function isNavigationSystem(): bool
    {
        return boolval($this->navigationSystem);
    }

    /**
     * @param bool $navigationSystem
     */
    public function setNavigationSystem( bool $navigationSystem )
    {
        $this->navigationSystem = $navigationSystem;
    }

    /**
     * @return bool
     */
    public function isPowerAssistedSteering(): bool
    {
        return boolval($this->powerAssistedSteering);
    }

    /**
     * @param bool $powerAssistedSteering
     */
    public function setPowerAssistedSteering( bool $powerAssistedSteering )
    {
        $this->powerAssistedSteering = $powerAssistedSteering;
    }

    /**
     * @return bool
     */
    public function isTailLift(): bool
    {
        return boolval($this->tailLift);
    }

    /**
     * @param bool $tailLift
     */
    public function setTailLift( bool $tailLift )
    {
        $this->tailLift = $tailLift;
    }

    /**
     * @return bool
     */
    public function isXenonHeadlights(): bool
    {
        return boolval($this->xenonHeadlights);
    }

    /**
     * @param bool $xenonHeadlights
     */
    public function setXenonHeadlights( bool $xenonHeadlights )
    {
        $this->xenonHeadlights = $xenonHeadlights;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription( $description )
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param array $images
     */
    public function setImages( $images )
    {
        $this->images = $images;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice(Price $price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * @param mixed $seller
     */
    public function setSeller( $seller )
    {
        $this->seller = $seller;
    }

    /**
     * @return mixed
     */
    public function getWindshield()
    {
        return $this->windshield;
    }

    /**
     * @param mixed $windshield
     */
    public function setWindshield( $windshield )
    {
        $this->windshield = $windshield;
    }

    /**
     * @return bool
     */
    public function isTv(): bool
    {
        return $this->tv;
    }

    /**
     * @param bool $tv
     */
    public function setTv( bool $tv )
    {
        $this->tv = $tv;
    }

    /**
     * @return bool
     */
    public function isCrane()
    {
        return boolval($this->crane);
    }

    /**
     * @param bool $crane
     */
    public function setCrane( $crane )
    {
        $this->crane = $crane;
    }

    /**
     * @return mixed
     */
    public function getParkingAssistants()
    {
        return $this->parkingAssistants;
    }

    /**
     * @param mixed $parkingAssistants
     */
    public function setParkingAssistants( $parkingAssistants )
    {
        if ($parkingAssistants) {
            $this->parkingAssistants = 'REAR_VIEW_CAM';
        }
    }

    /**
     * @return bool
     */
    public function isAirco(): bool
    {
        return boolval($this->airco);
    }

    /**
     * @param bool $airco
     */
    public function setAirco( bool $airco )
    {
        $this->airco = $airco;
    }

    /**
     * @return bool
     */
    public function isAutomaticAirco(): bool
    {
        return boolval($this->automaticAirco);
    }

    /**
     * @param bool $automaticAirco
     */
    public function setAutomaticAirco( bool $automaticAirco )
    {
        $this->automaticAirco = $automaticAirco;
    }






    public function getClimatisation(){
        if (!$this->isAirco() && !$this->isAutomaticAirco()){
            return "NO_CLIMATISATION";
        }
        if ($this->isAutomaticAirco()){
            return "AUTOMATIC_CLIMATISATION";
        }
        if ($this->isAirco()){
            return "MANUAL_CLIMATISATION";
        }
    }



    public function jsonSerialize()
    {
        return [
            'mobileSellerId'    => $this->getMobileSellerId(),
            'make'              => $this->getMake(),
            'price'             => $this->getPrice()->jsonSerialize(),
            'firstRegistration'             => $this->getFirstRegistration(),
            'mileage'                       => $this->getMileage(),
            'modelDescription'              => $this->getModelDescription(),
            'damageUnrepaired'              => $this->isDamageUnrepaired(),
            'vehicleClass'                  => $this->getVehicleClass(),
            'category'                      => $this->getCategory(),
            'abs'                           => $this->isAbs(),
            'centralLocking'                => $this->isCentralLocking(),
            'climatisation'                 => $this->getClimatisation(),
            'crane'                         => $this->isCrane(),
            'cruiseControl'                 => $this->isCruiseControl(),
            //'driversSleepingCompartment'    => $this->isDriversSleepingCompartment(),
            //'electricExteriorMirrors'       => $this->isElectricExteriorMirrors(),
            //'electricHeatedSeats'           => $this->isElectricHeatedSeats(),
            //'electricWindows'               => $this->isElectricWindows(),
            //'handsFreePhoneSystem'          => $this->isHandsFreePhoneSystem(),
            'emissionClass'                 => $this->getEmissionClass(),
            'licensedWeight'                => $this->getLicensedWeight(),
            'licensedWeight'                => $this->getLicensedWeight(),
            'loadCapacity'                  => $this->getLoadCapacity(),
            'loadCapacity'                  => $this->getLoadCapacity(),
            //'multifunctionalWheel'          => $this->isMultifunctionalWheel(),
            'navigationSystem'              => $this->isNavigationSystem(),
            'powerAssistedSteering'         => $this->isPowerAssistedSteering(),
            //'retarder'                      => $this->isRetarder(),
            'tailLift'                      => $this->isTailLift(),
            //'tv'                            => $this->isTv(),



        ];
    }

}
