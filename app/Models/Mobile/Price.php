<?php

namespace App\Models\Mobile;

use Jenssegers\Model\Model;

class Price extends Model
{
    /*
    * Price of the vehicle for other dealers including VAT if reclaimable. Only visible in dealer area.
    * @var string
    */
    protected $dealerPriceGross = '';


    /*
    * Price of the vehicle for private buyers including VAT if reclaimable. This is the main price shown.
    * @var string
    */
    protected $consumerPriceGross = '';

    /*
    * Net price of the vehicle for other dealers. Can only be used when providing a VAT rate as gross price is calculated then. Only visible in dealer area.
    * @var string
    */
    protected $dealerPriceNet = '';


    /*
    * Net price of the vehicle for private buyers. Can only be used when providing a VAT rate as gross price is calculated then. price type explanation of values https://services.mobile.de/docs/seller-api.html#priceType_refdata
    * @var string
    */
    protected $consumerPriceNet = '';

    /*
    * Net price of the vehicle for private buyers. Can only be used when providing a VAT rate as gross price is calculated then. price type explanation of values https://services.mobile.de/docs/seller-api.html#priceType_refdata
    * @var string
    */
    protected $includedDeliveryCosts = '';


    /*
    * The VAT rate as percent number (a number between 0 and 100). For example 19. When you add this attribute then your vehicle is displayed as VAT deductible. See https://services.mobile.de/docs/seller-api.html#vatrate_refdata
    * @var string
    */
    protected $vatRate = '';


    /*
    * https://services.mobile.de/docs/seller-api.html#priceType_refdata
    * @var string
    */
    protected $type = '';

    /*
    * Information in which currency the price is stated. During upload this field is ignored and the currency taken from the sellers site-ID instead.
    * @var string
    */
    protected $currency = '';

    /**
     * @return string
     */
    public function getDealerPriceGross()
    {
        return $this->dealerPriceGross;
    }

    /**
     * @param string $dealerPriceGross
     */
    public function setDealerPriceGross( $dealerPriceGross )
    {
        $this->dealerPriceGross = $dealerPriceGross;
    }

    /**
     * @return string
     */
    public function getConsumerPriceGross()
    {
        return $this->consumerPriceGross;
    }

    /**
     * @param string $consumerPriceGross
     */
    public function setConsumerPriceGross( $consumerPriceGross )
    {
        $this->consumerPriceGross = $consumerPriceGross;
    }

    /**
     * @return string
     */
    public function getDealerPriceNet()
    {
        return $this->dealerPriceNet;
    }

    /**
     * @param string $dealerPriceNet
     */
    public function setDealerPriceNet( $dealerPriceNet )
    {
        $this->dealerPriceNet = $dealerPriceNet;
    }

    /**
     * @return string
     */
    public function getConsumerPriceNet()
    {
        return $this->consumerPriceNet;
    }

    /**
     * @param string $consumerPriceNet
     */
    public function setConsumerPriceNet( $consumerPriceNet )
    {
        $this->consumerPriceNet = $consumerPriceNet;
    }

    /**
     * @return string
     */
    public function getVatRate()
    {
        return $this->vatRate;
    }

    /**
     * @param string $vatRate
     */
    public function setVatRate( $vatRate )
    {
        $this->vatRate = $vatRate;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType( $type )
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency( $currency )
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getIncludedDeliveryCosts()
    {
        return $this->includedDeliveryCosts;
    }

    /**
     * @param string $includedDeliveryCosts
     */
    public function setIncludedDeliveryCosts( $includedDeliveryCosts )
    {
        $this->includedDeliveryCosts = $includedDeliveryCosts;
    }





    public function jsonSerialize()
    {
        return [
          'dealerPriceGross'        => $this->getDealerPriceGross(),
          'consumerPriceGross'      => $this->getConsumerPriceGross(),
          'dealerPriceNet'          => $this->getDealerPriceNet(),
          'consumerPriceNet'        => $this->getConsumerPriceNet(),
          'vatRate'                 => $this->getVatRate(),
          'type'                    => "FIXED",
          'currency'                => "EUR",
        ];
    }

}
