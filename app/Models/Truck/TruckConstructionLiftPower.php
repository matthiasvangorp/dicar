<?php

namespace App\Models\Truck;

use Illuminate\Database\Eloquent\Model;

class TruckConstructionLiftPower extends Model
{
    //
    protected $table = 'TruckConstructionLiftPowers';
}
