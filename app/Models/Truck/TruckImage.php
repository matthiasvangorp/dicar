<?php

namespace App\Models\Truck;

use Illuminate\Database\Eloquent\Model;

class TruckImage extends Model
{
    //
    protected $table = 'truckImages';

    public function truck(){
      return $this->belongsTo(('App\Models\Truck\Truck'));
    }
}
