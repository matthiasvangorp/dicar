<?php

namespace App\Models\Truck;

use Illuminate\Database\Eloquent\Model;

class TruckEngine extends Model
{
    //
    protected $table = 'truckEngines';
}
