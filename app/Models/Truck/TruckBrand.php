<?php

namespace App\Models\Truck;

use Illuminate\Database\Eloquent\Model;

class TruckBrand extends Model
{
    //
    protected $table = 'truckBrands';
}
