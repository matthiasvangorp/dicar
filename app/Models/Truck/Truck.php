<?php

namespace App\Models\Truck;

use Illuminate\Database\Eloquent\Model;

class Truck extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'trucks';

    public function brand(){
      return $this->belongsTo(TruckBrand::class, 'truckbrand_id');
    }

    public function engine(){
      return $this->belongsTo(TruckEngine::class, 'truckengine_id');
    }

    public function constructionLiftPower(){
        return $this->belongsTo(TruckConstructionLiftPower::class, 'construction_lift_power_id');
    }

    public function images(){
      return $this->hasMany('App\Models\Truck\TruckImage', 'truck_id')->orderBy('order');
    }
}
