<?php

namespace App\Models\Motorhome;

use Illuminate\Database\Eloquent\Model;

class Motorhome extends Model
{
    //
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'motorhomes';
}
