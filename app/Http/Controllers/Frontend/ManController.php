<?php

namespace App\Http\Controllers\Frontend;

use App\Repositories\Backend\Truck\TruckBrandRepository;
use App\Repositories\Backend\Truck\TruckConstructionLiftPowerRepository;
use App\Repositories\Backend\Truck\TruckEngineRepository;
use App\Repositories\Frontend\Truck\TruckRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManController extends Controller
{

    /*
     * @var TruckRepository
     */
    /**
     * TruckController constructor.
     * @param \App\Repositories\Frontend\Truck\TruckRepository $trucks
     * @param \App\Repositories\Frontend\Truck\TruckBrandRepository $brands
     * @param \App\Repositories\Backend\Truck\TruckEngineRepository $engines
     * @param \App\Repositories\Frontend\Truck\TruckConstructionLiftPowerRepository $constructionLiftPowers
     */
    public function __construct(TruckRepository $trucks, TruckBrandRepository $brands, TruckEngineRepository $engines, TruckConstructionLiftPowerRepository $constructionLiftPowers)
    {
        $this->trucks = $trucks;
        $this->brands = $brands;
        $this->engines = $engines;
        $this->constructionLiftPowers = $constructionLiftPowers;
    }



    /**
     * @return $this
     */
    public function index()
    {
        //
        $trucks = $this->trucks->getManTrucks();
        $brands = $this->brands->render();
        $brands->prepend(trans('trucks.fields.no_preference'), 0);
        $sort = $this->trucks->returnSort();
        $engines = $this->brands->render();
        $selectedSort = 'alphabetic';
        $constructionLiftPowers = $this->constructionLiftPowers->render();
        return view ('frontend.man.index')->with([
            'trucks'=> $trucks,
            'brands' => $brands,
            'engines' => $engines,
            'selectedBrand' => 0,
            'selectedEuronorm' => 0,
            'constructionLiftPowers' => $constructionLiftPowers,
            'sort'  => $sort,
            'selectedSort' => $selectedSort
        ]);
    }
}
