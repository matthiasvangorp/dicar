<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Truck\Truck;
use App\Repositories\Backend\Truck\TruckEngineRepository;
use App\Repositories\Frontend\Truck\TruckBrandRepository;
use App\Repositories\Frontend\Truck\TruckConstructionLiftPowerRepository;
use App\Repositories\Frontend\Truck\TruckRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;


class TruckController extends Controller
{
    /*
     * @var TruckRepository
     */
    /**
     * TruckController constructor.
     * @param \App\Repositories\Frontend\Truck\TruckRepository $trucks
     * @param \App\Repositories\Frontend\Truck\TruckBrandRepository $brands
     * @param \App\Repositories\Backend\Truck\TruckEngineRepository $engines
     * @param \App\Repositories\Frontend\Truck\TruckConstructionLiftPowerRepository $constructionLiftPowers
     */
    public function __construct(TruckRepository $trucks, TruckBrandRepository $brands, TruckEngineRepository $engines, TruckConstructionLiftPowerRepository $constructionLiftPowers)
    {
      $this->trucks = $trucks;
      $this->brands = $brands;
      $this->engines = $engines;
      $this->constructionLiftPowers = $constructionLiftPowers;
    }

    /**
     * @var TruckRepository
     */
    protected $trucks;

    /**
     * @var \App\Repositories\Frontend\Truck\TruckBrandRepository
     */
    protected $brands;

  /**
   * @var \App\Repositories\Frontend\Truck\TruckEngineRepository
   */
    protected $engines;

    /**
     * @var \App\Repositories\Frontend\Truck\TruckConstructionLiftPowerRepository
     */
    protected $constructionLiftPowers;


    /**
     * @return $this
     */
    public function index()
    {
        //
        $trucks = $this->trucks->render();
        $soldTrucks = $this->trucks->renderSoldTrucks();
        $brands = $this->brands->render();
        $brands->prepend(trans('trucks.fields.no_preference'), 0);
        $sort = $this->trucks->returnSort();
        $engines = $this->brands->render();
        $selectedSort = 'alphabetic';
        $constructionLiftPowers = $this->constructionLiftPowers->render();
        return view ('frontend.truck.index')->with([
            'trucks'=> $trucks,
            'soldTrucks'=> $soldTrucks,
            'brands' => $brands,
            'engines' => $engines,
            'selectedBrand' => 0,
            'selectedEuronorm' => 0,
            'constructionLiftPowers' => $constructionLiftPowers,
            'sort'  => $sort,
            'selectedSort' => $selectedSort
        ]);
    }


    /**
     * @param \Illuminate\Http\Request $request
     * @return $this
     */
      public function filter (Request $request){
        $trucks = $this->trucks->filter($request);
        $selectedBrand = $request->brand;
        $selectedEuronorm = $request->euronorm;

        $brands = $this->brands->render();
        $brands->prepend(trans('trucks.fields.no_preference'), 0);
        $engines = $this->brands->render();
        $constructionLiftPowers = $this->constructionLiftPowers->render();

        $sort = $this->trucks->returnSort();
        $selectedSort = 'updated_at';


        return view ('frontend.truck.index')->with([
            'trucks'=> $trucks,
            'brands' => $brands,
            'engines' => $engines,
            'selectedBrand' => $selectedBrand,
            'selectedEuronorm' => $selectedEuronorm,
            'constructionLiftPowers' => $constructionLiftPowers,
            'sort'  => $sort,
            'selectedSort' => $request->sortBy
        ]);
      }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
        $truck = $this->trucks->renderOne($id);
        $title = $truck->brand->name .' '. $truck->name . ' ' .$truck->engine->name . ' ' .trans('trucks.fields.secondhand_truck_for_sale');
        $locale = $this->getLocale();
        $defaultText1 = DB::table('defaultTexts')->where(['locale' => $locale, 'type' => 'fixed_text_1'])->value('message');
        $defaultText2 = DB::table('defaultTexts')->where(['locale' => $locale, 'type' => 'fixed_text_2'])->value('message');
        return view ('frontend.truck.show')->with(['title' => $title, 'truck'=> $truck, 'defaultText1' => $defaultText1, 'defaultText2' => $defaultText2]);
    }


    /**
     * @return mixed
     */
    public function createPDF($id){

        $truck = $this->trucks->renderOne($id);
        $title = $truck->brand->name .' '. $truck->name . ' ' .$truck->engine->name . ' ' .trans('trucks.fields.secondhand_truck_for_sale');
        $locale = $this->getLocale();
        $defaultText1 = DB::table('defaultTexts')->where(['locale' => $locale, 'type' => 'fixed_text_1'])->value('message');
        $defaultText2 = DB::table('defaultTexts')->where(['locale' => $locale,
          'type' => 'fixed_text_2'])->value('message');
        //return view ('frontend.truck.pdf')->with(['title' => $title, 'truck'=> $truck, 'defaultText1' => $defaultText1, 'defaultText2' => $defaultText2]);
        $pdf = PDF::loadView('frontend.truck.pdf', ['title' => $title, 'truck'=> $truck, 'defaultText1' => $defaultText1, 'defaultText2' => $defaultText2]);
        return $pdf->download('truck.pdf');
    }

    /**
     * @return mixed
     */
    public function createExport($id){

        $truck = $this->trucks->renderOne($id);
        $title = $truck->brand->name .' '. $truck->name . ' ' .$truck->engine->name . ' ' .trans('trucks.fields.secondhand_truck_for_sale');
        $locale = $this->getLocale();
        $defaultText1 = DB::table('defaultTexts')->where(['locale' => $locale, 'type' => 'fixed_text_1'])->value('message');
        $defaultText2 = DB::table('defaultTexts')->where(['locale' => $locale,
            'type' => 'fixed_text_2'])->value('message');
        return view ('frontend.truck.export')->with(['title' => $title, 'truck'=> $truck, 'defaultText1' => $defaultText1, 'defaultText2' => $defaultText2]);
    }

    private function getLocale(){
        if (session()->get('locale') == ""){
            $locale = "en";
        }
        else {
            $locale = session()->get('locale');
        }

        return $locale;
    }
}
