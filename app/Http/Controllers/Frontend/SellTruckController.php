<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\ContactFormRequest;
use App\Http\Requests\Frontend\SellFormRequest;
use App\Mail\Interest;
use App\Mail\SellYourTruck;
use Illuminate\Support\Facades\Mail;

class SellTruckController extends Controller
{
    //

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.sell_your_truck.form');
    }

    /**
     * @param \App\Http\Requests\Frontend\SellFormRequest $request
     * @return $this
     */
    public function store(SellFormRequest $request)
    {

        $content =
        [
            'title' => 'Verkoop je vrachtwagen',
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'zipcode' => $request->zipcode,
            'city' => $request->city,
            'phone' => $request->phone,
            'email' => $request->email,
            'brand' => $request->brand,
            'model' => $request->model,
            'year' => $request->year,
            'kilometers' => $request->kilometers,
            'maintenance_book' => $this->convertCheckboxValueToString($request->maintenance_book),
            'accident_free' => $this->convertCheckboxValueToString($request->accident_free),
            'horsepower' => $request->horsepower,
            'color' => $request->color,
            'asking_price' => $request->asking_price,
            'vat_inclusive' => $this->convertCheckboxValueToString($request->vat_inclusive),
            'private' => $this->convertCheckboxValueToString($request->private),
            'options' => $request->options,
            'remarks' => $request->remarks,
        ];

        Mail::to('maarten@dicar.be')->send(new SellYourTruck($content));
        return view('frontend.sell_your_truck.show')
          ->with('message', 'Thanks for contacting us!');

    }

    /**
     * @param $value
     * @return string
     */
    private function convertCheckboxValueToString($value){
        if ($value == "on"){
            return "Ja";
        }
        else {
            return "Nee";
        }
    }

    /**
     * @param \App\Http\Requests\Frontend\ContactFormRequest $request
     * @return $this
     */
    public function contact(ContactFormRequest $request)
    {

        $content =
          [
            'title' => 'Interesse in '. $request->truck,
            'reference' => $request->reference,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'remarks' => $request->remarks,
          ];

        Mail::to('maarten@dicar.be')->send(new Interest($content));
        return view('frontend.sell_your_truck.show')
          ->with('message', 'Thanks for contacting us!');

    }
}
