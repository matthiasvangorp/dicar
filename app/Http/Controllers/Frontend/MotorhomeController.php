<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Motorhome\ManageMotorhomeRequest;
use App\Models\Motorhome\Motorhome;
use App\Repositories\Frontend\Motorhome\MotorhomeRepository;
use Illuminate\Http\Request;

class MotorhomeController extends Controller
{
    /*
     * @var MotorhomeRepository
     */
    public function __construct(MotorhomeRepository $motorhomes)
    {
      $this->motorhomes = $motorhomes;
    }

    /**
     * @var MotorhomeRepository
     */
    protected $motorhomes;

    /*public function __construct(MotorhomeRepository $motorhomes)
    {
      $this->motorhomes = $motorhomes;
    }*/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ManageMotorhomeRequest $request)
    {
        //
        $motorhomes = $this->motorhomes->render();
        return view ('frontend.motorhome.index')->with('motorhomes', $motorhomes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
