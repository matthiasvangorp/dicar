<?php

namespace App\Http\Controllers;

/**
 * Class LanguageController.
 */
class LanguageController extends Controller
{
    /**
     * @param $lang
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function swap($lang)
    {

        $url = url()->previous();
        $uri_segments = explode('/', $url);
        $uri_segments[3] = $lang;
        $url = implode("/", $uri_segments);
        session()->put('locale', $lang);

        return redirect($url);
    }


}
