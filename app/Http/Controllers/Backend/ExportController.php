<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Truck\TruckRepository;
use App\Services\ExportService;

class ExportController extends Controller
{

    /**
     * ExportController constructor.
     * @param \App\Repositories\Backend\Truck\TruckRepository $trucks
     * @param \App\Services\ExportService $exportService
     */
    public function __construct(
        TruckRepository $trucks,
        ExportService $exportService
    )

    {
        $this->trucks = $trucks;
        $this->exportService = $exportService;
    }

    /**
     * @var TruckRepository
     */
    protected $trucks;

    /**
     * @var \xportService
     */
    protected $exportService;


    /**
     *
     */
    public function exportAll(){
        $trucks = $this->trucks->getTrucksForExport();
        foreach ($trucks as $truck){
            $this->exportService->exportTruck($truck->id);
        }
        flash("Trucks succesvol geëxporteerd naar Mobile.de")->success();
        return redirect('admin/trucks/');

    }

    /**
     * @param $truckId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function export($truckId){
        $adId = $this->exportService->exportTruck($truckId);
        flash("Truck succesvol geëxporteerd naar Mobile.de (adId : $adId)")->success();
        return redirect('admin/trucks/');
    }

    public function destroy($truckId){
        $adId = $this->exportService->deleteTruck($truckId);
        flash("Trucks succesvol verwijdert uit Mobile.de  (adId : $adId)" )->success();
        return redirect('admin/trucks/');
    }


}
