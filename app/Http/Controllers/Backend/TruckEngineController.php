<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Truck\StoreTruckEngineRequest;
use App\Http\Requests\Backend\Truck\ManageTruckEngineRequest;
use App\Models\Truck\Truck;
use App\Repositories\Backend\Truck\TruckEngineRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class TruckEngineController extends Controller
{
    /*
     * @var TruckRepository
     */
    public function __construct(TruckEngineRepository $engines)
    {
      $this->engines = $engines;
    }

    /**
     * @var TruckEngineRepository
     */
    protected $engines;

  /**
   * Display a listing of the resource.
   *
   * @param \App\Http\Requests\Backend\Truck\ManageTruckEngineRequest $request
   * @return \Illuminate\Http\Response
   */
    public function index(ManageTruckEngineRequest $request)
    {
        //
      $engines = $this->engines->render();

      if ($request->ajax()) {
        $response = new Response();
        $response->setStatusCode(200);
        $response->setContent($engines);
        return $response;
      } else {
        return view('backend.truck.engine.index')->with('engines', $engines);
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
      $engines = $this->engines->render();
      return view ('backend.truck.engine.create')->with('engines', $engines);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTruckEngineRequest $request) {
      //check if engine exists before adding it
      $alreadyExits = (boolean) $this->engines->find($request->input());
      if (!$alreadyExits) {
        $engine = $this->engines->create(
          [
            'data' => $request->input(),
          ]);
      }

      //redirect
      if ($request->ajax()) {
        $response = new Response();
        if (!$alreadyExits){
          $response->setStatusCode(200);
          $response->setContent(json_encode($engine));

        }
        else {
          $response->setStatusCode(409);
          $response->setContent(json_encode(trans('strings.backend.general.engine_already_exists')));
        }
        return $response;
      }
      else {
        $engines = $this->engines->render();
        return view('backend.truck.engine.index')->with('engines', $engines);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $truck = $this->engines->renderOne($id);
        $user = Auth::user();
        return view ('backend.truck.show')->with(['truck'=> $truck, 'user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
      $truck = $this->engines->renderOne($id);
      $user = Auth::user();
      return view ('backend.truck.edit')->with(['truck'=> $truck, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $truck = $this->engines->renderOne($id);
        $this->engines->update(
          [
            'truck' => $truck,
            'data' => $request->input(),
            'images'  => $request->images
          ]);

      $engines = $this->engines->render();
      return view ('backend.truck.engine.index')->with('engines', $engines);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
