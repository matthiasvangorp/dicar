<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Motorhome\StoreMotorhomeRequest;
use App\Http\Requests\Frontend\Motorhome\ManageMotorhomeRequest;
use App\Models\Motorhome\Motorhome;
use App\Repositories\Frontend\Motorhome\MotorhomeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class MotorhomeController extends Controller
{
    /*
     * @var MotorhomeRepository
     */
    public function __construct(MotorhomeRepository $motorhomes)
    {
      $this->motorhomes = $motorhomes;
    }

    /**
     * @var MotorhomeRepository
     */
    protected $motorhomes;

    /*public function __construct(MotorhomeRepository $motorhomes)
    {
      $this->motorhomes = $motorhomes;
    }*/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ManageMotorhomeRequest $request)
    {
        //
        $motorhomes = $this->motorhomes->render();
        return view ('backend.motorhome.index')->with('motorhomes', $motorhomes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
      $motorhomes = $this->motorhomes->render();
      return view ('backend.motorhome.create')->with('motorhomes', $motorhomes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMotorhomeRequest $request)
    {
      $this->motorhomes->create(
        [
          'data' => $request->only(
            'name',
            'seats',
            'sleepingPlaces',
            'images[]'
          ),
          'images'  => $request->images
        ]);

      Redirect::to('admin/motorhomes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $motorhome = $this->motorhomes->renderOne($id);
        $user = Auth::user();
        return view ('backend.motorhome.show')->with(['motorhome'=> $motorhome, 'user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
