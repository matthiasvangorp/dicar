<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Truck\ManageTruckConstructionLiftPowerRequest;
use App\Http\Requests\Backend\Truck\StoreTruckConstructionLiftPowerRequest;
use App\Models\Truck\Truck;
use App\Repositories\Backend\Truck\TruckConstructionLiftPowerRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class TruckConstructionLiftPowerController extends Controller
{
    /*
     * @var TruckRepository
     */
    public function __construct(TruckConstructionLiftPowerRepository $constructionLiftPowers)
    {
      $this->constructionLiftPowers = $constructionLiftPowers;
    }

    /**
     * @var TruckConstructionLiftPowerRepository
     */
    protected $constructionLiftPowers;

  /**
   * Display a listing of the resource.
   *
   * @param \App\Http\Requests\Backend\Truck\ManageTruckConstructionLiftPowerRequest $request
   * @return \Illuminate\Http\Response
   */
    public function index(ManageTruckConstructionLiftPowerRequest $request)
    {
        //
      $constructionLiftPowers = $this->constructionLiftPowers->render();

      if ($request->ajax()) {
        $response = new Response();
        $response->setStatusCode(200);
        $response->setContent($constructionLiftPowers);
        return $response;
      } else {
        return view('backend.truck.construction_lift_power.index')->with('constructionLiftPowers', $constructionLiftPowers);
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
      $constructionLiftPowers = $this->constructionLiftPowers->render();
      return view ('backend.truck.construction_lift_power.create')->with('constructionLiftPowers', $constructionLiftPowers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTruckConstructionLiftPowerRequest $request) {
      //check if construction lift power exists before adding it
      $alreadyExits = (boolean) $this->constructionLiftPowers->find($request->input());
      if (!$alreadyExits) {
        $constructionLiftPower = $this->constructionLiftPowers->create(
          [
            'data' => $request->input(),
          ]);
      }

      //redirect
      if ($request->ajax()) {
        $response = new Response();
        if (!$alreadyExits){
          $response->setStatusCode(200);
          $response->setContent(json_encode($constructionLiftPower));

        }
        else {
          $response->setStatusCode(409);
          $response->setContent(json_encode(trans('strings.backend.general.construction_lift_power_already_exists')));
        }
        return $response;
      }
      else {
        $constructionLiftPowers = $this->constructionLiftPowers->render();
        return view('backend.truck.construction_lift_power.index')->with('constructionLiftPowers', $constructionLiftPowers);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $truck = $this->constructionLiftPowers->renderOne($id);
        $user = Auth::user();
        return view ('backend.truck.show')->with(['truck'=> $truck, 'user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
      $truck = $this->constructionLiftPowers->renderOne($id);
      $user = Auth::user();
      return view ('backend.truck.edit')->with(['truck'=> $truck, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $truck = $this->constructionLiftPowers->renderOne($id);
        $this->constructionLiftPowers->update(
          [
            'truck' => $truck,
            'data' => $request->input(),
            'images'  => $request->images
          ]);

      $constructionLiftPowers = $this->constructionLiftPowers->render();
      return view ('backend.truck.construction_lift_power.index')->with('constructionLiftPowers', $constructionLiftPowers);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
