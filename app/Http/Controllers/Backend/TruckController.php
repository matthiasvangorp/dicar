<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AjaxReferenceExistsRequest;
use App\Http\Requests\Backend\Truck\StoreTruckRequest;
use App\Http\Requests\Backend\Truck\ManageTruckRequest;
use App\Repositories\Backend\Truck\TruckBrandRepository;
use App\Repositories\Backend\Truck\TruckConstructionLiftPowerRepository;
use App\Repositories\Backend\Truck\TruckEngineRepository;
use App\Repositories\Backend\Truck\TruckRepository;
use App\Repositories\Backend\Truck\TruckImageRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\ExportService;

class TruckController extends Controller
{
    /**
     * TruckController constructor.
     * @param \App\Repositories\Backend\Truck\TruckRepository $trucks
     * @param \App\Repositories\Backend\Truck\TruckBrandRepository $brands
     * @param \App\Repositories\Backend\Truck\TruckImageRepository $images
     * @param \App\Repositories\Backend\Truck\TruckEngineRepository $engines
     * @param \App\Repositories\Backend\Truck\TruckConstructionLiftPowerRepository $constructionLiftPowers
     * @param \App\Services\ExportService $exportService
     */
    public function __construct(
        TruckRepository $trucks,
        TruckBrandRepository $brands,
        TruckImageRepository $images,
        TruckEngineRepository $engines,
        TruckConstructionLiftPowerRepository $constructionLiftPowers,
        ExportService $exportService

    )
    {
        $this->trucks = $trucks;
        $this->brands = $brands;
        $this->engines = $engines;
        $this->images = $images;
        $this->constructionLiftPowers = $constructionLiftPowers;
        $this->exportService = $exportService;
    }

    /**
     * @var TruckRepository
     */
    protected $trucks;

    /**
    * @var TruckBrandRepository
    */

    protected $brands;

    /**
     * @var \ExportService
     */
    protected $exportService;


  /**
   * Display a listing of the resource.
   *
   * @param \App\Http\Requests\Backend\Truck\ManageTruckRequest $request
   * @return \Illuminate\Http\Response
   */
    public function index(ManageTruckRequest $request)
    {
        //
        $trucks = $this->trucks->render();
        return view ('backend.truck.index')->with('trucks', $trucks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        foreach (config('colors.colors') as $key){
            $colors[$key] = trans('strings.colors.'.$key);
        }

        foreach (config('construction.construction_types') as $key){
            $construction_types[$key] = trans('strings.construction_types.'.$key);
        }

        foreach (config('construction.drivetrains') as $key){
            $drivetrains[$key] = trans('strings.drivetrains.'.$key);
        }

        $brands = $this->brands->render();
        $engines = $this->engines->render();
        $constructionLiftPowers = $this->constructionLiftPowers->render();


        $locale = $this->getLocale();
        $defaultText1 = DB::table('defaultTexts')->where(['locale' => $locale, 'type' => 'fixed_text_1'])->value('message');
        $defaultText2 = DB::table('defaultTexts')->where(['locale' => $locale, 'type' => 'fixed_text_2'])->value('message');

        return view ('backend.truck.create')->with([
          'brands' => $brands,
          'colors' => $colors,
          'engines' => $engines,
          'construction_types' => $construction_types,
          'drivetrains' => $drivetrains,
          'constructionLiftPowers' => $constructionLiftPowers,
          'defaultText1' => $defaultText1,
          'defaultText2' => $defaultText2
        ]);
    }

    /*
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTruckRequest $request)
    {
        $id = $this->trucks->create(
        [
          'data' => $request->input(),
          'images'  => $request->images
        ]);

        //truck has been saved, now export it to mobile
        //$adId = $this->exportService->exportTruck($id);
        //flash("Truck succesvol geëxporeerd naar Mobile.de (adId : $adId)")->success();


        $trucks = $this->trucks->render();
        return view ('backend.truck.index')->with('trucks', $trucks);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $truck = $this->trucks->renderOne($id);
        $user = Auth::user();
        return view ('backend.truck.show')->with(['truck'=> $truck, 'user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $truck = $this->trucks->renderOne($id);
        foreach (config('colors.colors') as $key){
        $colors[$key] = trans('strings.colors.'.$key);
        }

        foreach (config('construction.construction_types') as $key){
        $construction_types[$key] = trans('strings.construction_types.'.$key);
        }

        foreach (config('construction.drivetrains') as $key){
        $drivetrains[$key] = trans('strings.drivetrains.'.$key);
        }

        $imageOrder = "";
        foreach ($truck->images as $image){
        $imageOrder .= $image->filename . ",";
        }

        $imageOrder =  substr($imageOrder, 0, -1);

        $brands = $this->brands->render();
        $engines = $this->engines->render();
        $constructionLiftPowers = $this->constructionLiftPowers->render();


        $locale = $this->getLocale();
        $defaultText1 = DB::table('defaultTexts')->where(['locale' => $locale, 'type' => 'fixed_text_1'])->value('message');
        $defaultText2 = DB::table('defaultTexts')->where(['locale' => $locale, 'type' => 'fixed_text_2'])->value('message');

        return view ('backend.truck.edit')->with([
            'truck'=> $truck,
            'brands' => $brands,
            'colors' => $colors,
            'engines' => $engines,
            'construction_types' => $construction_types,
            'drivetrains' => $drivetrains,
            'constructionLiftPowers' => $constructionLiftPowers,
            'defaultText1' => $defaultText1,
            'defaultText2' => $defaultText2,
            'imageOrder' => $imageOrder
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTruckRequest $request, $id)
    {
        //
        if ($id == "upload"){
            return $this->upload($request);
        }
        $truck = $this->trucks->renderOne($id);
        $this->trucks->update(
          [
            'truck' => $truck,
            'data' => $request->input()
          ]);

        //truck has been saved, now export it to mobile
        //$adId = $this->exportService->exportTruck($id);

        $trucks = $this->trucks->render();
        //flash("Truck succesvol geëxporeerd naar Mobile.de (adId : $adId)")->success();
        return view ('backend.truck.index')->with('trucks', $trucks);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //remove from mobile
        $this->exportService->deleteTruck($id);
        //remove truck
        $this->trucks->delete($id);

        $trucks = $this->trucks->render();
        return view ('backend.truck.index')->with('trucks', $trucks);
    }

    public function upload (Request $request){
      $images = [];

      foreach($request->images as $image){
        if ($image->isValid()){

          //$fileName = $image->store('/images/trucks/temp');

          $imageDirectory = public_path() . "/images/trucks/temp/";
          $extension = $image->getClientOriginalExtension(); // getting image extension
          $fileName = rand(11111,99999).'.'.$extension; // renameing image
          $image->move($imageDirectory, $fileName);

          $truckImage = $this->images->create(['data' => ['filename' => $fileName]]);
          $photo = new \stdClass();
          $photo->name = $fileName;
          $photo->url = $imageDirectory . $fileName;
          $photo->thumbnailUrl = "http://$_SERVER[HTTP_HOST]". "/photo/100x100/images/trucks/temp/".$fileName;
          $photo->size = round(filesize($imageDirectory . $fileName) / 1024, 2);
          $photo->file_id = $truckImage->id;
          $images[] = $photo;

        }
      }
      return response()->json(array('files' => $images), 200);
    }

    public function defaultText(Request $request){
        $message = $request->input('text');
        $type = $request->input('text_type');

        $locale = $this->getLocale();
        DB::table('defaultTexts')->where(['type' => $type, 'locale' => $locale])->update(['message' => $message]);

        return response()->json(array('message' => $message), 200);
    }

    private function getLocale(){
        if (session()->get('locale') == ""){
            $locale = "en";
        }
        else {
            $locale = session()->get('locale');
        }

        return $locale;
    }


    public function reference_exists(AjaxReferenceExistsRequest $request){
        return $this->trucks->findBy('reference', $request->reference);
    }

}
