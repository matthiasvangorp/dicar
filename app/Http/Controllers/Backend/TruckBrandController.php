<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Truck\StoreTruckBrandRequest;
use App\Http\Requests\Backend\Truck\ManageTruckBrandRequest;
use App\Models\Truck\Truck;
use App\Repositories\Backend\Truck\TruckBrandRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class TruckBrandController extends Controller
{
    /*
     * @var TruckRepository
     */
    public function __construct(TruckBrandRepository $brands)
    {
      $this->brands = $brands;
    }

    /**
     * @var TruckRepository
     */
    protected $brands;

  /**
   * Display a listing of the resource.
   *
   * @param \App\Http\Requests\Backend\Truck\ManageTruckBrandRequest $request
   * @return \Illuminate\Http\Response
   */
    public function index(ManageTruckBrandRequest $request)
    {
        //
      $brands = $this->brands->render();

      if ($request->ajax()) {
        $response = new Response();
        $response->setStatusCode(200);
        $response->setContent($brands);
        return $response;
      } else {
        return view('backend.truck.brand.index')->with('brands', $brands);
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
      $brands = $this->brands->render();
      return view ('backend.truck.brand.create')->with('brands', $brands);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTruckBrandRequest $request) {
      //check if brand exists before adding it
      $alreadyExits = (boolean) $this->brands->find($request->input());
      if (!$alreadyExits) {
        $brand = $this->brands->create(
          [
            'data' => $request->input(),
          ]);
      }

      //redirect
      if ($request->ajax()) {
        $response = new Response();
        if (!$alreadyExits){
          $response->setStatusCode(200);
          $response->setContent(json_encode($brand));

        }
        else {
          $response->setStatusCode(409);
          $response->setContent(json_encode(trans('strings.backend.general.brand_already_exists')));
        }
        return $response;
      }
      else {
        $brands = $this->brands->render();
        return view('backend.truck.brand.index')->with('brands', $brands);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $truck = $this->brands->renderOne($id);
        $user = Auth::user();
        return view ('backend.truck.show')->with(['truck'=> $truck, 'user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
      $truck = $this->brands->renderOne($id);
      $user = Auth::user();
      return view ('backend.truck.edit')->with(['truck'=> $truck, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $truck = $this->brands->renderOne($id);
        $this->brands->update(
          [
            'truck' => $truck,
            'data' => $request->input(),
            'images'  => $request->images
          ]);

      $brands = $this->brands->render();
      return view ('backend.truck.brand.index')->with('brands', $brands);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
