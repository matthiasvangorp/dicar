<?php

namespace App\Http\Requests\Backend\Motorhome;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreMotorhomeRequest.
 */
class StoreMotorhomeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(1);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'            => 'required|max:191',
            'sleepingPlaces'  => 'required|max:191',
            'seats'           => 'required|max:191',
            'images[]'        => 'mimes:jpeg,bmp,png'
        ];
    }
}
