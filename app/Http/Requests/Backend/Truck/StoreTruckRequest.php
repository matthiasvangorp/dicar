<?php

namespace App\Http\Requests\Backend\Truck;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreTruckRequest.
 */
class StoreTruckRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(1);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required|max:191',
            'images[]'          => 'mimes:jpeg,bmp,png',
            'year'              => 'date_format:"m/Y"',
            'chassis_number'    => 'required|string|min:4',
            'price'             => 'required',
            'mileage'           => 'required|integer',
            'weights_mtm'       => 'required|integer',
        ];
    }
}
