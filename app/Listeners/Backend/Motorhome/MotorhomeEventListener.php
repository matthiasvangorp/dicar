<?php

namespace App\Listeners\Backend\Motorhome;
use App\Models\Access\User\User;

/**
 * Class MotorhomeEventListener.
 */
class MotorhomeEventListener
{
    /**
     * @var string
     */
    private $history_slug = 'Motorhome';

  /**
   * @param $event
   * @param $user
   */
    public function onCreated($event)
    {
      $user = $event;
        history()->withType($this->history_slug)
            ->withEntity($event->event->user->id)
            ->withText('trans("history.backend.motorhomes.created") '."<strong>".$event->event->motorhome->name."</strong>")
            ->withIcon('plus')
            ->withClass('bg-green')
            ->log();
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->user->id)
            ->withText('trans("history.backend.users.updated") <strong>{user}</strong>')
            ->withIcon('save')
            ->withClass('bg-aqua')
            ->withAssets([
                'user_link' => ['admin.access.user.show', $event->user->full_name, $event->user->id],
            ])
            ->log();
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->user->id)
            ->withText('trans("history.backend.users.deleted") <strong>{user}</strong>')
            ->withIcon('trash')
            ->withClass('bg-maroon')
            ->withAssets([
                'user_link' => ['admin.access.user.show', $event->user->full_name, $event->user->id],
            ])
            ->log();
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->user->id)
            ->withText('trans("history.backend.users.restored") <strong>{user}</strong>')
            ->withIcon('refresh')
            ->withClass('bg-aqua')
            ->withAssets([
                'user_link' => ['admin.access.user.show', $event->user->full_name, $event->user->id],
            ])
            ->log();
    }

    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->user->id)
            ->withText('trans("history.backend.users.permanently_deleted") <strong>{user}</strong>')
            ->withIcon('trash')
            ->withClass('bg-maroon')
            ->withAssets([
                'user_string' => $event->user->full_name,
            ])
            ->log();

        history()->withType($this->history_slug)
            ->withEntity($event->user->id)
            ->withAssets([
                'user_string' => $event->user->full_name,
            ])
            ->updateMotorhomeLinkAssets();
    }

    /**
     * @param $event
     */
    public function onPasswordChanged($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->user->id)
            ->withText('trans("history.backend.users.changed_password") <strong>{user}</strong>')
            ->withIcon('lock')
            ->withClass('bg-blue')
            ->withAssets([
                'user_link' => ['admin.access.user.show', $event->user->full_name, $event->user->id],
            ])
            ->log();
    }

    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->user->id)
            ->withText('trans("history.backend.users.deactivated") <strong>{user}</strong>')
            ->withIcon('times')
            ->withClass('bg-yellow')
            ->withAssets([
                'user_link' => ['admin.access.user.show', $event->user->full_name, $event->user->id],
            ])
            ->log();
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->user->id)
            ->withText('trans("history.backend.users.reactivated") <strong>{user}</strong>')
            ->withIcon('check')
            ->withClass('bg-green')
            ->withAssets([
                'user_link' => ['admin.access.user.show', $event->user->full_name, $event->user->id],
            ])
            ->log();
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Motorhome\MotorhomeCreated::class,
            'App\Listeners\Backend\Motorhome\MotorhomeEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Motorhome\MotorhomeUpdated::class,
            'App\Listeners\Backend\Motorhome\MotorhomeEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Motorhome\MotorhomeDeleted::class,
            'App\Listeners\Backend\Motorhome\MotorhomeEventListener@onDeleted'
        );

        $events->listen(
            \App\Events\Backend\Motorhome\MotorhomeRestored::class,
            'App\Listeners\Backend\Motorhome\MotorhomeEventListener@onRestored'
        );

        $events->listen(
            \App\Events\Backend\Motorhome\MotorhomePermanentlyDeleted::class,
            'App\Listeners\Backend\Motorhome\MotorhomeEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            \App\Events\Backend\Motorhome\MotorhomePasswordChanged::class,
            'App\Listeners\Backend\Motorhome\MotorhomeEventListener@onPasswordChanged'
        );

        $events->listen(
            \App\Events\Backend\Motorhome\MotorhomeDeactivated::class,
            'App\Listeners\Backend\Motorhome\MotorhomeEventListener@onDeactivated'
        );

        $events->listen(
            \App\Events\Backend\Motorhome\MotorhomeReactivated::class,
            'App\Listeners\Backend\Motorhome\MotorhomeEventListener@onReactivated'
        );
    }
}
