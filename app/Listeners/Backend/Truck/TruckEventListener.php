<?php

namespace App\Listeners\Backend\Truck;
use App\Models\Access\User\User;

/**
 * Class TruckEventListener.
 */
class TruckEventListener
{
    /**
     * @var string
     */
    private $history_slug = 'Truck';

  /**
   * @param $event
   * @param $user
   */
    public function onCreated($event)
    {
      $truck = $event;
        history()->withType($this->history_slug)
            ->withEntity($event->event->truck->id)
            ->withText('trans("history.backend.trucks.created") '."<strong>".$event->event->truck->name."</strong>")
            ->withIcon('plus')
            ->withClass('bg-green')
            ->log();
    }

    /**
     * @param $event
     * @param $user
     */
    public function onBrandCreated($event)
    {
      $brand = $event->event;
      history()->withType($this->history_slug)
        ->withEntity($brand->brand->id)
        ->withText('trans("history.backend.brands.created") '."<strong>".$brand->brand->name."</strong>")
        ->withIcon('plus')
        ->withClass('bg-green')
        ->log();
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->event->truck->id)
            ->withText('trans("history.backend.trucks.updated") <strong>{truck}</strong>')
            ->withIcon('save')
            ->withClass('bg-aqua')
            ->log();
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->truck->id)
            ->withText('trans("history.backend.trucks.deleted") <strong>{truck}</strong>')
            ->withIcon('trash')
            ->withClass('bg-maroon')
            ->log();
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->truck->id)
            ->withText('trans("history.backend.trucks.restored") <strong>{truck}</strong>')
            ->withIcon('refresh')
            ->withClass('bg-aqua')
            ->log();
    }

    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->truck->id)
            ->withText('trans("history.backend.trucks.permanently_deleted") <strong>{truck}</strong>')
            ->withIcon('trash')
            ->withClass('bg-maroon')
            ->log();

        history()->withType($this->history_slug)
            ->withEntity($event->truck->id)
            ->withAssets([
                'truck_string' => $event->truck->full_name,
            ])
            ->updateTruckLinkAssets();
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Truck\TruckCreated::class,
            'App\Listeners\Backend\Truck\TruckEventListener@onCreated'
        );

        $events->listen(
          \App\Events\Backend\Truck\TruckBrandCreated::class,
          'App\Listeners\Backend\Truck\TruckEventListener@onBrandCreated'
        );

        $events->listen(
            \App\Events\Backend\Truck\TruckUpdated::class,
            'App\Listeners\Backend\Truck\TruckEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Truck\TruckDeleted::class,
            'App\Listeners\Backend\Truck\TruckEventListener@onDeleted'
        );

        $events->listen(
            \App\Events\Backend\Truck\TruckRestored::class,
            'App\Listeners\Backend\Truck\TruckEventListener@onRestored'
        );

        $events->listen(
            \App\Events\Backend\Truck\TruckPermanentlyDeleted::class,
            'App\Listeners\Backend\Truck\TruckEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            \App\Events\Backend\Truck\TruckPasswordChanged::class,
            'App\Listeners\Backend\Truck\TruckEventListener@onPasswordChanged'
        );

        $events->listen(
            \App\Events\Backend\Truck\TruckDeactivated::class,
            'App\Listeners\Backend\Truck\TruckEventListener@onDeactivated'
        );

        $events->listen(
            \App\Events\Backend\Truck\TruckReactivated::class,
            'App\Listeners\Backend\Truck\TruckEventListener@onReactivated'
        );
    }
}
