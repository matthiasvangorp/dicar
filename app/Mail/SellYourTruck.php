<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SellYourTruck extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content)
    {
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->content['email'], $this->content['name'])
            ->subject(trans('strings.sell.sell_your_truck'))
            ->replyTo($this->content['email'], $this->content['name'])
            ->markdown('emails.truck.sell')
            ->with('content',$this->content);;
    }
}
