<?php

/**
 * Global Routes
 * Routes that are used between both frontend and backend.
 */

// Switch between the included languages
Route::get('lang/{lang}', 'LanguageController@swap');

//Route::resource('photo', 'PhotoController');
Route::get('/photo/{size}/{imagesDirectory?}/{imagesSubDirectory?}/{id?}/{name?}',
  function($size, $imagesDirectory = null, $imagesSubDirectory = null, $id = null, $name = null)
{

  if(!is_null($size) && !is_null($name)){
    $size = explode('x', $size);
    //$cache_image = Image::cache(function($image) use($size, $name){
    $cache_image = Image::cache(function($image) use($size, $name, $imagesDirectory, $imagesSubDirectory, $id){
      $test = url('/'.$imagesDirectory.'/'.$imagesSubDirectory.'/'.$id.'/'.$name);
      return $image->make(url('/'.$imagesDirectory.'/'.$imagesSubDirectory.'/'.$id.'/'.$name))->resize($size[0], $size[1]);
    }, 10); // cache for 10 minutes

    return Response::make($cache_image, 200, ['Content-Type' => 'image']);
  } else {
    abort(404);
  }
});
/* ----------------------------------------------------------------------- */

/*
 * Frontend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
    includeRouteFiles(__DIR__.'/Frontend/');
});

/* ----------------------------------------------------------------------- */

/*
 * Backend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    /*
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     */
    includeRouteFiles(__DIR__.'/Backend/');
});
