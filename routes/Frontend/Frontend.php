<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */

Route::get('macros', 'FrontendController@macros')->name('macros');
Route::resource('motorhomes', 'MotorhomeController');
Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function() {
        Route::get('/', 'TruckController@index')->name('index');
        Route::post('/', 'TruckController@filter')->name('filter');
        Route::get('sell_your_truck', 'SellTruckController@index')->name('sell_your_truck');
        Route::post('sell_your_truck', 'SellTruckController@store');
        Route::post('contact_us', 'SellTruckController@contact');
        Route::resource('trucks', 'TruckController');
        Route::get('tweedehands-vrachtwagens-man-tgl', 'ManController@index');
        Route::get('contact', 'ContactController@index')->name('contact');
    });


Route::get('pdf/{id}', 'TruckController@createPDF');
Route::get('export/{id}', 'TruckController@createExport');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        /*
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');

        /*
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');

    });
});
