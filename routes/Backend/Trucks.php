<?php

/**
 * All route names are prefixed with 'admin.'.
 */

Route::post('trucks/upload', 'TruckController@upload');
Route::post('trucks/reference_exists', 'TruckController@reference_exists');
Route::resource('trucks', 'TruckController');


