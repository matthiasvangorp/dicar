<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::resource('motorhomes', 'MotorhomeController');
