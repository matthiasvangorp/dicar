<?php

/**
 * All route names are prefixed with 'admin.'.
 */

Route::get('trucks/export/{truck}', 'ExportController@export');
Route::delete('trucks/export/{truck}', 'ExportController@destroy');

