<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\BrowserKitTestCase;

class truckTest extends BrowserKitTestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRoute()
    {
        $this->actingAs($this->admin)
          ->visit('/admin/trucks')
          ->see('Dicar trucks');
    }

    public function testCreate()
    {
        $this->actingAs($this->admin)
          ->visit('admin/truckBrands/create')
          ->type('Mercedes', 'name')
          ->press('Create the brand');

        $this->actingAs($this->admin)
          ->visit('admin/truckEngines/create')
          ->type('2.3 Dci', 'name')
          ->press('Create the engine');



        $this->actingAs($this->admin)
            ->visit('admin/trucks/create')
            ->select('1', 'brand')
            ->select('1', 'engine')
            ->type('XF', 'name')
            ->type('983309802223', 'chassis_number')
            ->type('20000', 'price')
            ->type('5', 'euronorm')
            ->type('01/2010', 'year')
            ->type('100000', 'mileage')
            ->type('110', 'horsepower')
            ->press('Create new truck')
            ->seePageIs('/admin/trucks')
            ->see('XF')
            ->see('2223');
    }
}
