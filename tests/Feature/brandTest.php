<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\BrowserKitTestCase;

class brandTest extends BrowserKitTestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRoute()
    {
        $this->actingAs($this->admin)
          ->visit('/admin/truckBrands')
          ->see('Dicar trucks');
    }

    public function testCreate()
    {
        $this->actingAs($this->admin)
            ->visit('admin/truckBrands/create')
            ->type('Mercedes', 'name')
            ->press('Create the brand')
            ->seePageIs('/admin/truckBrands')
            ->see('Mercedes');
    }
}
