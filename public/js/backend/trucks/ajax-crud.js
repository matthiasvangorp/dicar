$(document).ready(function() {
    $('#btn-add-brand').click(function(){
        //get the parent div
        var div = $(this).parent();
        $('#modalBrands').modal('show');
    });

    $('#btn-add-engine').click(function(){
        //get the parent div
        var div = $(this).parent();
        $('#modalEngines').modal('show');
    });

    $('#edit_default_text_1').click(function(){
        $('#default_text_type').val('fixed_text_1')
        CKEDITOR.instances.default_text_edit.setData($('#default_text_1').html())
        $('#modalDefaultText').modal('show');
    });
    $('#edit_default_text_2').click(function(){
        $('#default_text_type').val('fixed_text_2')
        CKEDITOR.instances.default_text_edit.setData($('#default_text_2').html())
        $('#modalDefaultText').modal('show');
    });

    $('#btn-add-construction-lift-power').click(function(){
        //get the parent div
        var div = $(this).parent();
        $('#modalConstructionLiftPowers').modal('show');
    });

    $('#construction_lift').click(function(){
       var checkbox = $(this);
       if ($(this).is(':checked')){
           $('.construction_lift_power').show();
       }
       else {
           $('.construction_lift_power').hide();
       }
    });


    if ($('#construction_lift').is(':checked'))
    {
        $('.construction_lift_power').show();
    }





    //filter
    $('#filter_equipment').keyup(function () {
        var filter = $(this).val().toLowerCase();
        $("#tab5default").find("input[type=checkbox]").each(function () {
            var checkBox = $(this);
            var checkBoxId = $(this).attr('id').toLowerCase();
            var found = checkBoxId.indexOf(filter);
            if (found >= 0){
                checkBox.parent().show();
            }
            else {
                checkBox.parent().hide();
            }
        })
    });





    $(".ajaxbutton").click(function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')

            }
        })

        e.preventDefault();

        var state = $(this).val();
        var type = "POST"; //for creating new resource

        switch (this.id){
            case "btn-save-brand":
                var formData = {
                    name: $('#brandName').val(),
                };
                var my_url = document.location.protocol + "//" + document.location.host + "/admin/truckBrands";
                var selectbox = $('#brand');
                var div = false;
                var modal = $('#modalBrands')
                break;
            case "btn-save-engine":
                var formData = {
                    name: $('#engineName').val(),
                };
                var my_url = document.location.protocol + "//" + document.location.host + "/admin/truckEngines";
                var selectbox = $('#engine');
                var modal = $('#modalEngines');
                var div = false;
                break;
            case "btn-save-construction-lift-power":
                var formData = {
                    name: $('#constructionLiftPowerName').val(),
                };
                var my_url = document.location.protocol + "//" + document.location.host + "/admin/truckConstructionLiftPowers";
                var selectbox = $('#construction_lift_power');
                var modal = $('#modalConstructionLiftPowers');
                var div = false;
                break;
            case "btn-save-default_text":
                var text_type = $('#default_text_type').val();
                var text = CKEDITOR.instances.default_text_edit.getData();
                var formData = {
                    text_type : text_type,
                    text : text
                }
                var my_url = document.location.protocol + "//" + document.location.host + "/admin/defaultText";
                var selectbox = false;
                var modal = $('#modalDefaultText');
                if (text_type.match("1")) {
                    var myDiv = $('#default_text_1');
                }
                if (text_type.match("2")) {
                    var myDiv = $('#default_text_2');
                }


                break;

        }

        console.log(formData);

        $.ajax({

            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                if (selectbox) {
                    console.log(data);
                    var id = data.id;

                    //refill the selectbox
                    $.ajax({
                        type: "GET",
                        url: my_url,
                        success: function (data) {

                                selectbox.empty();
                                var list = '';
                                $.each(data, function (index, element) {
                                    list += "<option value='" + index + "'>" + element + "</option>";
                                });
                                selectbox.html(list);
                                selectbox.val(id);
                                modal.modal('hide');
                        },
                        error: function (data) {


                        }
                    })
                }
                else {
                    modal.modal('hide');
                    myDiv.html(data.message);
                }
            },
            error: function (data) {
                modal.modal('hide');
                $("h1:first").after("<div class='alert alert-danger'>"+data.responseJSON+"</div>");
                console.log('Error:', data);
            }
        });
    });


    /*jslint unparam: true */
    /*global window, $ */
    $(function () {
        'use strict';
        // Change this to the location of your server-side upload handler:
        var url = window.location.hostname === 'blueimp.github.io' ?
            '//jquery-file-upload.appspot.com/' : '/admin/truckBrands';
        $('#fileupload').fileupload({
            url: url,
            dataType: 'json',
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {

                    $('<div class="row" id="'+file.name+'"><i class="fa fa-fw fa-sort"></i><img src="'+file.thumbnailUrl+'"> <span title="Remove" style="color:red; font-size: 30px; cursor: pointer;"class="glyphicon glyphicon-remove" onclick="removeImage(this)"></span> </div>').appendTo('#files');

                });
                updateOrder();
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
        $('#files').sortable({
                tolerance: 'pointer',
                revert: 'invalid',
                placeholder: 'span2 well placeholder tile',
                forceHelperSize: true,
                update: function(event, ui) {
                    var image_order = $("#files").sortable('toArray').toString();
                    $('#image_order').val(image_order);
                }

            }
        );
        $('#files').disableSelection();

    });

    CKEDITOR.replace( 'remarks' );
    CKEDITOR.replace( 'construction_remarks' );
    CKEDITOR.replace( 'weights_remarks' );
    CKEDITOR.replace( 'equipment_terms_remarks' );
    CKEDITOR.replace( 'delivery_terms_remarks' );
    CKEDITOR.replace( 'default_text_edit' );
    CKEDITOR.replace( 'internal_remarks' );


    //check if the reference already exists
    $('#chassis_number').focusout(function(e) {
        e.preventDefault();

        $('#form-errors').html("");
        $('#submit_button').attr('disabled', false);
        var reference = $('#chassis_number').val(),
            $this = this; //aliased so we can use in ajax success function

        $.ajax({
            type: 'POST',
            url: '/admin/trucks/reference_exists',
            data: {reference: reference},
            success: function (data) {
                if (data != "") {
                    errorsHtml = '<div class="alert alert-danger"><ul><li>Chassis number already exists</li></ul></div>';
                    $('#form-errors').html(errorsHtml);
                    $('#submit_button').attr('disabled', true);
                }
            }
        });
    });


});

function removeImage(image, fileName){
    $(image).parent().remove();
    var deletedImages = $('#deleted_images').val();
    deletedImages += fileName + ",";
    $('#deleted_images').val(deletedImages);
    updateOrder();
}

function updateOrder(){
    var image_order = $("#files").sortable('toArray').toString();
    $('#image_order').val(image_order);
}


