$(document).ready(function() {
    /* when the interested button is clicked, the interested tab should be activated
     */
    $('#interested_button').click(function (event) {
        event.preventDefault();
        ga('send', 'event', 'Interest button', 'click', 'Dicar vrachtwagens');
        $('#interested_tab').click();
    });

    $('#contact_button').click(function (event) {
        event.preventDefault();
        ga('send', 'event', 'Contact button', 'click', 'Dicar vrachtwagens');
        $('#contact_form').submit();
    });
});



